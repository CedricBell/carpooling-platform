import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

// import by Material
// tslint:disable-next-line:no-implicit-dependencies
import 'hammerjs';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().
  bootstrapModule(AppModule)
  .catch(error => console.log(error));
