import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet-routing-machine';
import { InitializeMapService } from '~core/map/initialize-map.service';
import { SwPush } from '@angular/service-worker';
import { NotifService } from '~core/notifier-sw/notifier-sw.service';
import { UserService } from '~core/user-service/user.service';
import { User } from '~shared/components/user/user';
/**
 * Home page
 */
@Component({
    selector: 'home-page',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss', '../../sass/custom.scss']
})
export class HomePage implements OnInit {

    map: L.Map;

    onMapReady(map: L.Map) {
        L.marker([39.5334477, -0.1312811], {
            icon: L.icon({
                iconSize: [40, 41],
                iconAnchor: [22, 41],
                iconUrl: 'assets/bluepin.png'
            })
        }).addTo(map);
    }

    constructor(
        private readonly initializeMapService: InitializeMapService,
        private readonly swPush: SwPush,
        private readonly notifService: NotifService, private userService: UserService) {
    }



    ngOnInit(): void {
        this.map = L.map('map').setView([47.413220, -1.219482], 12);
        this.initializeMapService.setWikiMaps().addTo(this.map);
        if (this.swPush.isEnabled) {
            this.userService.getCurrentUser().then((user: User) => {
                this.notifService.subscribeToPush(user.id);
            }).catch(
                error => {
                    console.log(error);
                });
        }
    }

}
