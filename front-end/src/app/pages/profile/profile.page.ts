import { Component, OnInit } from '@angular/core';
import { UserService } from '~core/user-service/user.service';
import { User } from '~shared/components/user/user';
import { MatDialog } from '@intech/intech-ui';
import { ModifyProfileInfosModalComponent } from '~shared/components/modify-profile-infos-modal/modify-profile-infos-modal.component';
import { ModifyProfileAddressModalComponent } from '~shared/components/modify-profile-address-modal/modify-profile-address-modal.component';
import { ModifyProfileCarModalComponent } from '~shared/components/modify-profile-car-modal/modify-profile-car-modal.component';
import { AppConstants } from '~shared/components/constantes/app-constants';
import { MovInTechToaster } from '~shared/components/toaster/movInTech-toaster';
import { Car } from '~shared/components/car/car';
import { CarService } from '~core/car/car.service';
import { Address } from '~shared/components/address/address';
import { AddressService } from '~core/address/address.service';
import { IntechActionNotifierService } from '@intech/intech-core';

/**
 * Components to manage ride details
 */
@Component({
    selector: 'profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss', '../../sass/custom.scss'],
    providers: [UserService]
  })

export class ProfilePage implements OnInit {

  public currentCar: Car;
  public currentUser: User;
  public currentAddress: Address;
  public addUserToaster: MovInTechToaster = new MovInTechToaster('');
  public pictureUrl64: string;

  constructor(
      private readonly notifierService: IntechActionNotifierService,
      private readonly carService: CarService,
      private readonly userService: UserService,
      private readonly addressService: AddressService,
      public readonly dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.userService.getCurrentUser().then(
      (currentUser: User) => {
        this.currentUser = currentUser;
        this.currentCar = currentUser.car;
        this.currentAddress = currentUser.address;
        this.pictureUrl64 = currentUser.picture;
        Promise.resolve();
      },
      error => Promise.reject(error)
    );
  }

  /**
   * Function which open the dialog to modify personnal informations
   */
  public openInfosDialog(): void {
    const modalUser = new User();
    modalUser.firstName = this.currentUser.firstName;
    modalUser.lastName = this.currentUser.lastName;
    modalUser.email = this.currentUser.email;
    modalUser.trigram = this.currentUser.trigram;
    modalUser.username = this.currentUser.username;
    const dialogRef = this.dialog.open(ModifyProfileInfosModalComponent, {
      width: '550px',
      data: { user : modalUser }
    });
    dialogRef.afterClosed().subscribe(bool => {
      if (bool) {
        this.currentUser.firstName = modalUser.firstName;
        this.currentUser.lastName = modalUser.lastName;
        this.currentUser.email = modalUser.email;
        this.userService.updateUser(this.currentUser, this.currentUser.id).catch(error => {
          this.notifierService.notifyError(error.error.code);
        });
        this.notifierService.notifySuccess(AppConstants.USER_MODIFIED);
      }
    });
  }

  /**
   * Function which open the dialog to modify or add address
   */
  public openAddressDialog(): void {
    const dialogRef = this.dialog.open(ModifyProfileAddressModalComponent, {
      width: '550px',
      data: { address : this.currentAddress }
    });
    dialogRef.afterClosed().subscribe(newAddress => {
      if (newAddress !== undefined) {
        newAddress.id = this.currentAddress.id;
        this.currentAddress = newAddress;
        this.currentUser.address = this.currentAddress;
        this.addressService.updateAddress(this.currentAddress, this.currentAddress.id);
        this.userService.updateUser(this.currentUser, this.currentUser.id).catch(error => {
          this.notifierService.notifyError(error.error.code);
        });
        this.notifierService.notifySuccess(AppConstants.USER_MODIFIED);
      }
    });
  }
  /**
   * Function which open the dialog to modify car
   */
  public openCarDialog(): void {
    const modalCar = new Car();
    modalCar.licensePlate = this.currentCar.licensePlate;
    modalCar.brand = this.currentCar.brand;
    const dialogRef = this.dialog.open(ModifyProfileCarModalComponent, {
      width: '550px',
      data: { car : modalCar}
    });
    dialogRef.afterClosed().subscribe(bool => {
      if (bool === true) {
        this.currentCar.brand = modalCar.brand;
        this.currentCar.licensePlate = modalCar.licensePlate;
        this.currentUser.car = this.currentCar;
        this.carService.updateCar(this.currentCar, this.currentCar.id);
        this.userService.updateUser(this.currentUser, this.currentUser.id)
        .catch(error => {
          this.notifierService.notifyError(error.error.code);
        });
        this.notifierService.notifySuccess(AppConstants.USER_MODIFIED);
      }
    });
  }
  onFileChange(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = () => { // called once readAsDataURL is completed
        this.pictureUrl64 = reader.result.toString();
        if (this.pictureUrl64.length >= 16793600) {
          this.notifierService.notifyError(AppConstants.PROFILE_PICTURE_TOO_LONG);
          this.pictureUrl64 = this.currentUser.picture;
        } else {
          this.currentUser.picture = this.pictureUrl64;
          this.userService.updateUser(this.currentUser, this.currentUser.id).catch(error => {
            this.notifierService.notifyError(error.error.code);
          });
        }
      };

    }
  }
}
