import { Component, OnInit, AfterViewInit } from '@angular/core';
import { RideService } from '~core/rides/rides.service';
import { Ride } from '~shared/components/ride/ride';
import { UserService } from '~core/user-service/user.service';
import { __values } from 'tslib';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS, STEPPER_GLOBAL_OPTIONS } from '@intech/intech-ui';
import { DatePipe } from '@angular/common';

import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import moment from 'moment';
import { AppConstants } from '~shared/components/constantes/app-constants';
import { RideRequestHistoryService } from '~core/rides/ride-request-history.service';
import { RideRequestHistory } from '~shared/components/ride/ride-request-history';
import { NotifService } from '~core/notifier-sw/notifier-sw.service';
import { Notification } from '~shared/components/notification/notification';
import { RideUser } from '~shared/components/ride/ride-user';
import _ from 'lodash';
import { IntechActionNotifierService } from '@intech/intech-core';
import { Address } from '~shared/components/address/address';
import { AddressService } from '~core/address/address.service';
import { Observable } from 'rxjs';
import * as L from 'leaflet';
import 'leaflet-routing-machine';

import { AutocompletionService } from '~core/map/autocompletion.service';
import { InitializeMapService } from '~core/map/initialize-map.service';
import { RideRequestStatusEnum } from '~shared/components/ride-request-status-enum';
import { NONE_TYPE } from '@angular/compiler/src/output/output_ast';




@Component({
    selector: 'addride',
    templateUrl: './add-ride.page.html',
    styleUrls: ['add-ride.page.scss', '../../sass/custom.scss'],
    providers: [
        { provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false } },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class AddRidePage implements OnInit, AfterViewInit {

    constructor(
        private readonly router: Router,
        private readonly addressService: AddressService,
        private readonly rideService: RideService,
        private readonly userService: UserService,
        private readonly autocompletionService: AutocompletionService,
        private readonly formBuilder: FormBuilder,
        private readonly activatedRoute: ActivatedRoute,
        private readonly datePipe: DatePipe,
        private readonly rideRequestHistoryService: RideRequestHistoryService,
        private readonly notifService: NotifService,
        private readonly notifierService: IntechActionNotifierService,
        private readonly initializeMapService: InitializeMapService
    ) {
    }
    itineraryForm: FormGroup;
    dateForm: FormGroup;
    capacityForm: FormGroup;


    hours: number[] = new Array(24);
    mins: number[] = new Array(6);
    minDate = new Date();
    placeMin = 1;

    date = new FormControl(moment([2017, 0, 1]).format('DD/MM/YYYY'));
    capacityControl: FormControl = new FormControl('', [Validators.min(this.placeMin), Validators.max(150), Validators.required]);

    startControlAddress = new Address();
    endControlAddress = new Address();
    startAddress = new Address();
    endAddress = new Address();
    model = new Ride();
    hour: number = null;
    min: number = null;

    map: L.Map;

    stepsInputs: FormArray;

    private defaultIcon: L.Icon = L.icon({
        iconUrl: 'assets/none.png',
        shadowUrl: 'assets/none.png'
    });

    displayAutocompleteListStart: Address[] = [];
    displayAutocompleteListEnd: Address[] = [];
    displayAutocompleteListStep: Address[] = [];
    filteredDisplayAutocompleteListStart: Observable<Address[]>;
    filteredDisplayAutocompleteListEnd: Observable<Address[]>;
    filteredDisplayAutocompleteListStep: Observable<Address[]>[] = [];
    startControl = new FormControl([Validators.minLength(1), Validators.required]);
    endControl = new FormControl([Validators.minLength(1), Validators.required]);
    ngAfterViewInit() {
    }

    ngOnInit(): void {
        L.Marker.prototype.options.icon = this.defaultIcon;

        this.getCurrentUser();

        this.filteredDisplayAutocompleteListStart = this.autocompletionService.initializeFilteredDisplayAutocompleteList(this.startControl,
            this.displayAutocompleteListStart);
        this.filteredDisplayAutocompleteListEnd = this.autocompletionService.initializeFilteredDisplayAutocompleteList(this.endControl,
            this.displayAutocompleteListEnd);

        this.activatedRoute.queryParams.subscribe((rideIdToModify) => {
            if (rideIdToModify.rideId) {
                this.rideService.getRideById(rideIdToModify.rideId).then(
                    rideToModify => {

                        this.placeMin = this.numberParticipantsInRide(rideToModify);


                        this.capacityControl.setValidators([Validators.min(this.placeMin), Validators.max(150), Validators.required]);
                        this.capacityForm.setControl('capacityControl', this.capacityControl);

                        this.model = rideToModify;
                        this.startAddress = rideToModify.start;
                        this.endAddress = rideToModify.end;

                        this.initializeRideSteps(rideToModify);

                        const rideDate = moment(rideToModify.date, moment.defaultFormat).toDate();
                        this.dateForm.controls.dateControl.setValue(rideDate);
                        this.hour = rideDate.getHours();
                        this.min = rideDate.getMinutes();
                        this.model.date = rideToModify.date;
                        Promise.resolve();
                    },
                    error => Promise.reject(error)
                );
            }

        }, error => this.notifierService.notifyError(error.error.code));

        this.itineraryForm = this.formBuilder.group({
            startControl:  this.startControl,
            endControl: this.endControl,
            stepsInputs: this.formBuilder.array([this.createStepInput()])
        });

        this.dateForm = this.formBuilder.group({
            dateControl: new FormControl('', [Validators.minLength(1), Validators.required]),
            hoursControl: new FormControl('', [Validators.required]),
            minsControl: new FormControl('', [Validators.required])
        });
        this.capacityForm = this.formBuilder.group({
            capacityControl: this.capacityControl
        });
        this.model.date = this.dateForm.controls.dateControl.value;

    }

    createStepInput(): FormGroup {
        const stepFormGroup = this.formBuilder.group({
            stepControl: '',
        });
        this.filteredDisplayAutocompleteListStep.push(this.autocompletionService.initializeFilteredDisplayAutocompleteList
            (stepFormGroup.controls.stepControl, this.displayAutocompleteListStep));
        return stepFormGroup;
    }
    addStepInput(): void {
        this.stepsInputs = this.itineraryForm.get('stepsInputs') as FormArray;
        const newControl = this.createStepInput();
        this.stepsInputs.push(newControl);
    }

    getStepInput(): FormArray {

        return this.itineraryForm.get('stepsInputs') as FormArray;
    }

    adressControl(control: AbstractControl): boolean {
      return control.value !== '' && control.value.length > 1;
  }

  getCurrentUser(): void {

    this.userService.getCurrentUser().then(
      currentUser => {
          this.model.owner = currentUser;
          Promise.resolve();
      },
      error => this.notifierService.notifyError(error.error.code)
    );
  }

    validForm(): void {

        const tmpDate = new Date(this.model.date);
        tmpDate.setHours(this.hour);
        tmpDate.setMinutes(this.min);
        this.model.date = this.datePipe.transform(tmpDate, AppConstants.DATE_FORMAT);


        this.activatedRoute.queryParams.subscribe(rideToModify => {
            if (rideToModify.rideId) {
                this.startAddress.id = this.model.start.id;
                this.endAddress.id = this.model.end.id;

                this.addressService.updateAddress(this.startAddress, this.startAddress.id);
                this.addressService.updateAddress(this.endAddress, this.endAddress.id);


                this.rideService.updateRide(this.model, rideToModify.rideId).then((ridemodified: Ride) => {
                    for (const entry of this.model.users) {
                        if (entry.user.fcmToken !== '') {
                            const notif = new Notification(entry.user.fcmToken, 'MovInTech',
                                'Un trajet auquel vous participez a été modifié par ' + this.model.owner.firstName, '');
                            this.notifService.sendToToken(notif);
                        }
                    }
                    this.notifierService.notifySuccess(AppConstants.RIDE_MODIFIED);
                });
            } else {
                this.model.users = [];

                Promise.all([
                    this.addStartAddress(),
                    this.addEndAddress(),
                    this.addStepAddress()
                ]).then(() => {
                    this.model.start = this.startAddress;
                    this.model.end = this.endAddress;
                    this.rideService.addRide(this.model).then(rideCreated => {
                        this.rideRequestHistoryService.addRideRequestHistory(new RideRequestHistory(rideCreated, new Array()));
                    });
                }, error => {
                    return Promise.resolve(error);
                });
                this.notifierService.notifySuccess(AppConstants.RIDE_ADDED);
            }
            this.router.navigate(['']);
        });
    }
    displayAddressStart(address?: Address): string | undefined {
        return address ? address.name : undefined;
    }

    displayAddressEnd(address?: Address): string | undefined {
        return address ? address.name : undefined;
    }

    displayAddressStep(address?: Address): string | undefined {
        return address ? address.name : undefined;
    }

    addStartAddress(): Promise<void> {
        return this.addressService.addAddress(this.startAddress).then(
            (createdAddress: Address) => {
                this.startAddress = createdAddress;
                Promise.resolve();
            },
            error => Promise.reject(error)
        );
    }

    addEndAddress(): Promise<void> {
        return this.addressService.addAddress(this.endAddress).then(
            (createdAddress: Address) => {
                this.endAddress = createdAddress;
                Promise.resolve();
            },
            error => Promise.reject(error)
        );
    }

    initializeRideSteps(rideToModify: Ride): void {

        for (let i = 0; i < rideToModify.stepAddresses.length - 1; i++) {
            this.addStepInput();
        }
        this.stepsInputs = this.itineraryForm.get('stepsInputs') as FormArray;

        for (let j = 0; j < this.stepsInputs.length; j++) {
            this.stepsInputs.controls[j].setValue({
                stepControl: rideToModify.stepAddresses[j]
            });
        }
    }

    /* type any a changer */
    addStepAddress(): Promise<any> {
        this.stepsInputs = this.itineraryForm.get('stepsInputs') as FormArray;

        const dbRequestsPromises = [];

        this.stepsInputs.controls.forEach(stepInput => {

            if (stepInput.value.stepControl !== '') {

                dbRequestsPromises.push(this.addressService.addAddress(stepInput.value.stepControl).then(
                    (createdAddress: Address) => {
                        this.model.stepAddresses.push(createdAddress);
                        return Promise.resolve();
                    },
                    error => Promise.reject(error)
                ));

            }

        });
        return Promise.all(dbRequestsPromises);
    }

    numberParticipantsInRide(ride: Ride) {
        let numberParticipants = 0;

        ride.users.forEach((rideUser: RideUser) => {
            if (rideUser.status === RideRequestStatusEnum.RIDE_ACCEPTED) {
                numberParticipants++;
            }
        });

        return numberParticipants;

    }

    selectionChange(event: any): void {

        if (event.selectedStep.state === 'recap') {
            if (this.map !== undefined) {
                this.map.remove();
            }

            this.map = L.map('map').setView([47.413220, -1.219482], 9);
            this.initializeMapService.setStreetMaps().addTo(this.map);
            this.initializeMapService.createRedMarker(this.startAddress).addTo(this.map);
            this.initializeMapService.createRedMarker(this.endAddress).addTo(this.map);
            this.stepsInputs = this.itineraryForm.get('stepsInputs') as FormArray;
            const waypointsTab = [L.latLng(+this.startAddress.latitude, +this.startAddress.longitude)];
            this.stepsInputs.controls.forEach(stepInput => {
                if (stepInput.value.stepControl !== '') {
                    waypointsTab.push(L.latLng(+stepInput.value.stepControl.latitude, +stepInput.value.stepControl.longitude));
                    this.initializeMapService.createBlueMarker(stepInput.value.stepControl).addTo(this.map);
                }
            });
            waypointsTab.push(L.latLng(+this.endAddress.latitude, +this.endAddress.longitude));
            this.map.panTo(new L.LatLng((+this.startAddress.latitude + +this.endAddress.latitude) / 2,
                (+this.startAddress.longitude + +this.endAddress.longitude) / 2));

            L.Routing.control({
                show: false,
                waypoints: waypointsTab,
                router: new L.Routing.OSRMv1({
                    serviceUrl: AppConstants.ROUTING_URL
                })
                }).addTo(this.map);


            window.dispatchEvent(new Event('resize'));

            /*const features = this.map.vectorLayer.getSource().getFeatures();
            features.forEach((feature) => {
                this.map.vectorLayer.getSource().removeFeature(feature);
            });
            this.drawMapService.drawRide(this.map, this.startAddress, this.endAddress);*/
        }

    }
}





