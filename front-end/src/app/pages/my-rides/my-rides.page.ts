import { Component, OnInit, ViewEncapsulation, Injectable } from '@angular/core';
import { Ride } from '~shared/components/ride/ride';
import { Router } from '@angular/router';
import { MatDialog } from '@intech/intech-ui';
import { DeleteModalComponent } from '~shared/components/delete-modal/delete-modal.component';
import { RideService } from '~core/rides/rides.service';
import { RideUser } from '~shared/components/ride/ride-user';
import { RideRequestStatusEnum } from '~shared/components/ride-request-status-enum';
import { AppConstants } from '~shared/components/constantes/app-constants';
import { IntechActionNotifierService } from '@intech/intech-core';
import moment from 'moment';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

/**
 * Components to manage all personnal rides
 */
@Component({
  selector: 'myrides',
  templateUrl: './my-rides.page.html',
  styleUrls: ['my-rides.page.css', '../../sass/custom.scss'],
  providers: [
  ],
})
@Injectable()
export class MyRidesPage implements OnInit {

  public myRides: Ride[] = [];

  constructor(
    private readonly rideService: RideService,
    private readonly router: Router,
    public readonly dialog: MatDialog,
    private readonly notifierService: IntechActionNotifierService,
  ) {
  }
  public ngOnInit(): void {
    this.refresh();
  }


  /**
   * Function which load all of my rides list from the api
   */
  public refresh(): Promise<void> {
    const promiseResult =  this.rideService.getAllMyRides().then(
      myRides => {
        myRides.forEach((ride: Ride) => {
            this.myRides.push(ride);
            ride.numbrerUsersAccepted = this.numberUsersAccepted(ride);
        });
        Promise.resolve();
      },
      error => Promise.reject(error)
    );
    return promiseResult;
  }

  /**
   * Function which open the dialog to confirm delete of the ride
   * @param ridetoDelete ride selected by user
   */
  public openDialog(rideToDelete: Ride): void {
    const dialogRef = this.dialog.open(DeleteModalComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe((response: boolean) => {
      if (response) {
        this.deleteRide(rideToDelete);
      }
    });

  }

  /**
   * Function which navigate to add-ride page to update a ride
   * @param rideToUpdate ride selected by user
   */
  public updateRide(rideToUpdate: Ride): void {
    this.router.navigate([AppConstants.ROUTE_ADD_COVOIT], {
      queryParams: { rideId: rideToUpdate.id }
    });
  }


  deleteRide(ride: Ride): void {
    this.rideService.deleteRide(ride.id);
    this.router.navigate(['']);
    this.notifierService.notifySuccess(AppConstants.DELETE_MODAL_CONFIRMATION);
  }

  rideEvents(ride: Ride): void {
    this.router.navigate([AppConstants.ROUTE_RIDE_EVENT], {
      queryParams: { rideId: ride.id }
    });
  }

  numberUsersAccepted(ride: Ride): number {

    let numberUserAccepted = 0;
    ride.users.forEach((rideUser: RideUser) => {
      if (rideUser.status === RideRequestStatusEnum.RIDE_ACCEPTED) {
        numberUserAccepted++;
      }
    });
    return numberUserAccepted;
  }

  getMonth(ride: Ride): string {
    return moment(ride.date).format('MMMM');
  }
  getDay(ride: Ride): string {
    return moment(ride.date).format('DD');
  }
  getHour(ride: Ride): string {
    return moment(ride.date).format('HH:mm');
  }

}
