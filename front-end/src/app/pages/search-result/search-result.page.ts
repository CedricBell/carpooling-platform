import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { MAT_DATE_FORMATS, DateAdapter, STEPPER_GLOBAL_OPTIONS, MAT_DATE_LOCALE } from '@intech/intech-ui';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { ActivatedRoute, Router } from '@angular/router';
import { RideService } from '~core/rides/rides.service';
import { DatePipe } from '@angular/common';
import { RideRequestHistoryService } from '~core/rides/ride-request-history.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Ride } from '~shared/components/ride/ride';
import { UserService } from '~core/user-service/user.service';
import { SearchService } from '~core/search/search.service';
import { User } from '~shared/components/user/user';
import { IntechActionNotifierService } from '@intech/intech-core';
import { RideRequestStatusEnum } from '~shared/components/ride-request-status-enum';
import { RideUser } from '~shared/components/ride/ride-user';
import { AppConstants } from '~shared/components/constantes/app-constants';

@Component({
    selector: 'search-result',
    templateUrl: './search-result.page.html',
    styleUrls: ['search-result.page.scss', '../../sass/custom.scss'],
    providers: [
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class SearchResultPage implements OnInit {

    constructor(
        private readonly router: Router,
        private readonly rideService: RideService,
        private readonly formBuilder: FormBuilder,
        private readonly activatedRoute: ActivatedRoute,
        private readonly datePipe: DatePipe,
        private readonly userService: UserService,
        private readonly searchService: SearchService,
        private readonly notifierService: IntechActionNotifierService,
        private readonly rideRequestHistoryService: RideRequestHistoryService
    ) {
    }

    @Input() rideFilter: Ride;
    rides: Ride[];
    hour: number = null;
    min: number = null;
    currentUser: User;

    ngOnInit(): void {

      this.userService.getCurrentUser().then(
        currentUser => {
          this.currentUser = currentUser;
          this.initRides();
          Promise.resolve();
        },
        error => this.notifierService.notifyError(error.error.code)
      );
    }

    /**
     * Function which load all rides from the api
     */
    initRides(): void {

    this.rideService.getAllRides().then((allRides: Ride[]) => {
      this.rides = allRides.filter((ride: Ride) => {

        if (!this.searchService.isMyRide(ride , this.currentUser) &&
        !this.searchService.isRideStatusIn(ride , this.currentUser, RideRequestStatusEnum.RIDE_ACCEPTED) &&
        !this.searchService.isRideStatusIn(ride , this.currentUser, RideRequestStatusEnum.RIDE_REJECTED) &&
        !this.searchService.isRideFull(ride)) {
          ride.numbrerUsersAccepted = this.numbrerUsersAccepted(ride);
          return true;
        }

        return false;

      });
      Promise.resolve();
    },
      error => Promise.reject(error)
    );
  }

    validForm(): void {

      this.searchService.setData(this.rideFilter);
      this.router.navigate([AppConstants.ROUTE_RESULT_SEARCH_MOBILE]);
    }

    numbrerUsersAccepted(ride: Ride): number {

      let numberUserAccepted = 0;
      ride.users.forEach((rideUser: RideUser) => {
        if (rideUser.status === RideRequestStatusEnum.RIDE_ACCEPTED) {
          numberUserAccepted++;
        }
      });
      return numberUserAccepted;
    }


  /**
   * Function which navigate to ride-details-page and shows the detail of the ride selected
   * @param rideToShow ride selected by user
   */
  showRideDetails(rideToShow: Ride): void {

    this.router.navigate([AppConstants.ROUTE_RIDE_DETAILS], {
      queryParams:
        { rideId: rideToShow.id }

    });
  }

 isRideRequested(ride: Ride): boolean {
    return this.searchService.isRideStatusIn(ride , this.currentUser, RideRequestStatusEnum.RIDE_REQUESTED);
  }


}





