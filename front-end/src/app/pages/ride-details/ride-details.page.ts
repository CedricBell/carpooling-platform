import { Component, Injectable, OnInit } from '@angular/core';
import { RideService } from '~core/rides/rides.service';
import { ActivatedRoute } from '@angular/router';
import { Ride } from '~shared/components/ride/ride';
import { MatDialog } from '@intech/intech-ui';
import { AddCommentModalComponent } from '~shared/components/add-comment-modal/add-comment-modal.component';
import { Location } from '@angular/common';
import { RideRequestHistory } from '~shared/components/ride/ride-request-history';
import _ from 'lodash';
import { UserService } from '~core/user-service/user.service';
import { User } from '~shared/components/user/user';
import { RideRequestStatusEnum } from '~shared/components/ride-request-status-enum';
import { IntechActionNotifierService } from '@intech/intech-core';
import { RideUser } from '~shared/components/ride/ride-user';
import { ConstantPool } from '@angular/compiler';


/**
 * Components to manage ride details
 */
@Component({
  selector: 'ride-details',
  templateUrl: './ride-details.page.html',
  styleUrls: ['ride-details.page.css', '../../sass/custom.scss'],
  providers: [
  ],
})
@Injectable()
export class RideDetailsPage implements OnInit {

  constructor(
    private readonly rideService: RideService,
    private readonly userService: UserService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly location: Location,
    public readonly dialog: MatDialog,
    public readonly notifierService: IntechActionNotifierService
  ) { }

  ride: Ride;
  previousUrl: string;
  myRideRequestHistory: RideRequestHistory;
  currentUser: User;
  allRides: Ride[];
  disabled = false;

  ngOnInit(): void {

  this.userService.getCurrentUser().then(
        currentUser => {
            this.currentUser = currentUser;
            Promise.resolve();
        },
        error => this.notifierService.notifyError(error.error.code)
    );
  this.activatedRoute.queryParams.subscribe(routeParam => {

      if (routeParam.rideId) {

        this.rideService.getRideById(routeParam.rideId).then(
          ride => {
            this.ride = ride;
            this.previousUrl = routeParam.url;
            Promise.resolve();
          },
          error =>
            this.notifierService.notifyError(error.error.code)
        );
      }
    }
    );

  this.rideService.getAllRides().then(
      (rides: Ride[]) => {
          this.allRides = rides.filter((ride: Ride) => ride.users.length !== 0);
          if (this.isUserAlreadyOnARide()) {
            this.disabled = true;
          }
      });
  }

  /**
   * Function which return the number of places left in the ride
   */
  getPlaceNumer(): number {
    return this.ride.capacity - this.ride.users.length;
  }

  /**
   * Function which open the dialog to add comment to the request
   * @param ridetoDelete ride selected by user
   */
  public openDialog(ridetoDelete: Ride): void {
    const dialogRef = this.dialog.open(AddCommentModalComponent, {
      width: '550px',
      data: { ride: this.ride, rideRequestHistory: this.myRideRequestHistory }
    });
  }


  public isRideUserAccepted(rideUser: RideUser): boolean {
    return rideUser.status === RideRequestStatusEnum.RIDE_ACCEPTED;
  }

  returnSearchList(): void {
    this.location.back();
  }

  isRideUserOnRide(rideUser) {
    return (rideUser.user.trigram === this.currentUser.trigram && rideUser.status === RideRequestStatusEnum.RIDE_ACCEPTED);
  }

  isUserAlreadyOnARide(): boolean {
    if (!this.ride) {
      return false;
    }/*
    _.forEach(this.ride.users , (rideUser: RideUser ) => {
      console.log(this.currentUser);
      if (rideUser.user.trigram === this.currentUser.trigram && rideUser.status === RideRequestStatusEnum.RIDE_ACCEPTED) {
        return true;
      }
    });*/
    return (this.ride.users.filter(this.isRideUserOnRide).length !== 0);
  }
}
