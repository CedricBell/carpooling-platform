import { Component, OnInit, OnDestroy } from '@angular/core';
import { MAT_DATE_FORMATS, DateAdapter, STEPPER_GLOBAL_OPTIONS, MAT_DATE_LOCALE } from '@intech/intech-ui';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { ActivatedRoute, Router } from '@angular/router';
import { RideService } from '~core/rides/rides.service';
import { DatePipe } from '@angular/common';
import { RideRequestHistoryService } from '~core/rides/ride-request-history.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Ride } from '~shared/components/ride/ride';
import { UserService } from '~core/user-service/user.service';
import { SearchService } from '~core/search/search.service';
import { AppConstants } from '~shared/components/constantes/app-constants';

@Component({
    selector: 'searchRideMobile',
    templateUrl: './search-ride-mobile.page.html',
    styleUrls: ['search-ride-mobile.page.scss', '../../sass/custom.scss'],
    providers: [
        { provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false } },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class SearchRideMobilePage implements OnInit {

    constructor(
        private readonly router: Router,
        private readonly rideService: RideService,
        private readonly formBuilder: FormBuilder,
        private readonly activatedRoute: ActivatedRoute,
        private readonly datePipe: DatePipe,
        private readonly userService: UserService,
        private readonly searchService: SearchService,
        private readonly rideRequestHistoryService: RideRequestHistoryService
    ) {
    }

    hours: number[] = new Array(24);
    mins: number[] = new Array(6);
    minDate = new Date();
    placeMin = 1;

    rideFilter: Ride = new Ride();
    hour: number = null;
    min: number = null;

    ngOnInit(): void {

      if (this.searchService.getData() != null) {
        this.rideFilter = this.searchService.getData();
      }
      this.rideFilter.owner = null;
      this.rideFilter.stepAddresses = null;
    }

    validForm(): void {

      this.searchService.setData(this.rideFilter);
      this.router.navigate([AppConstants.ROUTE_RESULT_SEARCH_MOBILE]);
    }

}





