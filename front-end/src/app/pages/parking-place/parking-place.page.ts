import { Component, OnInit } from '@angular/core';
import { ParkingHistory } from '~shared/components/parking/parking-history';
import { ParkingPlace } from '~shared/components/parking/parking-place';
import { ParkingHistoryService } from '~core/parking/parking-history.service';
import { ParkingPlaceService } from '~core/parking/parking-place.service';
import { DatePipe } from '@angular/common';
import _ from 'lodash';
import { UserService } from '~core/user-service/user.service';
import { User } from '~shared/components/user/user';
import { AppConstants } from '~shared/components/constantes/app-constants';
import { IntechActionNotifierService } from '@intech/intech-core';


/**
 * Components to manage parking places
 */
@Component({
  selector: 'app-parking',
  templateUrl: './parking-place.page.html',
  styleUrls: ['./parking-place.page.css', '../../sass/custom.scss']
})
export class ParkingPlacePage implements OnInit {

  public parkingHistories: ParkingHistory[] = [];
  public parkingPlaces: ParkingPlace[] = [];
  public placeChecked: number;
  public currentUser: User;

  constructor(
    private readonly parkingPlaceService: ParkingPlaceService,
    private readonly parkingHistoryService: ParkingHistoryService,
    private readonly datepipe: DatePipe,
    private readonly notifierService: IntechActionNotifierService,
    private readonly userService: UserService,

  ) { }

  /**
   * Function which load parking places list and parking history list from the api
   */
  refresh(): Promise<boolean> {

    return Promise.all([
      this.getParkingList(),
      this.getParkingHistoryList()
    ]).then(() => {
      this.parkingHistories.forEach((history) => {
        if (history.trigramOccupant === this.currentUser.trigram) {
          history.parkingSpot.disabled = true;
        }
      });
      return Promise.resolve(true);
    }, error => {
      return Promise.resolve(false);
    });

  }
  ngOnInit() {

    this.userService.getCurrentUser().then(
      (currentUser: User) => {
        this.currentUser = currentUser;
        this.refresh();
        Promise.resolve();
      },
      error => Promise.reject(error)
    );
    this.parkingPlaces = this.parkingPlaces.map(place => {
      return place;
    });
  }

  /**
   * Function which call parking place service to charge parking places list
   *
   * Only free Parking places are kept and sorted by number of place crescents.
   */
  getParkingList(): Promise<void> {
    return this.parkingPlaceService.getParkingPlaces().then(
      (allParkingPlaces: ParkingPlace[]) => {
        this.parkingPlaces = _.sortBy(allParkingPlaces, 'placeNumber').filter(
          (place: ParkingPlace) => place.status === AppConstants.PARKING_FREE_STATUS
        );
        Promise.resolve();
      },
      error => Promise.reject(error)
    );

  }

  /**
   * Function which call parking history service to charge parking history list
   *
   * Only today's history and currently taken places are kept and sorted by number of place crescents.
   */
  getParkingHistoryList(): Promise<void> {
    return this.parkingHistoryService.getAllParkingHistory().then(
      (allParkingHistory: ParkingHistory[]) => {
        this.parkingHistories = _.sortBy(allParkingHistory, 'parkingSpot.placeNumber').filter(this.isParkingHistoryFromToday)
          .filter(
            (history: ParkingHistory) => history.freePlaceDate === null
          );
        Promise.resolve();
      },
      error => Promise.reject(error)
    );
  }

  /**
   * Function which determine if a parking history is from today
   * @param history  history to check
   *
   */
  isParkingHistoryFromToday(history: ParkingHistory): boolean {

    const parkingDate: Date = new Date(history.date);
    const today: Date = new Date(Date.now());

    return (parkingDate.getDate() === today.getDate() &&
      parkingDate.getMonth() === today.getMonth()
      && parkingDate.getFullYear() === today.getFullYear());

  }

  /**
   * Function which determine is a user already occupies a place today
   */
  isPlaceAlreadyOccupated(): boolean {
    return (this.parkingHistories.filter((history: ParkingHistory) =>
      history.trigramOccupant === this.currentUser.trigram).length !== 0);
  }

  /**
   * Function which determines if the history is mine
   * @param parkingHistory history to check
   */
  isPlaceMine(parkingHistory: ParkingHistory): boolean {

    if (this.currentUser) {
      return parkingHistory.trigramOccupant === this.currentUser.trigram;
    }

    return false;
  }

  /**
   * Function which update the status of the place when a user takes it
   * and create the history on parking history
   * We first check if the user doesn't occupies a place
   * @param placeChecked place taken from the user
   */
  selectParkingPlace(placeChecked: ParkingPlace): void {

    /*Status of the checked place change*/
    placeChecked.status = AppConstants.PARKING_OCCUPATED_STATUS;

    this.placeChecked = placeChecked.number;

    /*Add the occupation to the parking history*/
    const parkinghistoryCreated: ParkingHistory = new ParkingHistory();
    parkinghistoryCreated.licensePlate = this.currentUser.car.licensePlate;
    parkinghistoryCreated.trigramOccupant = this.currentUser.trigram;


    parkinghistoryCreated.date = this.datepipe
      .transform(Date.now(), AppConstants.DATE_FORMAT);
    parkinghistoryCreated.parkingSpot = placeChecked;

    if (this.isPlaceAlreadyOccupated()) {

      placeChecked.status = AppConstants.PARKING_FREE_STATUS;
      this.notifierService.notifyWarning(AppConstants.PLACE_ALREADY_OCCUPATED);

    } else {
      this.parkingPlaceService.updateParkingPlace(placeChecked.id, parkinghistoryCreated)
      .then(() => {
        this.refresh();
        this.notifierService.notifySuccess(AppConstants.PLACE_RESERVED);

      }, error => {
        this.notifierService.notifyError(error.error.code);
      });
    }
  }

  /**
   * Function which released a place of a user and set to the correct history the date of the release
   * The status of the parking place associated change to free
   * The free date is added to the history, so we remove the old one (whithout date free place) and add the new (whith date free place)
   * We don't update the old one because in the api the updateParkingPlace funciton add the history to Parking History collection
   * @param historyRemoved history to add the date of release
   */
  removeOccupation(historyRemoved: ParkingHistory): void {

    const placeToUpdate: ParkingPlace = historyRemoved.parkingSpot;
    historyRemoved.parkingSpot.status = AppConstants.PARKING_FREE_STATUS;
    historyRemoved.freePlaceDate = this.datepipe
      .transform(Date.now(), AppConstants.DATE_FORMAT);

    Promise.all([
      this.parkingHistoryService.removeParkingHistory(historyRemoved.id),
      this.parkingPlaceService.updateParkingPlace(placeToUpdate.id, historyRemoved)
    ]).then(() => {
      this.notifierService.notifySuccess(AppConstants.PLACE_FREE);
      this.refresh();
    }, error => {
      this.notifierService.notifyError(error.error.code);
    });
  }
}
