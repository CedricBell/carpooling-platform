import { Component, OnInit, AfterContentInit, Output } from '@angular/core';
import { RideService } from '~core/rides/rides.service';
import { Ride } from '~shared/components/ride/ride';
import { __values } from 'tslib';
import { User } from '~shared/components/user/user';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment } from 'moment';

const moment = _rollupMoment || _moment;
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@intech/intech-ui';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { UserService } from '~core/user-service/user.service';
import _ from 'lodash';
import { SearchService } from '~core/search/search.service';
import { RideUser } from '~shared/components/ride/ride-user';
import { IntechActionNotifierService } from '@intech/intech-core';
import { RideRequestStatusEnum } from '~shared/components/ride-request-status-enum';
import { AppConstants } from '~shared/components/constantes/app-constants';


/**
 * Components to manage search ride
 */
@Component({
  selector: 'search-ride',
  templateUrl: './search-ride.page.html',
  styleUrls: ['./search-ride.page.scss', '../../sass/custom.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]

})
export class SearchRidePage implements OnInit {


  constructor(
    private readonly router: Router,
    private readonly addCovoitService: RideService,
    private readonly formBuilder: FormBuilder,
    private readonly userService: UserService,
    private readonly searchService: SearchService,
  ) { }

  element: any;
  searchGroup: FormGroup;
  owner: User;
  step = null;
  hours: number[] = new Array(24);
  mins: number[] = new Array(6);


  minDate = new Date();

  dateCtrl = new FormControl(moment([2017, 0, 1]).format('DD/MM/YYYY'), [Validators.minLength(1), Validators.required]);

  rideFilter: Ride = new Ride();

  ngOnInit(): void {

    this.searchGroup = this.formBuilder.group({
      dateCtrl: this.dateCtrl,
    });

    this.rideFilter.owner = null;
    this.rideFilter.stepAddresses = null;
  }

  setStep(index: Ride) {
    this.step = index;
  }

  back(): void {
      this.router.navigate([AppConstants.ROUTE_SEARCH_COVOIT]);
    }

  removeAllFilter(): void {
      this.rideFilter = new Ride();
  }
}
