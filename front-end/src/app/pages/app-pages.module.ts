import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppPagesRoutes } from './app-pages.routing';
import { HomePage } from './home/home.page';
import { SharedModule } from '~shared/shared.module';
import { ParkingPlacePage } from './parking-place/parking-place.page';
import { AddRidePage } from './add-ride/add-ride.page';
import { SearchRidePage } from './search-ride/search-ride.page';
import { RideEventsPage } from './ride-events/ride-events.page';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyRidesPage } from './my-rides/my-rides.page';
import { LoginPage } from '../pages/login/login.page';
import { RideDetailsPage } from './ride-details/ride-details.page';
import { DeleteModalComponent } from '~shared/components/delete-modal/delete-modal.component';
import { AddCommentModalComponent } from '~shared/components/add-comment-modal/add-comment-modal.component';
import { MyBookingsPage } from './my-bookings/my-bookings.page';
import { ProfilePage } from './profile/profile.page';
import { ModifyProfileInfosModalComponent } from '~shared/components/modify-profile-infos-modal/modify-profile-infos-modal.component';
import { ModifyProfileAddressModalComponent } from '~shared/components/modify-profile-address-modal/modify-profile-address-modal.component';
import { ModifyProfileCarModalComponent } from '~shared/components/modify-profile-car-modal/modify-profile-car-modal.component';
import { SearchRideMobilePage } from './search-ride-mobile/search-ride-mobile.page';
import { ResultSearchMobilePage } from './result-search-mobile/result-search-mobile.page';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { RideRecapPage } from './ride-recap/ride-recap.page';
import { SearchResultPage } from './search-result/search-result.page';

const PAGES_AND_COMPONENTS_FOR_PAGES = [
  HomePage,
  AddRidePage,
  SearchRidePage,
  ParkingPlacePage,
  MyRidesPage,
  LoginPage,
  RideDetailsPage,
  RideEventsPage,
  MyBookingsPage,
  SearchRideMobilePage,
  ResultSearchMobilePage,
  MyBookingsPage,
  ProfilePage,
  RideRecapPage,
  SearchResultPage
  /** ADD YOUR PAGES HERE ! */
];

/**
 * Service provider
 */
const FUNCTIONAL_SERVICES = [];

@NgModule({
  declarations: [...PAGES_AND_COMPONENTS_FOR_PAGES],
  imports: [SharedModule, RouterModule, AppPagesRoutes, BrowserAnimationsModule, LeafletModule.forRoot()],
  providers: [...FUNCTIONAL_SERVICES],
  entryComponents: [DeleteModalComponent, AddCommentModalComponent, ModifyProfileInfosModalComponent,
  ModifyProfileAddressModalComponent, ModifyProfileCarModalComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppPagesModule { }
