import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomePage } from './home/home.page';

// TODO delete import and example page when the application was started ang you learn how it's works`
import { AddRidePage } from './add-ride/add-ride.page';
import { SearchRidePage } from './search-ride/search-ride.page';
import { ParkingPlacePage } from './parking-place/parking-place.page';
import { MyRidesPage } from './my-rides/my-rides.page';
import { RideEventsPage } from './ride-events/ride-events.page';
import { LoginPage } from '../pages/login/login.page';
import { AuthGuardService as AuthGuard } from '../core/authentication/authguard.service';
import { RideDetailsPage } from './ride-details/ride-details.page';
import { MyBookingsPage } from './my-bookings/my-bookings.page';
import { SearchRideMobilePage } from './search-ride-mobile/search-ride-mobile.page';
import { ResultSearchMobilePage } from './result-search-mobile/result-search-mobile.page';
import { ProfilePage } from './profile/profile.page';


/**
 * All routes
 */
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomePage, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfilePage, canActivate: [AuthGuard] },
  { path: 'parking', component: ParkingPlacePage, canActivate: [AuthGuard] },
  { path: 'add-covoit', component: AddRidePage, canActivate: [AuthGuard] },
  { path: 'search-ride', component: SearchRidePage, canActivate: [AuthGuard] },
  { path: 'search-ride-mobile', component: SearchRideMobilePage, canActivate: [AuthGuard] },
  { path: 'result-search-mobile', component: ResultSearchMobilePage, canActivate: [AuthGuard] },
  { path: 'parking', component: ParkingPlacePage, canActivate: [AuthGuard] },
  { path: 'myrides', component: MyRidesPage, canActivate: [AuthGuard] },
  { path: 'ride-details', component: RideDetailsPage, canActivate: [AuthGuard] },
  { path: 'ride-event', component: RideEventsPage, canActivate: [AuthGuard] },
  { path: 'my-bookings', component: MyBookingsPage, canActivate: [AuthGuard] },
  { path: 'login', component: LoginPage },
  /**  ADD YOUR APPLICATION ROUTE HERE ! */

  // TODO delete import and example page when the application was started ang you learn how it's works`
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppPagesRoutes { }
