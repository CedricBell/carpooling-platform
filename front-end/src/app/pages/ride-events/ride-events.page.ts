import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import _ from 'lodash';
import { RideRequestStatusEnum } from '~shared/components/ride-request-status-enum';
import { ActivatedRoute } from '@angular/router';
import { RideRequestHistoryService } from '~core/rides/ride-request-history.service';
import { RideEventService } from '~core/rides/ride-event.service';
import { RideRequestHistory } from '~shared/components/ride/ride-request-history';
import { RideEvent } from '~shared/components/ride/ride-event';
import { RideService } from '~core/rides/rides.service';
import { Ride } from '~shared/components/ride/ride';
import { User } from '~shared/components/user/user';
import { RideUser } from '~shared/components/ride/ride-user';
import { IntechActionNotifierService } from '@intech/intech-core';
import { RideUserService } from '~core/rides/ride-user.service';
import { AppConstants } from '~shared/components/constantes/app-constants';

/**
 * Components to manage ride request
 */
@Component({
  selector: 'ride-event',
  templateUrl: './ride-events.page.html',
  styleUrls: ['./ride-events.page.scss', '../../sass/custom.scss'],
  providers: [RideEventService, RideRequestHistoryService]

})
export class RideEventsPage implements OnInit {

  public rideUsersAc: RideUser[] = [];
  public rideUsersRq: RideUser[] = [];
  public rideUsersRj: RideUser[] = [];
  public ride: Ride;

  public allRideUsers: RideUser[] = [];

  public myRideRequestHistory: RideRequestHistory;

  public newRideEvent = new RideEvent();

  public rideId: string;

  constructor(
    private readonly rideRequestHistoryService: RideRequestHistoryService,
    private readonly rideUserService: RideUserService,
    private readonly notifierService: IntechActionNotifierService,
    private readonly actroute: ActivatedRoute,
    private readonly datePipe: DatePipe,
    private readonly rideService: RideService
  ) { }

  /**
   * Function which load ride events list and ride request history list from the api
   */
  refresh(): Promise<boolean> {
    return Promise.all([
      this.getRideUpdate(),
      this.getMyRideRequestHistory(),
      this.getAllRideUsers(),
    ]).then(() => {

      this.rideUsersAc = this.getRideUsersAcList();
      this.rideUsersRj = this.getRideUsersRjList();
      this.rideUsersRq = this.getRideUsersRqList();

      return Promise.resolve(true);
    }, error => {
      return Promise.resolve(false);
    });

  }

  ngOnInit() {
    this.refresh();
  }


  /**
   * Function which charge requested ride events list.
   * We want ride events which have the "requested" status, but aren't already in the accepted or rejected ride events list.
   */
  getRideUsersRqList(): RideEvent[] {

    return this.allRideUsers.filter(
      (rideUser: RideUser) => {
        return rideUser.status === RideRequestStatusEnum.RIDE_REQUESTED;
      }
    );

  }

  /**
   * Function which charge ride events list with the status "accepted".
   */
  getRideUsersAcList(): RideEvent[] {

    return this.allRideUsers
      .filter(
        (rideUser: RideUser) => rideUser.status === RideRequestStatusEnum.RIDE_ACCEPTED
      );
  }

  /**
   * Function which charge ride events list with the status "rejected".
   */
  getRideUsersRjList(): RideEvent[] {
    return this.allRideUsers
      .filter(
        (rideUser: RideUser) => rideUser.status === RideRequestStatusEnum.RIDE_REJECTED
      );
  }

  getRideUpdate(): Promise<void> {
    let id = '';
    this.actroute.queryParams.subscribe(params => {
      if (params) {
        id = params.rideId;
      }
    });

    return this.rideService.getRideById(id).then(
      (currentRide: Ride) => {
        this.ride = currentRide;
        Promise.resolve();
      },
      error => Promise.reject(error)
    );
  }

  /**
   * Function which call ride request history service to charge requested rides history list
   *
   * Only history associated with the current ride is kept.
   */
  getAllRideUsers(): Promise<void> {
    return this.rideService.getRideById(this.rideId).then(

      (ride: Ride) => {
        this.allRideUsers = ride.users;
        Promise.resolve();
      },
      error => Promise.reject(error)
    );
  }


  /**
   * Function which call ride request history service to charge requested rides history list
   *
   * Only history associated with the current ride is kept.
   */
  getMyRideRequestHistory(): Promise<void> {
    this.actroute.queryParams.subscribe(params => {
      if (params) {
        this.rideId = params.rideId;
      }
    });
    return this.rideRequestHistoryService.getRideRequestHistoryByRideId(this.rideId).then(

      (rideRequest: RideRequestHistory) => {
        this.myRideRequestHistory = rideRequest;
        Promise.resolve();
      },
      error => Promise.reject(error)
    );
  }


  /**
   * Function which accept a ride request.
   *
   * The function create a new ride event with the status "accepted" and the current date, and add it to the ride request history.
   * @param rideEvent ride event we want to accept
   */
  acceptRequest(rideUser: RideUser): void {
    if (this.isPlacesLeft() && !(this.isUserAlreadyAccepted(rideUser.user))) {
      this.actionRequest(rideUser, RideRequestStatusEnum.RIDE_ACCEPTED);
    }
  }

  actionRequest(rideUser: RideUser, newStatus: RideRequestStatusEnum): void {
    const rideToUpdate: Ride = this.myRideRequestHistory.ride;

    // const index = _.findIndex(rideToUpdate.rideUsers, rideUser);

    rideUser.status = newStatus;
    rideUser.date = this.datePipe.transform(Date.now(), AppConstants.DATE_FORMAT);

    this.newRideEvent.comment = rideUser.comment;
    this.newRideEvent.date = this.datePipe.transform(Date.now(), AppConstants.DATE_FORMAT);
    this.newRideEvent.status = newStatus;
    this.newRideEvent.user = rideUser.user;
    this.myRideRequestHistory.rideEvents.push(this.newRideEvent);
    // rideToUpdate.rideUsers[index] = rideUser;

    this.rideUserService.updateRideUser(rideUser.id , rideToUpdate.id, rideUser).catch(error =>
      this.notifierService.notifyError(error.error.code));

    /*this.rideService.updateRide(rideToUpdate, this.myRideRequestHistory.ride.id).catch(error => {
      this.notifierService.notifyError(error.error.code);
    });*/
    this.rideRequestHistoryService.updateRideRequestHistory(this.myRideRequestHistory.id, this.myRideRequestHistory).then(() => {
      this.refresh();
    });
  }

  /**
   * Function which reject a ride request.
   *
   * The function create a new ride event with the status "rejected" and the current date, and add it to the ride request history.
   * @param rideEvent ride event we want to reject
   */
  rejectRequest(rideUser: RideUser): void {
      this.actionRequest(rideUser, RideRequestStatusEnum.RIDE_REJECTED);
    }

  /**
   * Function which determine if the ride is full or not
   */
  isPlacesLeft(): boolean {
    if (this.ride === undefined) {
      return false;
    }
    return (this.ride.capacity - this.rideUsersAc.length > 0);
  }

  isUserAlreadyAccepted(user: User): boolean {
    if (this.ride !== undefined) {
      return false;
    }
    let userAlreadyAccepted = false;
    this.rideUsersAc.forEach(rideUser => {
      if (rideUser.user.id === user.id) {
        userAlreadyAccepted = true;
      }
    });
    return userAlreadyAccepted;
  }
}
