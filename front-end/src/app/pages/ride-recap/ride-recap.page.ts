import { Component, Injectable, OnInit, Input } from '@angular/core';
import { Ride } from '~shared/components/ride/ride';
import { Address } from '~shared/components/address/address';
import { FormArray } from '@angular/forms';

/**
 * Components to manage ride recap
 */
@Component({
  selector: 'ride-recap',
  templateUrl: './ride-recap.page.html',
  styleUrls: ['ride-recap.page.scss', '../../sass/custom.scss'],
  providers: [
  ],
})
@Injectable()
export class RideRecapPage {

  @Input() hour: number;
  @Input() min: number;
  @Input() model: Ride;
  @Input() startAddress: Address;
  @Input() endAddress: Address;
  @Input() stepsInputs: FormArray;





}
