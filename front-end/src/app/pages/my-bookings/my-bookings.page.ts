import { Component, OnInit } from '@angular/core';
import { IntechAppNotifierToaster } from '~core/notifier-toaster/intech-notifier-toaster.service';
import _ from 'lodash';
import { RideRequestStatusEnum } from '~shared/components/ride-request-status-enum';
import { RideRequestHistoryService } from '~core/rides/ride-request-history.service';
import { RideEventService } from '~core/rides/ride-event.service';
import { RideEvent } from '~shared/components/ride/ride-event';
import { User } from '~shared/components/user/user';
import { UserService } from '~core/user-service/user.service';
import { RideService } from '~core/rides/rides.service';
import { Ride } from '~shared/components/ride/ride';
import { RideUser } from '~shared/components/ride/ride-user';
import { RideUserService } from '~core/rides/ride-user.service';
import { DatePipe } from '@angular/common';
import { AppConstants } from '~shared/components/constantes/app-constants';
import { RideRequestHistory } from '~shared/components/ride/ride-request-history';
import { IntechActionNotifierService } from '@intech/intech-core';
import { MatDialog } from '@intech/intech-ui';
import { DeleteModalComponent } from '~shared/components/delete-modal/delete-modal.component';

/**
 * Components to manage ride request
 */
@Component({
    selector: 'my-bookings',
    templateUrl: './my-bookings.page.html',
    styleUrls: ['./my-bookings.page.scss', '../../sass/custom.scss'],
    providers: [RideEventService, RideRequestHistoryService]

})
export class MyBookingsPage implements OnInit {

    public currentUser: User;
    public newRideEvent = new RideEvent();
    public ridesAccepted: Ride[] = [];
    public ridesRejected: Ride[] = [];
    public ridesRequested: Ride[] = [];
    public allRides: Ride[];

    public currentRideRequestHistory: RideRequestHistory;
    public rideUser: RideUser;


    constructor(
        private readonly datePipe: DatePipe,
        private readonly rideUserService: RideUserService,
        private readonly rideRequestHistoryService: RideRequestHistoryService,
        private readonly userService: UserService,
        public readonly dialog: MatDialog,
        private readonly notifierService: IntechActionNotifierService,
        private readonly _appToasterService: IntechAppNotifierToaster,
        private readonly rideService: RideService
    ) { }

    /**
     * Function which load ride events list and ride request history list from the api
     */
    refresh(): Promise<boolean> {
        return this.getAllRides().then(() => {
            this.getFilteredRides();
            return Promise.resolve(true);
          }, error => {
            this._appToasterService.subscribeAll();
            return Promise.resolve(false);
          });
    }

    ngOnInit() {
        this.userService.getCurrentUser().then(
            currentUser => {
                this.currentUser = currentUser;
                this.refresh();
                Promise.resolve();
            },
            error => Promise.reject(error)
        );
    }

    getAllRides(): Promise<void> {

      return this.rideService.getAllRides().then(
        (rides: Ride[]) => {
            this.allRides = rides;
            Promise.resolve();

         },
         error => Promise.reject(error)

         );
    }
    /**
     * Function which call ride request history service to charge requested rides history list
     *
     * Only history associated with the current ride is kept.
     */
    getCurrentRideRequestHistory(currentRide: Ride): Promise<void> {
        return this.rideRequestHistoryService.getRideRequestHistoryByRideId(currentRide.id).then(
        (rideRequest: RideRequestHistory) => {
            this.currentRideRequestHistory = rideRequest;
            Promise.resolve();
        },
        error => Promise.reject(error)
        );
    }

    getFilteredRides(): void {

      this.ridesAccepted = [];
      this.ridesRejected = [];
      this.ridesRequested = [];
      this.allRides.forEach((ride: Ride) => {
        const rideToPush = ride;
        _.forEach(ride.users, (rideUser: RideUser) => {
            if (rideUser.user.id === this.currentUser.id) {
              if (rideUser.status === RideRequestStatusEnum.RIDE_ACCEPTED) {
                this.ridesAccepted.push(rideToPush);
              } else if (rideUser.status === RideRequestStatusEnum.RIDE_REJECTED) {
                this.ridesRejected.push(rideToPush);
              } else if (rideUser.status === RideRequestStatusEnum.RIDE_REQUESTED) {
                this.ridesRequested.push(rideToPush);
              }
            }
      });
    });
  }

  public openDialog(ride: Ride): void {
    const dialogRef = this.dialog.open(DeleteModalComponent, {
      width: '200px',
    });
    dialogRef.afterClosed().subscribe((response: boolean ) => {
       if (response) {
        this.abandonRide(ride);
       }
    });
  }



    /**
     * Function which abandon a ride.
     *
     * The function create a new ride event with the status "abandonned" and the current date, and add it to the ride request history.
     * @param rideEvent ride event we want to abandon
     */
    abandonRide(ride: Ride): void {
        this.getCurrentRideRequestHistory(ride).then(() => {

            ride.users.forEach (rideUser => {
                if (rideUser.user.id === this.currentUser.id) {
                    this.rideUser = rideUser;
                }
            });

            this.rideUser.status = RideRequestStatusEnum.RIDE_ABANDONNED;
            this.rideUser.date = this.datePipe.transform(Date.now(), AppConstants.DATE_FORMAT);

            this.newRideEvent.comment = this.rideUser.comment;
            this.newRideEvent.date = this.datePipe.transform(Date.now(), AppConstants.DATE_FORMAT);
            this.newRideEvent.status = RideRequestStatusEnum.RIDE_ABANDONNED;
            this.newRideEvent.user = this.rideUser.user;
            this.currentRideRequestHistory.rideEvents.push(this.newRideEvent);

            this.rideUserService.updateRideUser(this.rideUser.id , ride.id, this.rideUser).catch(error =>
              this.notifierService.notifyError(error.error.code));

            this.rideRequestHistoryService.updateRideRequestHistory(this.currentRideRequestHistory.id, this.currentRideRequestHistory)
            .then(() => {
                this.refresh();
            });
        });
        this.refresh();
    }

}
