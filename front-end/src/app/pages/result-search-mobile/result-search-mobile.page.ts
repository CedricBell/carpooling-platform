import { OnInit, Component } from '@angular/core';
import { RideService } from '~core/rides/rides.service';
import { UserService } from '~core/user-service/user.service';
import { Ride } from '~shared/components/ride/ride';
import { User } from '~shared/components/user/user';
import { SearchService } from '~core/search/search.service';
import { RideFilterPipe } from '~shared/pipes/filter.pipe';
import { Router } from '@angular/router';
import { Location } from '@angular/common';



@Component({
  selector: 'result-search-mobile',
  templateUrl: './result-search-mobile.page.html',
  styleUrls: ['result-search-mobile.page.scss', '../../sass/custom.scss'],
  providers: [
  ]
})
export class ResultSearchMobilePage implements OnInit {

  constructor(
      private readonly rideService: RideService,
      private readonly userService: UserService,
      private readonly searchService: SearchService,
      private readonly router: Router,
      private readonly location: Location,
      private readonly rideFilterPipe: RideFilterPipe
  ) {
  }

  rideFilter: Ride = new Ride();
  currentUser: User;
  rides: Ride[];

  ngOnInit(): void {

    this.rideFilter = this.searchService.getData();
  }





  updateSearch(): void {

    this.location.back();
  }


  numberParticipantsInRide(ride: Ride) {


  }
}
