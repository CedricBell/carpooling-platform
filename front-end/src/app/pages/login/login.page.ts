import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { IntechAppAuthService } from '~core/authentication/intech-app-auth.service';
import { AuthGuardService } from '~core/authentication/authguard.service';
import { AppConstants } from '~shared/components/constantes/app-constants';
import { IntechActionNotifierService } from '@intech/intech-core';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.css']
})
export class LoginPage {
    model: any = {};
    error: string;

    constructor(
        private readonly router: Router,
        private readonly authenticationService: IntechAppAuthService,
        private readonly checkAuthService: AuthGuardService,
        private readonly notifierService: IntechActionNotifierService) {
          if (this.authenticationService.currentUserValue) {
            this.router.navigate([AppConstants.ROUTE_HOME]);
          }
        }

    login() {
        this.authenticationService.login(this.model.username, this.model.password).then((result) => {
            if (this.checkAuthService.canActivate()) {
                this.notifierService.notifySuccess(AppConstants.SUCCESSFULL_LOGIN);
                this.router.navigate([AppConstants.ROUTE_HOME]);
            } else {
                this.error = 'Bad credentials';
                this.notifierService.notifyError(AppConstants.ERROR_LOGIN);
            }
        }, error => this.notifierService.notifyError(error.error.code));
    }
}

