import { Injectable } from '@angular/core';
import { ParkingHistory } from '~shared/components/parking/parking-history';
import { IntechHttpService } from '@intech/intech-core';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import { KEYS } from '~core/service.keys.constants';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';



/**
 * Service which manage all parking history request to the api
 */
@Injectable()
export class ParkingHistoryService {

  constructor(
    private readonly intechHttpService: IntechHttpService,
    private readonly intechHttpOptionsService: IntechHttpOptionsService) { }

  /**
   * Function which return all parking history list
   */
  getAllParkingHistory(): Promise<ParkingHistory[]> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.PARKING_HISTORY.GET.ALL);
    return this.intechHttpService.get<ParkingHistory[]>(callOps);
  }

  /**
   * Function which return a parking history
   * @param id id of the desired parking history
   */
  getParkingHistory(id: string): Promise<ParkingHistory> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.PARKING_HISTORY.GET.GET_BY_ID, id);
    return this.intechHttpService.get<ParkingHistory>(callOps);
  }

  /**
   * Function which update a parking history
   * @param id of the updated parking history
   * @param parkingHistory new parking history
   */
  updateParkingHistory(id: string, parkingHistory: ParkingHistory): Promise<void> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.PARKING_HISTORY.UPDATE, id);
    return this.intechHttpService.put(parkingHistory, callOps);
  }

  /**
   * Function which add a parking history
   * @param parkingHistory added
   */
  addParkingHistory(parkingHistory: any): Promise<void> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.PARKING_HISTORY.ADD);
    return this.intechHttpService.post(parkingHistory, callOps);
  }

  /**
   * Function which remove a parking history
   * @param id id of the removed parking history
   */
  removeParkingHistory(id: string): Promise<ParkingHistory> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.PARKING_HISTORY.DELETE, id);
    return this.intechHttpService.delete(callOps);

  }

}
