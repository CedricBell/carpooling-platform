import { Injectable } from '@angular/core';
import { ParkingPlace } from '~shared/components/parking/parking-place';
import { IntechHttpService } from '@intech/intech-core';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import { ParkingHistory } from '~shared/components/parking/parking-history';
import { KEYS } from '~core/service.keys.constants';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';


/**
 * Service which manage all parking request to the api
 */
@Injectable()
export class ParkingPlaceService {

  constructor(
    private readonly intechHttpService: IntechHttpService,
    private readonly intechHttpOptionsService: IntechHttpOptionsService) { }

  /**
   * Function which return all parking places
   */
  getParkingPlaces(): Promise<ParkingPlace[]> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.PARKING.GET.ALL);
    return this.intechHttpService.get<ParkingPlace[]>(callOps);
  }

  /**
   * Function which return a parking place
   * @param id id of the desired parking place
   */
  getParkingPlace(id: string): Promise<ParkingPlace> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.PARKING.GET.GET_BY_ID, id);
    return this.intechHttpService.get<ParkingPlace>(callOps);
  }

  /**
   * Function which update a parking place
   * @param id id of the updated parking place
   * @param history history add when the parking place is updated , the new parking place is within the parking history
   */
  updateParkingPlace(id: string, history: ParkingHistory): Promise<void> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.PARKING.UPDATE, id);
    return this.intechHttpService.put(history, callOps);
  }

  /**
   * Function which add a parking place
   * @param parkingPlace place added
   */
  addParkingPlace(parkingPlace: any): Promise<ParkingPlace> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.PARKING.ADD);
    return this.intechHttpService.post(parkingPlace, callOps);
  }

}
