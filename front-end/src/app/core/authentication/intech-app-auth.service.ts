import { Injectable } from '@angular/core';
import { HttpRequest } from '@angular/common/http';
import { IntechAuthService, IntechHttpService, IntechActionNotifierService } from '@intech/intech-core';
import { NGXLogger } from 'ngx-logger';
import { IntechAppConfigService } from '~core/configuration/intech-app-config.service';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import { Token } from '~shared/components/token/token';
import * as jwt_decode from 'jwt-decode';
import { TOKEN_NAME, KEYS } from '~core/service.keys.constants';
import { BehaviorSubject, Observable } from 'rxjs';




@Injectable()
export class IntechAppAuthService extends IntechAuthService {

  public token: string;
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(
    logger: NGXLogger,
    intechHttpService: IntechHttpService,
    intechConfig: IntechAppConfigService,
  ) {
    super(logger, intechHttpService, intechConfig);
    // set token if saved in local storage
    const currentUser = JSON.parse(localStorage.getItem(TOKEN_NAME));
    this.token = currentUser && currentUser.token;
    this.currentUserSubject = new BehaviorSubject<any>(currentUser);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
}

  public applySecurityOnRequest(req: HttpRequest<any>): Promise<HttpRequest<any>> {
    return new Promise(resolve => {
      // Add specific header on secured request
      const headers = req.headers.set('Authorization', 'Bearer ' + this.token);
      resolve(req.clone({ headers }));
      resolve(req);
    });
  }

  public login(username: string, pswd: string): Promise<void> {

    const callOps: IntechHttpOptions = {
      hasBody: true,
      reportProgress: true,
      retryAttempt: 0,
      secured: false,
      serviceKey: KEYS.LOGIN
    };

    return this._intechHttpService

      .post({ username, password: pswd }, callOps)
      .then((response: Token) => {
        if (response) {
          this.token = response.token;
          localStorage.setItem(TOKEN_NAME, JSON.stringify({ username, token: this.token }));
          this.currentUserSubject.next(response);
          Promise.resolve();
        }
      }
      );
  }

  getToken(): string {
    if (localStorage.length !== 0) {
      return JSON.parse(localStorage.getItem(TOKEN_NAME)).token;
    }
    return null;
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);
    if (decoded.exp === undefined) {
      return null;
    }
    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if (!token) {
      token = this.getToken();
    }
    if (!token) {
      return true;
    }
    const date = this.getTokenExpirationDate(token);
    if (date === undefined) {
      return false;
    }
    return !(date.valueOf() > new Date().valueOf());
  }


  public logout(): void {
    console.log('ici');
    this.token = null;
    this.currentUserSubject.next(null);
    localStorage.removeItem(TOKEN_NAME);
  }

}
