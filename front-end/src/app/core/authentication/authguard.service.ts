import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { IntechAppAuthService } from '~core/authentication/intech-app-auth.service';
import { IntechActionNotifierService } from '@intech/intech-core';

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(
        public authService: IntechAppAuthService,
        public router: Router,
        private readonly notifierService: IntechActionNotifierService) { }

    canActivate(): boolean {
        if (this.authService.isTokenExpired()) {
            this.notifierService.notifyError('error.tokenexpired');
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }

}
