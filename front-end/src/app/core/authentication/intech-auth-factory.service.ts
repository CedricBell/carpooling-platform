import * as _ from 'lodash';
import { IntechAuthService, IntechHttpService } from '@intech/intech-core';
import { IntechAppAuthService } from './intech-app-auth.service';
import { NGXLogger } from 'ngx-logger';
import { IntechAppConfigService } from '~core/configuration/intech-app-config.service';

export let intechAuthFactory = (
  _logger: NGXLogger,
  _intechHttpService: IntechHttpService,
  _intechConfig: IntechAppConfigService
): IntechAuthService => {
  return new IntechAppAuthService(_logger, _intechHttpService, _intechConfig);
};
