import { IntechHttpService } from '@intech/intech-core';
import { Injectable } from '@angular/core';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import { SwPush } from '@angular/service-worker';
import * as firebase from 'firebase';
import 'firebase/messaging';
import { Subject } from 'rxjs';
import { Notification } from '~shared/components/notification/notification';
import { KEYS } from '~core/service.keys.constants';
import { environment } from '../../../environments/environment.default';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';
import { AppConstants } from '~shared/components/constantes/app-constants';

@Injectable()
export class NotifService {

    private messaging: firebase.messaging.Messaging = null;
    private messageSource = new Subject();
    currentMessage = this.messageSource.asObservable();


    constructor(
        private swPush: SwPush,
        private http: IntechHttpService,
        private readonly intechHttpOptionsService: IntechHttpOptionsService
        ) {

        navigator.serviceWorker.getRegistration().then(registration => {
            if (!!registration && registration.active && registration.active.state
                && registration.active.state === AppConstants.STATE_ACTIVATED) {
                firebase.initializeApp(environment.firebaseConfig);
                this.messaging = firebase.messaging();
                this.messaging.useServiceWorker(registration);
                this.messaging.usePublicVapidKey(environment.VAPID_PUBLIC_KEY);
            }
        });
    }



    subscribeToPush(id: string) {
        if (this.swPush.isEnabled) {
            this.swPush.requestSubscription({ serverPublicKey: environment.VAPID_PUBLIC_KEY }).then((response) => {
                this.messaging.getToken().then((token) => {
                    const subscription = new Notification(token, id, 'tentative de souscription', '');
                    this.sendToServer(subscription);
                });
            }).catch(error => {
                console.log(error);
            });
        }
    }

    sendToServer(params: Notification): Promise<any> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.NOTIFICATION.REGISTER);
        return this.http.post(params, callOps);
    }

    receiveMessages() {
        this.messaging.onMessage(payload => {
            this.messageSource.next(payload);
        });
    }

    sendToToken(notif: Notification) {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.NOTIFICATION.TOKEN);
        this.http.post(notif, callOps);
    }
}
