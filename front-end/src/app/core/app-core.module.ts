import { LoggerModule, NGXLogger } from 'ngx-logger';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { TranslateModule, TranslateLoader, TranslateCompiler } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  IntechCoreModule,
  IntechLoggerInterceptor,
  IntechAuthInterceptor,
  IntechUrlService,
  IntechConfigurationService,
  translateCompilerFactory,
  IntechHttpService
} from '@intech/intech-core';
import { IntechUiModule } from '@intech/intech-ui';
import { IntechInternalUiModule } from '@intech/intech-internal-ui';
import { IntechInternalCoreModule } from '@intech/intech-internal-core';
import { IntechAppAuthService } from './authentication/intech-app-auth.service';
import { IntechAppConfigService } from './configuration/intech-app-config.service';
import { configServiceFactory } from './configuration/intech-config-factory.service';
import { intechAuthFactory } from './authentication/intech-auth-factory.service';
import { IntechAppLoggerService } from './logging/intech-app-logger.service';
import { intechLoggerFactory } from './logging/intech-logger-factory.service';
import { IntechAppNotifierToaster } from './notifier-toaster/intech-notifier-toaster.service';
import { ParkingHistoryService } from './parking/parking-history.service';
import { ParkingPlaceService } from './parking/parking-place.service';
import { UserService } from './user-service/user.service';
import { AuthGuardService } from './authentication/authguard.service';
import { RideService } from './rides/rides.service';
import { RideEventService } from './rides/ride-event.service';
import { RideRequestHistoryService } from './rides/ride-request-history.service';
import { RideUserService } from './rides/ride-user.service';
import { CarService } from './car/car.service';
import { SearchService } from './search/search.service';
import { AddressService } from './address/address.service';
import { AutocompletionService } from './map/autocompletion.service';
import { DrawMapService } from './map/draw-map.service';
import { InitializeMapService } from './map/initialize-map.service';
import { NotifService } from './notifier-sw/notifier-sw.service';
import { IntechHttpOptionsService } from './intech-http-options.service';

/**
 * Core providers
 */
export const APP_CORE_PROVIDERS = [
  {
    provide: IntechAppAuthService,
    useFactory: intechAuthFactory,
    deps: [NGXLogger, IntechHttpService, IntechConfigurationService]
  },

  {
    provide: IntechAppLoggerService,
    useFactory: intechLoggerFactory,
    deps: [NGXLogger]
  },

  {
    provide: IntechConfigurationService,
    useFactory: configServiceFactory,
    deps: [NGXLogger]
  },

  { provide: HTTP_INTERCEPTORS, useClass: IntechLoggerInterceptor, deps: [IntechAppLoggerService], multi: true },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: IntechAuthInterceptor,
    deps: [IntechAppAuthService, IntechConfigurationService],
    multi: true
  },
  // ------------------ PLATFORM RELATED ------------------
  { provide: LocationStrategy, useClass: HashLocationStrategy },

  // ------------------  APP SERVICES ------------------
  IntechAppConfigService,
  IntechAppNotifierToaster,
  RideService,
  UserService,
  CarService,
  AddressService,
  ParkingHistoryService,
  ParkingPlaceService,
  AuthGuardService,
  RideEventService,
  RideRequestHistoryService,
  RideUserService,
  NotifService,
  SearchService,
  DrawMapService,
  InitializeMapService,
  AutocompletionService,
  IntechHttpOptionsService
  /** ADD YOUR APPLICATION SERVICES HERE ! */
];

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

/**
 * Core module definition
 */
export const appCoreModuleDef = {
  imports: [
    HttpClientModule,
    IntechCoreModule.forRoot(),
    IntechUiModule.forRoot(),
    IntechInternalUiModule.forRoot(),
    IntechInternalCoreModule.forRoot(),
    LoggerModule.forRoot({ level: 1 }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient, IntechUrlService, IntechConfigurationService]
      },
      compiler: {
        provide: TranslateCompiler,
        useFactory: translateCompilerFactory,
        deps: [IntechConfigurationService]
      }
    }),
  ],
  exports: [],
  declarations: [],
  providers: []
};

@NgModule(appCoreModuleDef)
export class AppCoreModule {
  /* make sure CoreModule is imported only by one NgModule the AppModule */
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: AppCoreModule
  ) {
    if (parentModule) {
      throw new Error('AppCoreModule is already loaded. Import only in AppModule');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppCoreModule,
      providers: [...APP_CORE_PROVIDERS]
    };
  }
}
