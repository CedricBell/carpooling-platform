import { Injectable } from '@angular/core';
import { IntechHttpService } from '@intech/intech-core';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import { User } from '~shared/components/user/user';
import { KEYS } from '~core/service.keys.constants';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';

@Injectable()
export class UserService {

    constructor(
        private readonly intechHttpService: IntechHttpService,
        private readonly intechHttpOptionsService: IntechHttpOptionsService) { }

    public getAllUsers(): Promise<User[]> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.USERS.GET.ALL);
        return this.intechHttpService.get<User[]>(callOps);
    }

    public getCurrentUser(): Promise<User> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.USERS.GET.ME);
        return this.intechHttpService.get<User>(callOps);
    }

    public getUserById(id: string): Promise<User> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.USERS.GET.GET_BY_ID, id);
        return this.intechHttpService.get<User>(callOps);
    }

    public updateUser(user: User, id: string): Promise<User> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.USERS.UPDATE, id);
        return this.intechHttpService.put(user, callOps);
    }

}
