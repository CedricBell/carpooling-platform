import { Injectable } from '@angular/core';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';

@Injectable()
export class IntechHttpOptionsService {

    intechHttpOptions: IntechHttpOptions;

    constructor( ) { }

    setIntechHttpOptions( serviceKeyConstant: string, id?: string, rideId?: string ): IntechHttpOptions {
        this.intechHttpOptions = {
            hasBody: true,
            reportProgress: true,
            retryAttempt: 0,
            secured: true,
            serviceKey: serviceKeyConstant,
        };
        console.log ('id,rideId', id, rideId);
        if (id && rideId) {
            this.intechHttpOptions.urlParams = {id, rideId};
        }
        if (id && !rideId) {
            this.intechHttpOptions.urlParams = {id};
        }
        if (!id && rideId) {
            this.intechHttpOptions.urlParams = {rideId};
        }
        return this.intechHttpOptions;
    }
}
