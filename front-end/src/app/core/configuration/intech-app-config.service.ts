import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { environment as env_env } from '../../../environments/environment';
import { environment as env_default } from '../../../environments/environment.default';
import { IntechConfigurationService } from '@intech/intech-core';

@Injectable()
export class IntechAppConfigService extends IntechConfigurationService {
  constructor() {
    super();

    const parentEnv = super.getEnv();
    this.environment = _.defaultsDeep(env_env, env_default, parentEnv);
  }

  /**
   * Get the current environment
   */
  public getEnv() {
    return this.environment;
  }
}
