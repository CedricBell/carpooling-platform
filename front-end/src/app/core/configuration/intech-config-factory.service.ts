import * as _ from 'lodash';
import { IntechAppConfigService } from './intech-app-config.service';
import { IntechConfigurationService } from '@intech/intech-core';

export let configServiceFactory = (): IntechConfigurationService => {
  return new IntechAppConfigService();
};
