import { IntechHttpService } from '@intech/intech-core';
import { Injectable } from '@angular/core';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import { Ride } from '~shared/components/ride/ride';
import { KEYS } from '~core/service.keys.constants';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';

@Injectable()
export class RideService {

    constructor(
        private readonly intechHttpService: IntechHttpService,
        private readonly intechHttpOptionsService: IntechHttpOptionsService
        ) { }

    public addRide(ride: Ride): Promise<Ride> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDES.ADD);
        return this.intechHttpService.post(ride, callOps);
    }

    getRideById(id: string): Promise<Ride> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDES.GET.GET_BY_ID, id);
        return this.intechHttpService.get<Ride>(callOps);
    }

    public getAllRides(): Promise<Ride[]> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDES.GET.ALL);
        return this.intechHttpService.get<Ride[]>(callOps);
    }

    public getAllMyRides(): Promise<Ride[]> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDES.GET.ME);
        return this.intechHttpService.get(callOps);
    }

    public deleteRide(id: string): Promise<Ride> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDES.DELETE, id);
        return this.intechHttpService.delete(callOps);
    }

    updateRide(ride: Ride, id: string): Promise<Ride> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDES.UPDATE, id);
        return this.intechHttpService.put(ride, callOps);
    }
}
