import { RideEvent } from '~shared/components/ride/ride-event';
import { RideRequestHistory } from '~shared/components/ride/ride-request-history';
import { IntechHttpService } from '@intech/intech-core';
import { Injectable } from '@angular/core';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import { KEYS } from '~core/service.keys.constants';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';

@Injectable()
export class RideEventService {

  constructor(
    private readonly intechHttpService: IntechHttpService,
    private readonly intechHttpOptionsService: IntechHttpOptionsService) { }

  getRideEvents(rideId: string): Promise<RideEvent[]> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_EVENT.GET.ALL, undefined, rideId);
    return this.intechHttpService.get<RideEvent[]>(callOps);
  }

  getRideEvent(id: string , rideId: string): Promise<RideEvent> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_EVENT.GET.GET_BY_ID, id, rideId);
    return this.intechHttpService.get<RideEvent>(callOps);
  }

  updateRideEvent(id: string,  rideId: string , history: RideRequestHistory): Promise<void> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_EVENT.UPDATE, id, rideId);
    return this.intechHttpService.put(history, callOps);
  }

  addRideEvent(rideEvent: RideEvent, rideId: string): Promise<RideEvent> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_EVENT.ADD, undefined, rideId);
    return this.intechHttpService.post(rideEvent, callOps);
  }

}
