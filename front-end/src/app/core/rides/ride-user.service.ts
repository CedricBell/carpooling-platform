import { IntechHttpService } from '@intech/intech-core';
import { Injectable } from '@angular/core';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import { KEYS } from '~core/service.keys.constants';
import { RideUser } from '~shared/components/ride/ride-user';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';

@Injectable()
export class RideUserService {

  constructor(
    private readonly intechHttpService: IntechHttpService,
    private readonly intechHttpOptionsService: IntechHttpOptionsService) { }

  getRideUsers( rideId: string): Promise<RideUser[]> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_USER.GET.ALL, undefined, rideId);
    return this.intechHttpService.get<RideUser[]>(callOps);
  }

  getRideUser(id: string ,  rideId: string): Promise<RideUser> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_USER.GET.GET_BY_ID, id, rideId);
    return this.intechHttpService.get<RideUser>(callOps);
  }

  updateRideUser(id: string , rideId: string , newRideUser: RideUser): Promise<RideUser> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_USER.UPDATE, id, rideId);
    return this.intechHttpService.put(newRideUser , callOps);
  }

  addRideUser(rideUser: RideUser , rideId: string): Promise<RideUser> {
    const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_USER.ADD, undefined, rideId);
    return this.intechHttpService.post(rideUser, callOps);
  }

}
