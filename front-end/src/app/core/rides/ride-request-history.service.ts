import { RideRequestHistory } from '~shared/components/ride/ride-request-history';
import { IntechHttpService } from '@intech/intech-core';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import { Injectable } from '@angular/core';
import { KEYS } from '~core/service.keys.constants';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';

@Injectable()
export class RideRequestHistoryService {

    constructor(
      private readonly intechHttpService: IntechHttpService,
      private readonly intechHttpOptionsService: IntechHttpOptionsService) {}

    getAllRideRequestHistory(): Promise<RideRequestHistory[]> {
      const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_REQUEST_HISTORY.GET.ALL);
      return this.intechHttpService.get<RideRequestHistory[]>(callOps);
      }

      getRideRequestHistory(id: string): Promise<RideRequestHistory> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_REQUEST_HISTORY.GET.GET_BY_ID, id);
        return this.intechHttpService.get<RideRequestHistory>(callOps);
      }

      getRideRequestHistoryByRideId(id: string): Promise<RideRequestHistory> {
        const callOps: IntechHttpOptions =
          this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_REQUEST_HISTORY.GET.GET_BY_RIDE_ID, id);
        return this.intechHttpService.get<RideRequestHistory>(callOps);
      }

      updateRideRequestHistory(id: string, rideRequestHistory: RideRequestHistory): Promise<object> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_REQUEST_HISTORY.UPDATE, id);
        return this.intechHttpService.put(rideRequestHistory, callOps);
      }

      addRideRequestHistory(rideRequestHistory: RideRequestHistory): Promise<object> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_REQUEST_HISTORY.ADD);
        return this.intechHttpService.post(rideRequestHistory, callOps);
      }

      removeRideRequestHistory(id: string): Promise<object> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.RIDE_REQUEST_HISTORY.DELETE, id);
        return this.intechHttpService.delete(callOps);

    }

}
