import { Injectable } from '@angular/core';
import { IntechHttpService } from '@intech/intech-core';
import { Car } from '~shared/components/car/car';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import { KEYS } from '~core/service.keys.constants';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';

@Injectable()
export class CarService {

    constructor(
        private readonly intechHttpService: IntechHttpService,
        private readonly intechHttpOptionsService: IntechHttpOptionsService
        ) { }

    public addCar(car: Car): Promise<Car> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.CAR.ADD);
        return this.intechHttpService.post(car, callOps);
    }

    public getCarById(id: string): Promise<Car> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.CAR.GET.GET_BY_ID, id);
        return this.intechHttpService.get<Car>(callOps);
    }

    public getAllCars(): Promise<Car[]> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.CAR.GET.ALL);
        return this.intechHttpService.get<Car[]>(callOps);
    }

    public deleteCar(id: string): Promise<Car> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.CAR.DELETE, id);
        return this.intechHttpService.delete(callOps);
    }

    public updateCar(car: Car, id: string): Promise<Car> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.CAR.UPDATE, id);
        return this.intechHttpService.put(car, callOps);
    }
}
