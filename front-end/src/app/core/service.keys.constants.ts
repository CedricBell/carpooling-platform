/**
 * All keys for WS calls
 * The value of the keys need to be presents in the environments files
 */
export const KEYS = {
  HEALTH_CHECK: 'healthCheck',
  LOGIN: 'login',

  USERS: {
    GET: {
      ALL: 'users.get.all',
      GET_BY_ID: 'users.get.getById',
      ME: 'users.get.me'
    },
    UPDATE: 'users.update',
    ADD: 'users.add',
    DELETE: 'users.delete'
  },

  ADDRESS: {
    GET: {
      ALL: 'address.get.all',
      GET_BY_ID: 'address.get.getById'
    },
    UPDATE: 'address.update',
    ADD: 'address.add',
    DELETE: 'address.delete'
  },

  CAR: {
    GET: {
      ALL: 'car.get.all',
      GET_BY_ID: 'car.get.getById'
    },
    UPDATE: 'car.update',
    ADD: 'car.add',
    DELETE: 'car.delete'
  },

  NONCE: 'nonces.getOne',

  PARKING: {
    GET: {
      ALL: 'parking.get.all',
      GET_BY_ID: 'parking.get.getById',
    },
    UPDATE: 'parking.update',
    ADD: 'parking.add',
    DELETE: 'parking.delete'
  },

  PARKING_HISTORY: {
    GET: {
      ALL: 'parkingHistory.get.all',
      GET_BY_ID: 'parkingHistory.get.getById',
    },
    UPDATE: 'parkingHistory.update',
    ADD: 'parkingHistory.add',
    DELETE: 'parkingHistory.delete'
  },

  RIDES: {
    GET: {
      ALL: 'rides.get.all',
      GET_BY_ID: 'rides.get.getById',
      ME: 'rides.get.me',
    },
    UPDATE: 'rides.update',
    ADD: 'rides.add',
    DELETE: 'rides.delete'
  },

  RIDE_EVENT: {
    GET: {
      ALL: 'rideEvent.get.all',
      GET_BY_ID: 'rideEvent.get.getById'
    },
    UPDATE: 'rideEvent.update',
    ADD: 'rideEvent.add',
  },

  RIDE_REQUEST_HISTORY: {
    GET: {
      ALL: 'rideRequestHistory.get.all',
      GET_BY_ID: 'rideRequestHistory.get.getById',
      GET_BY_RIDE_ID: 'rideRequestHistory.get.getByRideId'
    },
    UPDATE: 'rideRequestHistory.update',
    ADD: 'rideRequestHistory.add',
    DELETE: 'rideRequestHistory.delete'
  },

  RIDE_USER: {
    GET: {
      ALL: 'rideUser.get.all',
      GET_BY_ID: 'rideUser.get.getById'
    },
    UPDATE: 'rideUser.update',
    ADD: 'rideUser.add',
  },
  NOTIFICATION: {
    REGISTER: 'notification.register',
    TOKEN: 'notification.token'
  }
};

export const TOKEN_NAME = 'CurrentUser';
