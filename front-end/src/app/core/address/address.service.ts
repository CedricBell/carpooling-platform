import { Injectable } from '@angular/core';
import { IntechHttpService } from '@intech/intech-core';
import { Address } from '~shared/components/address/address';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import { KEYS } from '~core/service.keys.constants';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';

@Injectable()
export class AddressService {

    constructor(
        private readonly intechHttpService: IntechHttpService,
        private readonly intechHttpOptionsService: IntechHttpOptionsService
        ) { }

    public addAddress(address: Address): Promise<Address> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.ADDRESS.ADD);
        return this.intechHttpService.post(address, callOps);
    }

    public getAddressById(id: string): Promise<Address> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.ADDRESS.GET.GET_BY_ID, id);
        return this.intechHttpService.get<Address>(callOps);
    }

    public getAllAddresss(): Promise<Address[]> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.ADDRESS.GET.ALL);
        return this.intechHttpService.get<Address[]>(callOps);
    }

    public deleteAddress(id: string): Promise<Address> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.ADDRESS.DELETE, id);
        return this.intechHttpService.delete(callOps);
    }

    public updateAddress(address: Address, id: string): Promise<Address> {
        const callOps: IntechHttpOptions = this.intechHttpOptionsService.setIntechHttpOptions(KEYS.ADDRESS.UPDATE, id);
        return this.intechHttpService.put(address, callOps);
    }
}
