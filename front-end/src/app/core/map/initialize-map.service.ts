import * as L from 'leaflet';
import 'leaflet-routing-machine';
import { Address } from '~shared/components/address/address';
import { Injectable } from '@angular/core';

@Injectable()
export class InitializeMapService {

    constructor() { }

    createRedMarker(address: Address): L.Marker {
        return L.marker([+address.latitude, +address.longitude], {
            icon: L.icon({
            iconSize: [ 40, 41 ],
            iconAnchor: [ 22, 41 ],
            iconUrl: 'assets/pin.png'
        })
    }); }


    createBlueMarker(address: Address): L.Marker {
        return L.marker([+address.latitude, +address.longitude], {
            icon: L.icon({
            iconSize: [ 40, 41 ],
            iconAnchor: [ 22, 41 ],
            iconUrl: 'assets/bluepin.png'
            })
        });
    }

    setStreetMaps() {
        return L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            detectRetina: true,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });
    }

    setWikiMaps() {
        return L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
            detectRetina: true,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });
    }
}
