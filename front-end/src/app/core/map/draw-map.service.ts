import { Injectable } from '@angular/core';
import { IntechHttpService } from '@intech/intech-core';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import OlPolylines from 'ol/format/Polyline';
import OlFeature from 'ol/Feature';
import OlPoint from 'ol/geom/Point';
import { fromLonLat, toLonLat, transform } from 'ol/proj';
import { Address } from '~shared/components/address/address';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';



@Injectable()
export class DrawMapService {


    constructor(
        private readonly intechHttpService: IntechHttpService,
        private readonly intechHttpOptionsService: IntechHttpOptionsService) { }

    drawPath(point1: string , point2: string): Promise<any> {

        const callOps: IntechHttpOptions = {
            hasBody: true,
            reportProgress: true,
            retryAttempt: 0,
            secured: true,
            url : 'https://router.project-osrm.org/route/v1/driving/' + point1 + ';' + point2

        };
        return this.intechHttpService.get<any>(callOps);
    }



    createFeature(addresse: Address, map/*: Map*/ ): void {
        const feature = new OlFeature({
            type: 'place',
            geometry: new OlPoint(fromLonLat([+addresse.longitude, +addresse.latitude]))
        });
        feature.setStyle(map.styles.icon);
        map.vectorSource.addFeature(feature);
    }

/*
    createFeature(coord: number[], map: Map): void {
        const feature = new OlFeature();
        feature.type = 'place';
        feature.geometry = new OlPoint(fromLonLat([+coord[0], +coord[1]]));
        console.log(feature);
        feature.setStyle(map.styles.icon);
        map.vectorLayer.vectorSource.addFeature(feature);
        map.vectorSource.addFeature(feature);
        console.log(map.vectorSource.feature);
    }
*/
    /*
     createRoute(polyline): void {
        // route is ol.geom.LineString
        const route = new OlPolylines({
            factor: 1e5
        }).readGeometry(polyline, {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857'
        });
        const feature = new OlFeature({
            type: 'route',
            geometry: route
        });
        feature.setStyle(this.styles.route);
        this.vectorSource.addFeature(feature);
    }
*/

    // Pour la carte
    createRoute(polyline, map/*: Map*/ ): void {
        // route is ol.geom.LineString
        const route = new OlPolylines({
            factor: 1e5
        }).readGeometry(polyline, {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857'
        });
        const feature = new OlFeature({
            type: 'route',
            geometry: route
        });
        feature.setStyle(map.styles.route);
        map.vectorSource.addFeature(feature);
    }

    // Pour la carte
    drawRide(map/*: Map*/, startAddress: Address, endAddress: Address): void {

        map.centerLongitude = (+startAddress.longitude + +endAddress.longitude) / 2;
        map.centerLatitude = (+startAddress.latitude + +endAddress.latitude) / 2;

        map.map.getView().setCenter(fromLonLat([map.centerLongitude, map.centerLatitude]));
        map.map.getView().setZoom(6);

        // get the route
        const point1 = [startAddress.longitude, startAddress.latitude].join();
        const point2 = [endAddress.longitude, endAddress.latitude].join();

        this.createFeature(startAddress, map);
        this.createFeature(endAddress, map);

        fetch('//router.project-osrm.org/route/v1/driving/' + point1 + ';' + point2)
            .then(r => r.json())
            .then(json => {
                if (json.code !== 'Ok') {
                    // msg_el.innerHTML = 'No route found.';
                    return;
                }
                // msg_el.innerHTML = 'Route added';
                this.createRoute(json.routes[0].geometry, map);
            });

    }

}
