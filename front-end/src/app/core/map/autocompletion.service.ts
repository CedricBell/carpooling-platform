import { Injectable, ɵConsole } from '@angular/core';
import { Ride } from '~shared/components/ride/ride';
import { User } from '~shared/components/user/user';
import { Address } from '~shared/components/address/address';
import { Observable } from 'rxjs';
import { FormControl, AbstractControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import * as Nominatim from 'nominatim-browser';
import { HttpClient, HttpParams, HttpHandler, HttpHeaders } from '@angular/common/http';
import { IntechHttpService } from '@intech/intech-core';
import { IntechHttpOptions } from '@intech/intech-core/lib/intech-http/models/intech-http-options.model';
import _ from 'lodash';
import { DEC } from '@angular/material/core';
import { IntechHttpOptionsService } from '~core/intech-http-options.service';

@Injectable()
export class AutocompletionService {

    public sharedData: Ride;
    public currentUser: User;

    constructor() {
    }
// Pour l'autocomplete
public _filter(name: string, displayAutocompleteList: Address[]): Address[] {
    displayAutocompleteList = [];
    if (name.length < 1) { return [];
    } else {
        Nominatim.geocode({
            q: name,
            addressdetails: true,
            limit: 30
        })
        .then((addressResults: Nominatim.NominatimResponse[]) => {
            if (addressResults.length === 0) { displayAutocompleteList = []; }
            for (const addressResult of addressResults) {
                if (displayAutocompleteList.length < 6) {
                    if (addressResult.address.country_code === 'fr' || addressResult.address.country_code === 'de'
                            || addressResult.address.country_code === 'lu' || addressResult.address.country_code === 'be') {
                        const newAddress = new Address();
                        _.mapKeys(addressResult.address, (value, category) => {
                                if (category !== 'county'  && category !== 'country_code' && category !== 'postcode') {
                                    if (newAddress.name === undefined) {
                                        newAddress.name = value;
                                    } else {
                                        newAddress.name = newAddress.name + ', ' + value;
                                    }
                                }
                            });
                        newAddress.longitude = addressResult.lon;
                        newAddress.latitude = addressResult.lat;
                        displayAutocompleteList.push(newAddress);
                    }
                } else {
                    break;
                }
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
    return displayAutocompleteList;
}

    initializeFilteredDisplayAutocompleteList(control: AbstractControl, displayAutocompleteList: Address[]): Observable<Address[]> {
        return control.valueChanges
        .pipe(
            startWith(''),
            map(value => typeof value === 'string' ? value : value.name),
            map(name => name ? this._filter(name, displayAutocompleteList) : displayAutocompleteList.slice())
        );
    }
}
