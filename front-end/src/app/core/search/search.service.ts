import { Injectable } from '@angular/core';
import { Ride } from '~shared/components/ride/ride';
import { RideUser } from '~shared/components/ride/ride-user';
import { RideRequestStatusEnum } from '~shared/components/ride-request-status-enum';
import { User } from '~shared/components/user/user';
import _ from 'lodash';

@Injectable()
export class SearchService {

  public sharedData: Ride;
  public currentUser: User;

  constructor() {

  }

  setData(data: Ride) {
    this.sharedData = data;
  }
  getData(): Ride {
    return this.sharedData;
  }

  removeData(): void {
    this.sharedData = null;
  }

  isRideFull(ride: Ride): boolean {
    let nbPlacesDispo: number = ride.capacity;

    _.forEach(ride.users, (rideUser: RideUser) => {
      if (rideUser.status === RideRequestStatusEnum.RIDE_ACCEPTED) {
        nbPlacesDispo--;
      }
    });
    return nbPlacesDispo === 0;
  }

  isRideStatusIn(ride: Ride, currentUser: User, status: RideRequestStatusEnum): boolean {
    return ride.users .filter((rideUser: RideUser) =>
    rideUser.user.trigram === currentUser.trigram && rideUser.status === status ) .length !== 0;
  }
/*
  isRideAcceptedOrRejected(ride: Ride , currentUser: User): boolean {

    let rideAcceptedOrRejected = false;
    _.forEach(ride.users, (rideUser: RideUser) => {
      if (rideUser.user.trigram === currentUser.trigram &&
        (rideUser.status === RideRequestStatusEnum.RIDE_ACCEPTED || rideUser.status === RideRequestStatusEnum.RIDE_REJECTED)) {
        rideAcceptedOrRejected = true;
      }
    });

    return rideAcceptedOrRejected;
  }

  isRideRequested(ride: Ride , currentUser: User) {
    let rideRequested = false;
    _.forEach(ride.users, (rideUser: RideUser) => {
      if (rideUser.user.trigram === currentUser.trigram && rideUser.status === RideRequestStatusEnum.RIDE_REQUESTED) {
        rideRequested = true;
      }
    });
    return rideRequested;
  }
*/
  isMyRide(ride: Ride , currentUser: User) {
      return ride.owner.trigram === currentUser.trigram;
    }
}
