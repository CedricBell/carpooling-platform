import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpResponse, HttpEvent } from '@angular/common/http';
import { IntechLoggerService } from '@intech/intech-core';
import { NGXLogger } from 'ngx-logger';
import { finalize, tap, map } from 'rxjs/operators';

@Injectable()
export class IntechAppLoggerService extends IntechLoggerService {
  constructor(_logger: NGXLogger) {
    super(_logger);
  }

  public applyLoggingOnRequest(req: HttpRequest<any>, next: HttpHandler): Promise<HttpEvent<any>> {
    const started = Date.now();
    let result: string;

    return next
      .handle(req)
      .pipe(
        tap(
          event => {
            result = event instanceof HttpResponse ? 'succeeded' : '';
          },
          error => {
            result = 'in error';
            this._logger.debug(`${req.method} "${req.urlWithParams}" ${result}`);
          }
        ),
        finalize(() => {
          const elapsed = Date.now() - started;
          this._logger.debug(`${req.method} "${req.urlWithParams}" ${result} in ${elapsed} ms.`);
        })
      )
      .toPromise();
  }
}
