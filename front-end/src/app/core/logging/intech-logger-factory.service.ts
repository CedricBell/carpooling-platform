import * as _ from 'lodash';
import { IntechAppLoggerService } from './intech-app-logger.service';
import { IntechLoggerService } from '@intech/intech-core';
import { NGXLogger } from 'ngx-logger';

export let intechLoggerFactory = (_logger: NGXLogger): IntechLoggerService => {
  return new IntechAppLoggerService(_logger);
};
