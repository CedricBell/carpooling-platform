import { Injectable, OnDestroy } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Subscription } from 'rxjs';
import { IntechToasterService, IntechToasterModel } from '@intech/intech-ui';
import { IntechActionNotifierService, IntechRemoteHealthCheckService, IntechNetworkConnectivityService } from '@intech/intech-core';
import { TranslateService } from '@ngx-translate/core';
import { AppConstants } from '~shared/components/constantes/app-constants';

// Automatically unsubscribe from arrays of subscriptions
@AutoUnsubscribe({ arrayName: 'subscriptions' })
@Injectable()
export class IntechAppNotifierToaster implements OnDestroy {
  /** SUBSCRIPTIONS */
  private readonly subscriptions: Subscription[] = [];
  private readonly dismiss: string = 'dismiss cb !';
  private readonly aferOpened: string = 'afterOpened cb !';
  private readonly onAction: string = 'onaction cb !';

  constructor(
    private readonly _translate: TranslateService,
    private readonly _toasterService: IntechToasterService,
    private readonly _notifierService: IntechActionNotifierService,
    private readonly _remoteHealthCheckService: IntechRemoteHealthCheckService,
    private readonly _connectivityService: IntechNetworkConnectivityService
  ) { }

  public subscribeAll(): void {
    this._subscribeNotifications();
    this._subscribeRmoteServer();
    this._subscribeConnectivity();
  }

  /** NOTIFICATION SUBSCRIPTIONS */
  private _subscribeNotifications(): void {
    this.subscriptions.push(
      this._notifierService.subscribeToSuccessNotifications(message => {
        this._translate.get(message).subscribe((res: string) => {
          const toasterModel: IntechToasterModel = {
            message: res,
            actionLabel: 'Close',
            configuration: { panelClass: ['test'], duration: 2000 },
            afterDismissCb: () => console.log(this.dismiss),
            afterOpenedCb: () => console.log(this.aferOpened),
            onActionCb: () => console.log(this.onAction)
          };
          this._toasterService.displaySuccessToast(toasterModel);
        });
      })
    );

    this.subscriptions.push(
      this._notifierService.subscribeToWarningNotifications(message => {
        this._translate.get(message).subscribe((res: string) => {
          const toasterModel: IntechToasterModel = {
            message: res,
            actionLabel: 'Close',
            configuration: { panelClass: ['test'], duration: 2000 },
            afterDismissCb: () => console.log(this.dismiss),
            afterOpenedCb: () => console.log(this.aferOpened),
            onActionCb: () => console.log(this.onAction)
          };
          this._toasterService.displayWarningToast(toasterModel);
        });
      })
    );

    this.subscriptions.push(
      this._notifierService.subscribeToErrorNotifications(message => {
        if (message !== 'A technical error occured') {
          this._translate.get(message).subscribe((res: string) => {
            const toasterModel: IntechToasterModel = {
              message: res,
              actionLabel: 'Close',
              configuration: { panelClass: ['test'], duration: 2000 },
              afterDismissCb: () => console.log(this.dismiss),
              afterOpenedCb: () => console.log(this.aferOpened),
              onActionCb: () => console.log(this.onAction)
            };
            this._toasterService.displayErrorToast(toasterModel);
          });
        }
      })
    );
  }

  /** REMOTE SERVER SUBSCRIPTIONS */
  private _subscribeRmoteServer(): void {
    this.subscriptions.push(
      this._remoteHealthCheckService.subscribeRemoteServerState(state => {
        this.notificationAppStatus(state);
      })
    );
  }

  /** CONNECTIVITY SUBSCRIPTIONS */
  private _subscribeConnectivity(): void {
    this.subscriptions.push(
      this._connectivityService.subscribeNetworkState(state => {
        this.notificationAppStatus(state);
      })
    );
  }

  public notificationAppStatus(state: string) {
    const toasterModel: IntechToasterModel = { message: state };
    switch (state) {
      case AppConstants.STATE_OFFLINE:
        this._translate.get('connectivity.offline').subscribe((res: string) => {
          toasterModel.message = res;
          this._toasterService.displayErrorToast(toasterModel);
        });
        break;
      case AppConstants.STATE_ONLINE:
        this._translate.get('connectivity.online').subscribe((res: string) => {
          toasterModel.message = res;
          this._toasterService.displaySuccessToast(toasterModel);
        });
        break;
    }
  }

  ngOnDestroy() {
    // must be present for @Autounsubscribe to work properly
    // @Autounsubscribe decorator automatically unsubscribe from observable subscriptions when the component is destroyed
  }
}
