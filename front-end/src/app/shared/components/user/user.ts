import { Car } from '../car/car';
import { Address } from '../address/address';

export class User {
    public id: string;
    public userNo: string;
    public firstName: string;
    public lastName: string;
    public username: string;
    public email: string;
    public car: Car;
    public trigram: string;
    public address: Address;
    public picture: string;
    public fcmToken: string;

    constructor(
    ) {
    }
}
