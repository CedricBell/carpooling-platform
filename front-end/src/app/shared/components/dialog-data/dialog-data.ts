import { RideRequestHistory } from '../ride/ride-request-history';
import { Ride } from '../ride/ride';
import { User } from '../user/user';
import { Car } from '../car/car';
import { Address } from '../address/address';

export interface DialogData {

  rideRequestHistory?: RideRequestHistory;
  ride?: Ride;
  user?: User;
  car?: Car;
  address?: Address;
}
