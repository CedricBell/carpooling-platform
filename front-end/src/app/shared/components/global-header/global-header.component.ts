import { Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import { Observable } from 'rxjs';
// tslint:disable-next-line: no-implicit-dependencies
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { IntechAppAuthService } from '~core/authentication/intech-app-auth.service';
import { User } from '../user/user';
import { TOKEN_NAME } from '~core/service.keys.constants';
import { Router } from '@angular/router';
import { AppConstants } from '../constantes/app-constants';
/**
 * Global application header
 */
@Component({
  selector: 'global-header',
  templateUrl: 'global-header.component.html',
  styleUrls: ['global-header.component.css'],
})
export class GlobalHeaderComponent  {
  title = 'MovInTech';
  toggle: boolean;
  currentUser: User;

  constructor(
    private readonly breakpointObserver: BreakpointObserver,
    private readonly authenticationService: IntechAppAuthService,
    private readonly authService: IntechAppAuthService,
    private router: Router,
    ) {

      this.toggle = false;
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  logout(): void {
    this.toggleClick();
    this.router.navigate([AppConstants.ROUTE_LOGIN]);
    this.authService.logout();

  }

  toggleClick(): void {

    this.toggle = !this.toggle;
  }

  connectedUser(): string {

    if (localStorage.length === 0) {
      return null;
    } else {
      return JSON.parse(localStorage.getItem(TOKEN_NAME)).username;
    }
  }
}
