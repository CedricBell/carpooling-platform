export class AppConstants {

  public static PARKING_FREE_STATUS = 'Libre';
  public static PARKING_OCCUPATED_STATUS = 'Occupée';
  public static PLACE_RESERVED = 'Place réservée !';
  public static PLACE_FREE = 'Place libre !';
  public static PLACE_ALREADY_OCCUPATED = 'Une place a déja été prise ! ';
  public static RIDE_ADDED = ' Votre trajet a bien été ajouté';
  public static RIDE_MODIFIED = ' Votre trajet a bien été modifié';
  public static RIDE_ABANDONNED = ' Vous avez bien été retiré de la liste des participants';
  public static DELETE_MODAL_CONFIRMATION = 'Votre trajet a été supprimé de la liste des trajets';
  public static DATE_FORMAT = 'yyyy-MM-d H:m';
  public static SUCCESSFULL_LOGIN = 'Vous êtes connecté, bienvenue sur MovInTech ';
  public static ERROR_LOGIN = 'Connexion refusée';
  public static SUCCESSFULL_LOGOUT = 'Vous avez bien été déconnecté, au revoir';
  public static REQUEST_SEND_CONFIRMATION = 'Votre demande a bien été envoyé';
  public static USER_MODIFIED = 'Vos préférences ont été enregistrées';
  public static PROFILE_PICTURE_TOO_LONG = 'L image de profil doit faire moins de 12 Mo';
  public static ROUTING_URL = 'http://plateforme-covoiturage.intech-dev.com:5000/route/v1';

  public static STATE_ACTIVATED = 'activated';
  public static STATE_OFFLINE = 'offline';
  public static STATE_ONLINE = 'online';

  public static ROUTE_HOME = '/home';
  public static ROUTE_ADD_COVOIT = '/add-covoit';
  public static ROUTE_RIDE_EVENT = '/ride-event';
  public static ROUTE_RESULT_SEARCH_MOBILE = '/result-search-mobile';
  public static ROUTE_RIDE_DETAILS = '/ride-details';
  public static ROUTE_SEARCH_COVOIT = '/search-covoit';
  public static ROUTE_LOGIN = '/login';


}
