import { IntechToasterModel } from '@intech/intech-ui';

export class MovInTechToaster implements IntechToasterModel {


  message: string;
  actionLabel?: string;
  configuration?;
  afterDismissCb?: any;
  afterOpenedCb?: any;
  onActionCb?: any;

  constructor(message: string) {

    this.message = message;
    this.actionLabel = 'Close';
    this.configuration = { duration: 3000 };

  }



}
