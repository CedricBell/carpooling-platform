export class Notification {

    public token: string;
    public title: string;
    public message: string;
    public topic: string;

    constructor(
        token: string,
        title: string,
        message: string,
        topic: string
    ) {
        this.token = token;
        this.topic = topic;
        this.title = title;
        this.message = message;
    }
}
