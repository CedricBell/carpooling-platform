import { Component, OnInit, ViewChild, Inject, NgZone } from '@angular/core';
// tslint:disable-next-line: no-implicit-dependencies
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { MatDialogRef, IntechToasterService, MAT_DIALOG_DATA } from '@intech/intech-ui';
import { Router } from '@angular/router';
import { DialogData } from '../dialog-data/dialog-data';
import { UserService } from '~core/user-service/user.service';
import { DatePipe } from '@angular/common';
import { User } from '../user/user';
import { MovInTechToaster } from '../toaster/movInTech-toaster';
import { take } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Address } from '../address/address';
import { AutocompletionService } from '~core/map/autocompletion.service';

@Component({
    selector: 'modify-profile-address-modal.component',
    templateUrl: 'modify-profile-address-modal.component.html',
    styleUrls: ['modify-profile-address-modal.component.scss', '../../../sass/custom.scss']
})
export class ModifyProfileAddressModalComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<ModifyProfileAddressModalComponent>,
        private readonly toasterService: IntechToasterService,
        private readonly router: Router,
        @Inject(MAT_DIALOG_DATA) public dialogData: DialogData,
        private readonly autocompletionService: AutocompletionService,
        private readonly userService: UserService,
        private readonly datePipe: DatePipe,
        private readonly _ngZone: NgZone
    ) {}
    @ViewChild('autosize') autosize: CdkTextareaAutosize;
    currentAddress: Address;
    toastermodel: MovInTechToaster = new MovInTechToaster('');
    displayAutocompleteListAddress: Address[] = [];
    filteredDisplayAutocompleteListAddress: Observable<Address[]>;
    addressControl = new FormControl('');

    triggerResize() {
        this._ngZone.onStable.pipe(take(1)).subscribe(() => this.autosize.resizeToFitContent(true));
      }
    ngOnInit() {
        this.currentAddress = this.dialogData.address;

        this.filteredDisplayAutocompleteListAddress =
            this.autocompletionService.initializeFilteredDisplayAutocompleteList(this.addressControl, this.displayAutocompleteListAddress);

    }
    closeDialog() {
        this.dialogRef.close(this.currentAddress);
    }


    displayAddress(address?: Address): string | undefined {
        return address ? address.name : undefined;
    }
}
