import { MatDialogRef, MAT_DIALOG_DATA } from '@intech/intech-ui';
import { Component, Inject } from '@angular/core';
import { DialogData } from '~shared/components/dialog-data/dialog-data';


@Component({
    selector: 'delete-modal.component',
    templateUrl: 'delete-modal.component.html',
    styleUrls: ['delete-modal.component.scss', '../../../sass/custom.scss']
})
export class DeleteModalComponent {

    constructor(
        public dialogRef: MatDialogRef<DeleteModalComponent>,
        @Inject(MAT_DIALOG_DATA) public dialogData: DialogData) { }

    closeDialog(response: boolean) {
      this.dialogRef.close(response);
    }
}
