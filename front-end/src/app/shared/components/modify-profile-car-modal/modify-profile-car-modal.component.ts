import { Component, OnInit, ViewChild, Inject, NgZone } from '@angular/core';
// tslint:disable-next-line: no-implicit-dependencies
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { MatDialogRef, IntechToasterService, MAT_DIALOG_DATA } from '@intech/intech-ui';
import { Router } from '@angular/router';
import { DialogData } from '../dialog-data/dialog-data';
import { UserService } from '~core/user-service/user.service';
import { DatePipe } from '@angular/common';
import { MovInTechToaster } from '../toaster/movInTech-toaster';
import { take } from 'rxjs/operators';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Car } from '../car/car';

@Component({
    selector: 'modify-profile-car-modal.component',
    templateUrl: 'modify-profile-car-modal.component.html',
    styleUrls: ['modify-profile-car-modal.component.scss', '../../../sass/custom.scss']
})
export class ModifyProfileCarModalComponent implements OnInit {
    public carInfosForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<ModifyProfileCarModalComponent>,
        private readonly toasterService: IntechToasterService,
        private readonly router: Router,
        private readonly formBuilder: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public dialogData: DialogData,
        private readonly userService: UserService,
        private readonly datePipe: DatePipe,
        private readonly _ngZone: NgZone
    ) {}
    @ViewChild('autosize') autosize: CdkTextareaAutosize;
    currentCar: Car;
    toastermodel: MovInTechToaster = new MovInTechToaster('');
    triggerResize() {
        this._ngZone.onStable.pipe(take(1)).subscribe(() => this.autosize.resizeToFitContent(true));
      }
    ngOnInit() {
        this.currentCar = this.dialogData.car;

        this.carInfosForm = this.formBuilder.group({
            carNameControl: new FormControl('', [Validators.required, Validators.maxLength(30)]),
            carNumberplateControl: new FormControl('', [Validators.required, Validators.maxLength(10)]),
          });
    }

    closeDialog() {
        this.dialogRef.close(true);
    }
}
