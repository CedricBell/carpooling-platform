import { MatDialogRef, IntechToasterService, MAT_DIALOG_DATA } from '@intech/intech-ui';
import { Component, Inject, ViewChild, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DialogData } from '~shared/components/dialog-data/dialog-data';
import { AppConstants } from '~shared/components/constantes/app-constants';
// tslint:disable-next-line: no-implicit-dependencies
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs/operators';
import { RideRequestHistoryService } from '~core/rides/ride-request-history.service';
import { RideEventService } from '~core/rides/ride-event.service';
import { RideRequestHistory } from '../ride/ride-request-history';
import { RideEvent } from '../ride/ride-event';
import { User } from '../user/user';
import { UserService } from '~core/user-service/user.service';
import { RideRequestStatusEnum } from '../ride-request-status-enum';
import { DatePipe } from '@angular/common';
import { RideUser } from '../ride/ride-user';
import { RideService } from '~core/rides/rides.service';
import { RideUserService } from '~core/rides/ride-user.service';
import { IntechActionNotifierService } from '@intech/intech-core';
import { Ride } from '../ride/ride';
import { Notification } from '../notification/notification';
import { NotifService } from '~core/notifier-sw/notifier-sw.service';

@Component({
  selector: 'add-comment-modal.component',
  templateUrl: 'add-comment-modal.component.html',
  styleUrls: ['add-comment-modal.component.scss', '../../../sass/custom.scss']
})
export class AddCommentModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AddCommentModalComponent>,
    private readonly router: Router,
    @Inject(MAT_DIALOG_DATA) public dialogData: DialogData,
    private readonly rideRequestHistoryService: RideRequestHistoryService,
    private readonly rideEventService: RideEventService,
    private readonly userService: UserService,
    private readonly rideUserService: RideUserService,
    private readonly rideService: RideService,
    private readonly datePipe: DatePipe,
    private readonly _ngZone: NgZone,
    private readonly notifService: NotifService,
    private readonly notifierService: IntechActionNotifierService) { }

  @ViewChild('autosize') autosize: CdkTextareaAutosize;
  requestComment: string;
  myRideRequestHistory: RideRequestHistory;
  rideEvent: RideEvent = new RideEvent();
  currentUser: User;
  rideUser: RideUser = new RideUser();


  triggerResize() {
    this._ngZone.onStable.pipe(take(1)).subscribe(() => this.autosize.resizeToFitContent(true));
  }

  ngOnInit() {

    this.userService.getCurrentUser().then(
      currentUser => {
        this.currentUser = currentUser;
        Promise.resolve();
      },
      error => Promise.reject(error)
    );

    this.myRideRequestHistory = this.dialogData.rideRequestHistory;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  joinCovoit(): void {

    this.rideRequestHistoryService.getRideRequestHistoryByRideId(this.dialogData.ride.id).then(
      (rideRequest: RideRequestHistory) => {
        this.myRideRequestHistory = rideRequest;

        this.rideEvent.user = this.currentUser;
        this.rideEvent.status = RideRequestStatusEnum.RIDE_REQUESTED;
        this.rideEvent.date = this.datePipe.transform(Date.now(), AppConstants.DATE_FORMAT);
        this.rideEvent.comment = this.requestComment;

        this.rideUser = this.rideEvent;

        this.rideUserService.addRideUser(this.rideUser, this.dialogData.ride.id).then(
          (createdRideUser: RideUser) => {
            this.dialogData.ride.users.push(createdRideUser);
            this.addRequestedEventToRideRequestHistory();
            this.rideService.updateRide(this.dialogData.ride, this.dialogData.ride.id).then((ride: Ride) => {
              this.notifierService.notifySuccess(AppConstants.REQUEST_SEND_CONFIRMATION);
              const notif = new Notification(this.dialogData.ride.owner.fcmToken, 'MovInTech',
                this.currentUser.firstName + ' a demandé à rejoindre votre trajet', '');
              this.notifService.sendToToken(notif);
              this.router.navigate(['']);
            },
              error => this.notifierService.notifyError(error.error.code));

          },
          error => this.notifierService.notifyError(error.error.code)
        );

      }
    );
  }

  addRequestedEventToRideRequestHistory(): void {

    this.rideEventService.addRideEvent(this.rideEvent , this.dialogData.ride.id).then(
      (createdRideEvent: RideEvent) => {
        this.rideEvent = createdRideEvent;
        this.myRideRequestHistory.rideEvents.push(this.rideEvent);
        this.rideRequestHistoryService.updateRideRequestHistory(this.myRideRequestHistory.id, this.myRideRequestHistory);

      },
      error => this.notifierService.notifyError(error.error.code)
    );
  }

}
