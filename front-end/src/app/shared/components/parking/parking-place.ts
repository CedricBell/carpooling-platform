export class ParkingPlace {

  id: string;
  number: number;
  status: string;
  disabled: boolean;

  constructor() {
  }

}
