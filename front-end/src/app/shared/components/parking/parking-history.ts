import {ParkingPlace} from './parking-place';

export class ParkingHistory {

  id: string;
  parkingSpot: ParkingPlace;
  trigramOccupant: string;
  licensePlate: string;
  date: string;
  freePlaceDate: string;

  constructor() {

  }

}
