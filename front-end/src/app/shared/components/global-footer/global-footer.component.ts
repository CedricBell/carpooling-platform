import { Component } from '@angular/core';

/**
 * Global application footer
 */
@Component({
  selector: 'global-footer',
  templateUrl: 'global-footer.component.html'
})
export class GlobalFooterComponent {
  intech = 'InTech ©';
  constructor() {}
}
