import { Component, OnInit, ViewChild, Inject, NgZone } from '@angular/core';
// tslint:disable-next-line: no-implicit-dependencies
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { MatDialogRef, IntechToasterService, MAT_DIALOG_DATA } from '@intech/intech-ui';
import { Router } from '@angular/router';
import { DialogData } from '../dialog-data/dialog-data';
import { UserService } from '~core/user-service/user.service';
import { DatePipe } from '@angular/common';
import { User } from '../user/user';
import { MovInTechToaster } from '../toaster/movInTech-toaster';
import { take } from 'rxjs/operators';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
    selector: 'modify-profile-infos-modal.component',
    templateUrl: 'modify-profile-infos-modal.component.html',
    styleUrls: ['modify-profile-infos-modal.component.scss', '../../../sass/custom.scss']
})
export class ModifyProfileInfosModalComponent implements OnInit {
    public userInfosForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<ModifyProfileInfosModalComponent>,
        private readonly toasterService: IntechToasterService,
        private readonly router: Router,
        private readonly formBuilder: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public dialogData: DialogData,
        private readonly userService: UserService,
        private readonly datePipe: DatePipe,
        private readonly _ngZone: NgZone
    ) {}
    @ViewChild('autosize') autosize: CdkTextareaAutosize;
    currentUser: User;
    toastermodel: MovInTechToaster = new MovInTechToaster('');
    triggerResize() {
        this._ngZone.onStable.pipe(take(1)).subscribe(() => this.autosize.resizeToFitContent(true));
      }
    ngOnInit() {
        this.currentUser = this.dialogData.user;

        this.userInfosForm = this.formBuilder.group({
            firstNameControl: new FormControl('', [Validators.required, Validators.maxLength(30)]),
            lastNameControl: new FormControl('', [Validators.required, Validators.maxLength(30)]),
            emailControl: new FormControl('', [Validators.required, Validators.maxLength(60), Validators.email])
          });
    }


    closeDialog() {
        this.dialogRef.close(true);
    }

}
