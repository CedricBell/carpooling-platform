import { User } from '../user/user';
import { RideUser } from './ride-user';
import { Address } from '../address/address';



export class Ride {

    public id: string;
    public owner: User;
    public start: Address;
    public end: Address;
    public date: string;
    public capacity: number;
    public hour: number;
    public min: number;
    public users: RideUser[];
    public stepAddresses: Address[];
    public numbrerUsersAccepted: number;

    constructor(
    ) {
        this.owner = new User();
        this.start = new Address();
        this.end = new Address();
        this.date = null;
        this.capacity = null;
        this.stepAddresses = [];
    }
}

