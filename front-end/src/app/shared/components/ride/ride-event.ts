import { User } from '../user/user';
import { RideRequestStatusEnum } from '../ride-request-status-enum';

export class RideEvent {
    id: string;
    user: User;
    status: RideRequestStatusEnum;
    date: string;
    comment: string;

    constructor() {
    }
}
