import { Ride } from './ride';
import { RideEvent } from './ride-event';

export class RideRequestHistory {
    id: string;
    ride: Ride;
    rideEvents: Array<RideEvent>;

    constructor(ride: Ride, rideEvents: Array<RideEvent>) {
        this.ride = ride;
        this.rideEvents = rideEvents;
    }
}
