import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders } from '@angular/core';
import { GlobalHeaderComponent } from '~shared/components/global-header/global-header.component';
import { GlobalFooterComponent } from '~shared/components/global-footer/global-footer.component';
import { TranslateModule } from '@ngx-translate/core';
import { AllAngularMaterialModule, IntechUiModule, MAT_DATE_LOCALE } from '@intech/intech-ui';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IntechInternalUiModule } from '@intech/intech-internal-ui';
import { DatePipe } from '@angular/common';
import { SplitPipe } from '~shared/pipes/splitpipe';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { DeleteModalComponent } from '~shared/components/delete-modal/delete-modal.component';
import { RideFilterPipe } from '~shared/pipes/filter.pipe.ts';
import { AddCommentModalComponent } from './components/add-comment-modal/add-comment-modal.component';
import { ModifyProfileInfosModalComponent } from './components/modify-profile-infos-modal/modify-profile-infos-modal.component';
import { ModifyProfileAddressModalComponent } from './components/modify-profile-address-modal/modify-profile-address-modal.component';
import { ModifyProfileCarModalComponent } from './components/modify-profile-car-modal/modify-profile-car-modal.component';


export const APP_SHARED_PROVIDERS = [{ provide: MAT_DATE_LOCALE, useValue: 'fr-FR' }];

/**
 * Shared components
 */
const COMPONENTS = [GlobalHeaderComponent, GlobalFooterComponent, DeleteModalComponent , AddCommentModalComponent,
   ModifyProfileInfosModalComponent, ModifyProfileAddressModalComponent, ModifyProfileCarModalComponent] ;

/**
 * Shared directives
 */
const DIRECTIVES = [];

/**
 * Shared pipes
 */
const PIPES = [SplitPipe , RideFilterPipe];

/**
 * All modules to include
 */
const ALWAYS_USED_MODULES = [
  TranslateModule,
  AllAngularMaterialModule,
  IntechUiModule,
  IntechInternalUiModule,
  FormsModule,
  ReactiveFormsModule,
  MatMomentDateModule,
];

@NgModule({
  imports: [...ALWAYS_USED_MODULES],

  exports: [...ALWAYS_USED_MODULES, ...COMPONENTS, ...DIRECTIVES, ...PIPES],

  declarations: [...COMPONENTS, ...DIRECTIVES, ...PIPES],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],

  providers: [DatePipe, SplitPipe, DeleteModalComponent , RideFilterPipe , AddCommentModalComponent,
    ModifyProfileInfosModalComponent, ModifyProfileAddressModalComponent, ModifyProfileCarModalComponent]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [...APP_SHARED_PROVIDERS]
    };
  }
}
