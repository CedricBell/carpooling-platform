import { Pipe, PipeTransform } from '@angular/core';
import { Ride } from '~shared/components/ride/ride';
import moment, { isMoment } from 'moment';
import * as _ from 'lodash';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { AppConstants } from '~shared/components/constantes/app-constants';
import { Address } from '~shared/components/address/address';

@Pipe({
  name: 'ridefilter',
  pure: false
})
export class RideFilterPipe implements PipeTransform {


  transform(items: Ride[], filter: Ride , length: number): Ride[] {

    const ride = _.pickBy(filter, (property) => this.checkProperty);
    if ( _.isEmpty(ride)) {
      return items;
    }

    items = _.filter(items, (item) => this.applyFilter( item , ride));

    return items;

}

  checkProperty(property: any) {

    return !_.isNull(property) && !_.isEmpty(property);
  }

  applyFilter( arrayRide: Ride, rideFilter: Partial<Ride> ): boolean {

    let result = true;

    for ( const field in rideFilter ) {

      if (result) {
        if (rideFilter[field] instanceof Address && rideFilter[field].name ) {

          if ( arrayRide[field].name.toLowerCase().indexOf(rideFilter[field].name.toLowerCase()) > -1) {

            result = true;
          } else {

            result = false;
          }
      } else if (isMoment(rideFilter[field])) {

        result = this.isDateEquals(rideFilter, field, arrayRide);

      } else if ( _.isNumber(rideFilter[field])) {
        result = this.isTimeEquals(arrayRide, rideFilter, field);
        }
      }

  }
    return result;

  }


  private isDateEquals(rideFilter: Partial<Ride>, field: string, arrayRide: Ride): boolean {

    const searchRideDate = moment(rideFilter[field], 'YYYY-MM-DD').toDate();
    const arrayRideDate = moment(arrayRide[field], 'YYYY-MM-DD').toDate();

    if (searchRideDate.toDateString() === arrayRideDate.toDateString()) {
      return true;
    } else {
      return false;
    }
  }

  private isTimeEquals(arrayRide: Ride, rideFilter: Partial<Ride>, field: string): boolean {

    const arrayRideHour = moment(arrayRide.date, 'YYYY-MM-DD HH:mm').toDate().getHours();
    const arrayRideMin = moment(arrayRide.date, 'YYYY-MM-DD HH:mm').toDate().getMinutes();
    if (rideFilter[field] === arrayRideHour || rideFilter[field] === arrayRideMin) {
      return true;
    } else {
      return false;
    }
  }
}
