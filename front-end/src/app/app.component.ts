import { Component, OnInit } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { IntechThemeService } from '@intech/intech-ui';
import { IntechAppNotifierToaster } from '~core/notifier-toaster/intech-notifier-toaster.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';




/**
 * App root component
 */

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

/**
 * AppComponent
 */
export class AppComponent implements OnInit {
  public isDarkTheme = false;
  constructor(
    private readonly _logger: NGXLogger,
    private readonly _translate: TranslateService,
    private readonly _themeService: IntechThemeService,
    private readonly _appToasterService: IntechAppNotifierToaster,
    private readonly matIconRegistry: MatIconRegistry,
    private readonly domSanitizer: DomSanitizer

  ) {

    this.matIconRegistry.addSvgIcon(
      'ride',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/ride.svg')
    );
  }

  ngOnInit(): void {
    // this language will be used as a fallback when a translation isn't found in the current language
    this._translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this._translate.use('fr');

    this._logger.debug('Application is started');
    this._logger.debug(`Currently used : ${this._translate.currentLang}`);

    this._themeService.subscribeToStartLoading(isDark => {
      this.isDarkTheme = isDark;
    });

    this._appToasterService.subscribeAll();
  }
}
