const urlRides = '/rides';
const urlUsers = '/users';
const urlCar = '/cars';
const urlParking = '/parkingPlaces';
const urlParkingHistory = '/parkingHistories';
const urlRideRequestHistory = '/rideRequestHistories';
const urlRideEvent = '/rides/{rideId}/events';
const urlRideUser = '/rides/{rideId}/users';
const urlLogin = '/login';
const urlNotification = '/notifications';
const urlAddress = '/address';



export const environment = {
  // If production, Angular is start in production mode
  production: false,
  'backend-date-format': 'YYYM-MM-DD',
  // Application Name
  appName: 'covoiturage-angular',
  // Backend base context
  baseBackEndUrl: 'http://localhost:8080',
  // Value in ms for HTTP timeout
  requestTimeout: 5000,
  // Name of the attibute in i18n files for network state
  connectivity: 'connectivity',
  // Name of the UUID header inject in the HTTP request
  uuidHeaderName: 'UUID',
  // Parameter for enable / disbale the Remote HealthCheck Service
  // Remote HealthCheck Service => call backend at interval to check is up
  healthCheckService: false,
  // Delay between call of Remote HealthCheck Service if enable (ms)
  healthCheckDelay: 120000,
  // front-managed or back-managed
  i18nFileImportPolitic: 'front-managed',


  firebaseConfig: {
    apiKey: 'AIzaSyCxN0bKM3372ckYiasKZAnJctxgbfr0U58',
    authDomain: 'covoiturage-intech.firebaseapp.com',
    databaseURL: 'https://covoiturage-intech.firebaseio.com',
    projectId: 'covoiturage-intech',
    storageBucket: '',
    messagingSenderId: '500107194859',
    appId: '1:500107194859:web:2ba88b46752f6aee'
  },

  VAPID_PUBLIC_KEY: 'BF8XxCUtTESz0JIRmyY60dS-5wc8HgDpStbBIJDJEg0e_MEVxD3cz2ytjN5OFAtUSNNQMOFwtqe6WJE0n0srzg0',

  manage: {
    // Variable to enable / disable HttpRetry on HTTP calls
    httpRetry: true,
    // Variable to enable / disable Critical Error on HTTP calls
    criticalError: true
  },

  header: {
    secure: 'secured'
  },
  // Array of Backend end points
  // Each attribute have to be present in the service.keys.constants.ts (/core) file
  wsUrls: {
    healthCheck: '/actuator/health',
    login: urlLogin,
    nonce: 'lalala',
    parkingHistory: {
      get: {
        all: urlParkingHistory,
        getById: urlParkingHistory + '/{id}',
      },
      update: urlParkingHistory + '/{id}',
      add: urlParkingHistory,
      delete: urlParkingHistory + '/{id}'
    },
    parking: {
      get: {
        all: urlParking,
        getById: urlParking + '/{id}',
      },
      update: urlParking + '/{id}',
      add: urlParking,
      delete: urlParking + '/{id}'
    },
    rides: {
      get: {
        all: urlRides,
        getById: urlRides + '/{id}',
        me: urlRides + '/forUser'
      },
      update: urlRides + '/{id}',
      add: urlRides,
      delete: urlRides + '/{id}'
    },
    users: {
      get: {
        all: urlUsers,
        getById: urlUsers + '/{id}',
        me: urlUsers + '/me'
      },
      update: urlUsers + '/{id}',
      add: urlUsers,
      delete: urlUsers + '/{id}'
    },
    address: {
      get: {
        all: urlAddress,
        getById: urlAddress + '/{id}',
      },
      update: urlAddress + '/{id}',
      add: urlAddress,
      delete: urlAddress + '/{id}'
    },
    car: {
      get: {
        all: urlCar,
        getById: urlCar + '/{id}',
      },
      update: urlCar + '/{id}',
      add: urlCar,
      delete: urlCar + '/{id}'
    },
    rideRequestHistory: {
      get: {
        all: urlRideRequestHistory,
        getById: urlRideRequestHistory + '/{id}',
        getByRideId: urlRideRequestHistory + '/byRide' + '/{id}'
      },
      update: urlRideRequestHistory + '/{id}',
      add: urlRideRequestHistory,
      delete: urlRideRequestHistory + '/{id}'
    },
    rideEvent: {
      get: {
        all: urlRideEvent,
        getById: urlRideEvent + '/{id}',
      },
      update: urlRideEvent + '/{id}',
      add: urlRideEvent,
    },
    rideUser: {
      get: {
        all: urlRideUser,
        getById: urlRideUser + '/{id}',
      },
      update: urlRideUser + '/{id}',
      add: urlRideUser,
    },
    notification: {
      register: urlNotification + '/register',
      token: urlNotification + '/token'
    }
  }
};
