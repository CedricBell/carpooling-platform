/**
 * Enviroment for Intech Integration
 */
export const environment = {
  production: true,
  baseBackEndUrl: 'https://plateforme-covoiturage.intech-dev.com:8080'
};
