/**
 * Enviroment for production
 */
export const environment = {
  production: true,
  baseBackEndUrl: 'https://plateforme-covoiturage.intech-dev.com:8080'
};

