InTech - Frontend de la plateforme de covoiturage interne
===============

Réalisé par Jorane SCHUSTER, Marine PALLOTTA et Cédric BELL.
Encadré par Thibaut HUMBERT et Rémy BENOIST.


## Description générale

La plateforme de covoiturage d'InTech **MovInTech** est une des applications internes d'**InTech**. 


## Technologies utilisées

Ce projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.

## Configuration

1. Assurez-vous que [NodeJS](https://nodejs.org/), [npm](https://www.npmjs.com/) et [angular-cli](https://cli.angular.io/) sont bien installés et configurés.

2. Il est nécessaire de configurer et de lancer le [backend](https://gitlab.intech.lu/plateforme-covoiturage/back-end).

3. Ensuite, il est nécessaire d'installer vos dépendances.

```
cd path/to/plateforme-covoiturage-front
```
puis 
```
npm install
```
ou bien 
```
npm i
```

## Libraries

- Angular : 7.3.0
- NodeJs : 9.11.1
- NPM : 6.9.0
- TypeScript : 3.2.2

## Lancement

```
npm run start
```

L'application est désormais lancée est accessible à l'adresse suivante : [http://localhost:4300/](http://localhost:4300/).

## Autres commandes utiles

### Build

Pour obtenir de la documentation sur le build :  
```
./bin/build.sh --help
```

Pour builder l'application dans un environnement e (prod ou int par défaut) :
```
./bin/build.sh -e=XXX
```

### Generation Documentation
Pour obtenir la documentation du front-end de l'application : 
```
npm run compodoc
``` 

Un dossier "Documentation" a été généré à la racine du projet. Il Vous suffit d'ouvrir le fichier index.html pour accéder à la documentation.
### Sonar analyse
Pour analyser la qualité du code de l'application du front-end de l'application : 

```
npm run sonar
````

Ouvrer un navigateur à l'adresse : [sonar-covoiturage-frontend](http://192.168.11.27:9000/dashboard?id=PLATEFORME_COVOITURAGE_FRONT) pour accéder à la dernière analyse lancée.
## Gestion starter Angular


### Configration of Nexus

- Create a .npmrc configuration file in your project root :

  ```registry=https://nexus.intech.lu/nexus3/repository/npm-public/
  email=XXX
  always-auth=true
  _auth=XXX
  ```

- Replace by your email address
- Replace the '\_auth' value by a base 64 encoded value

  - Open a terminal
  - Run `echo -n 'username:password' | openssl base64` with your Active Directory credentials
  - Use the generated value

- https://docs.npmjs.com/files/npmrc.html

### Development

#### Create a Page

- Create a page in the **page** directory (src/app/pages)
- Add the import in the **app-pages.modules.ts**
- Add the new route in the **app-pages.routing.ts**

#### Create a Service

- Create the service in the **core** directory (src/app/core)
- Add the import in the **app-core.modules.ts**

#### Create a Component / Directive / Pipe

- Create the element in the **shared** directory (src/app/shared/xxx)
- Add the import in the **shared.modules.ts**


#### Import JS files

Add scripts in the "scripts" array of angular.json file :
`projects.XXX.architect.build.options.scripts`

Example :
`"scripts": [{ "input": "node_modules/jquery/dist/jquery.min.js" ]`

### Others information

#### Hooks

- Pre-commit : lint
- Pre push : build prod

#### Translation

The traduction files are in the 'i18n' folder, one file by language.
To simplify, we decide to include **localization** and **internationalization** in the same file (most current case).

#### Prettier configuration :

- <https://medium.com/@victormejia/setting-up-prettier-in-an-angular-cli-project-2f50c3b9a537>

- <https://prettier.io/docs/en/options.html>

- <https://prettier.io/docs/en/webstorm.html>


## Déploiement

Pour déployer, veuillez vous réferrez à ce [document de référence](https://gitlab.intech.lu/plateforme-covoiturage/deployment/blob/master/Notice-D%C3%A9ploiement.pdf).



## En cas de problème 
N'hésitez pas à contacter l'un des membres du projet ou bien Aymeric Zanirato, responsable du projet du starter Angular d'InTech.
