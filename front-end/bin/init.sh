#!/usr/bin/env bash

set -e

export CURRENT_PATH=`dirname "$0"`
export CURRENT_DIR=`( cd "$CURRENT_PATH" && pwd )`

export SCRIPTS_DIR=${CURRENT_DIR}
export PROJECT_DIR=${CURRENT_DIR}/..

source ${SCRIPTS_DIR}/utils.sh

function main(){
  displayEnd
  artify
  displayEnd

  read -p "Enter your project name : " projectName

  # Validate project name not empty
  if [[ -z  ${projectName} ]]; then
    echo "The project name is empty !"
    exit;
  fi

  # Validate project name : alphanumeric
  if [[ "$projectName" =~ [^a-zA-Z0-9] ]]; then
    echo "The project name is not valid (alphanumeric only) !"
    exit;
  fi

  echo "Your project name is : $projectName"
  echo ""
  echo "Generate your project ..."

  cd $PROJECT_DIR
  #  Remove .git
  rm -rf ".git"

  #  Remove dist
  rm -rf "dist"

  #  Remove node modules
  rm -rf "node_modules"

  #  Remove package-lock.json
  rm -rf "package-lock.json"


  #cd $CURRENT_PATH

  # Update project name in files

  cd $PROJECT_DIR
  # add dependency
  npm install replace -g

  # make the replace
  replace 'intech-angular-starter' ${projectName} . --ignoreCase --recursive --exclude="*.md,*.sh,npm-extract-dependencies"
  replace 'INTECH_ANGULAR_STARTER' ${projectName} . --ignoreCase --recursive --exclude="*.md,*.sh,npm-extract-dependencies"
  replace 'IntechAngularStarter' ${projectName} . --ignoreCase --recursive --exclude="*.md,*.sh,npm-extract-dependencies"

  # Init git
  cd $PROJECT_DIR
  git init

  echo "Project '$projectName' is now available in your workspace"
  echo ""
  echo "You need to add the remote repository now :-)"

  displayEnd

  # Remove init script
  cd $SCRIPTS_DIR
  rm init.sh
}


main
