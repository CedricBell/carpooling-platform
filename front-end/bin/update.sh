#!/usr/bin/env bash

set -e

export CURRENT_PATH=`dirname "$0"`
export CURRENT_DIR=`( cd "$CURRENT_PATH" && pwd )`

export SCRIPTS_DIR=${CURRENT_DIR}
export PROJECT_DIR=${CURRENT_DIR}/..

source ${SCRIPTS_DIR}/utils.sh

function usage(){


artify
cat <<-END
Usage:
======

From libraries root directory './bin/update.sh'

------- GENERAL OPTIONS -----------

   -h | --help
     Display this help.

------- LIBRARIES PUBLISH -----------

   intech-core
     Update @intech/intech-core

   intech-ui
     Update @intech/intech-ui

   intech-internal-core
     Update @intech/intech-internal-core

   intech-internal-ui
     Update @intech/intech-internal-ui

 you can chain them
 ex : ./bin/update.sh intech-core intech-ui

------- GROUPED UPDATE -----------

   --all
     Update all available libraries

   --intech-common
     Update common libraries that can be shared for external project, i.e: @intech/intech-core & @intech/intech-ui

   --intech-internal
     Update common libraries that can be shared for external project, i.e: @intech/intech-internal-core & @intech/intech-internal-ui

------- DISPLAY OPTIONS ------------

   --skip-artify
     Skip the display of ascii art on the beginning of the run
     This can be used when the script is include on another one
     Default to false

   --skip-display-end
     Skip the display of the end of script disclaimer
     This can be used when the script is include on another one
     Default to false

END


exit 1;

}

for i in "$@"
do
  case $i in
      -h|--help)
      usage
      ;;

      --all)
      all=true
      shift
      ;;

      --skip-artify)
      skip_artify=true
      shift
      ;;

      --skip-display-end)
      skip_display_end=true
      shift
      ;;

      intech-core)
      intech_core=true
      shift
      ;;

      intech-ui)
      intech_ui=true
      shift
      ;;

      intech-internal-core)
      intech_internal_core=true
      shift
      ;;

      intech-internal-ui)
      intech_internal_ui=true
      shift
      ;;

      --all)
      all=true
      shift
      ;;

      --intech-common)
      intech_common=true
      shift
      ;;

      --intech-internal)
      intech_internal=true
      shift
      ;;


  esac
done


# ***** Initialize variables and parameters with defaults values

intech_core=${intech_core:="false"}
intech_ui=${intech_ui:="false"}
intech_internal_core=${intech_internal_core:="false"}
intech_internal_ui=${intech_internal_ui:="false"}
all=${all:="false"}
intech_common=${intech_common:="false"}
intech_internal=${intech_internal:="false"}


if [ "${all}" = "true" ]; then
    intech_core=true
    intech_ui=true
    intech_internal_core=true
    intech_internal_ui=true
fi

if [ "${intech_common}" = "true" ]; then
    intech_core=true
    intech_ui=true
fi

if [ "${intech_internal}" = "true" ]; then
    intech_internal_core=true
    intech_internal_ui=true
fi


function main(){
  if ! [ "${skip_artify}" = "true" ]; then
    artify
  fi

  displayTitle "Update libraries"

  if [ "${intech_core}" = "true" ]; then
    update_intech_core
  fi

  if [ "${intech_ui}" = "true" ]; then
    update_intech_ui
  fi

  if [ "${intech_internal_core}" = "true" ]; then
    update_intech_internal_core
  fi

  if [ "${intech_internal_ui}" = "true" ]; then
    update_intech_internal_ui
  fi

  if [ "${skip_display_end}" = "true" ]; then
    displayEnd
  fi
}


function update_intech_core(){
  displayTitle "Update Intech Core"

    cd ${PROJECT_DIR}
    npm install @intech/intech-core@latest --force

    displayEnd
}

function update_intech_ui(){
  displayTitle "Update Intech UI library"

  cd ${PROJECT_DIR}
  npm install @intech/intech-ui@latest --force

  displayEnd
}

function update_intech_internal_core(){
  displayTitle "Update Intech Internal Core"

  cd ${PROJECT_DIR}
  npm install @intech/intech-internal-core@latest --force

  displayEnd
}

function update_intech_internal_ui(){

  displayTitle "Update Intech Internal UI"

  cd ${PROJECT_DIR}
  npm install @intech/intech-internal-ui@latest --force

  displayEnd
}

main


