#!/bin/bash

# Color management
# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux

export RED='\033[1;31m'
export BLUE='\033[1;34m'
export NC='\033[0m' # No Color

# **** Beautify interaction (or not) ;)
function artify(){
cat <<-END

            *#####                                   |
          *(######/  .//.                            |
        .(########/  *///,                           |
      .(##########(  *////         ..                |  ............                        .
     (############(  */////,       ..                |        .                             .
  .(##############(  *//////*      ..    ........    |        .    ..........    .........  .........
.(################(  ,////////     ..   ..       ..  |        .   .          .  .           .        .
(##################  ,/////////.   ..  ..        ..  |        .   .         ..  .           .        .
###################  ,//////////   ..  ..        ..  |        .   .    ..       .           .        .
##################(  ,//////////,  ..  ..        ..  |        .   ...           .           .        .
(##################  ,//////////,  ..  ..        ..  |        .   .             .           .        .
  /################, ,/////////,   ..  ..        ..  |        .    .........     .........  .        .
     *(############*  //////,                        |
        *(########(#  *///,                          |
           /######(     ,                            |
             *#((/                                   |

END
}


# ***** UTILS FUNCTIONS

function displayTitle {
  echo -e ${RED}
  echo "****************** $1 ******************"
  echo -e ${NC}
}

function displayInstall {
  echo -e ${BLUE}
  echo ">> Install $1"
  echo -e ${NC}
}

function displayClean {
  echo -e ${BLUE}
  echo ">> Cleaning $1"
  echo -e ${NC}
}

function displayAlreadyInstalled {
  echo ">> $1 already installed, skip installation"
}

function displayEnd {
  echo -e ${RED}
  echo "*************************************************"
  echo -e ${NC}
}



