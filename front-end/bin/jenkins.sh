#!/bin/bash

set -e

export CURRENT_PATH=`dirname "$0"`
export CURRENT_DIR=`( cd "$CURRENT_PATH" && pwd )`

export SCRIPTS_DIR=${CURRENT_DIR}
export PROJECT_DIR=${CURRENT_DIR}/..

source ${SCRIPTS_DIR}/utils.sh

RED='\033[1;31m'
BLUE='\033[1;34m'
NC='\033[0m' # No Color



##### FUNCTION #####

# ***** Main function

function main(){
  artify
  clean
  install
  build
}



function clean(){
  displayClean
  rm -rf ${PROJECT_DIR}/node_modules/
  rm -rf ${PROJECT_DIR}/dist
}

function install(){
  displayInstall
  npm install
  displayEnd
}

function build(){
  ${SCRIPTS_DIR}/build.sh --env=int
}

# ****** run main function at the end
main



