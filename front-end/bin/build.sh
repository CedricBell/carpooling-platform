#!/usr/bin/env bash

set -e

export CURRENT_PATH=`dirname "$0"`
export CURRENT_DIR=`( cd "$CURRENT_PATH" && pwd )`

export SCRIPTS_DIR=${CURRENT_DIR}
export PROJECT_DIR=${CURRENT_DIR}/..

source ${SCRIPTS_DIR}/utils.sh


function usage(){

artify
cat <<-END
Usage:
======

From libraries root directory './bin/build.sh'

------- GENERAL OPTIONS -----------

   -h | --help
     Display this help.

------- BUILD -----------

-e=* | --env=* | env=*
     then application environment target of the build.

     value can be 'int' or 'production'

     Default value is default

------- DISPLAY OPTIONS ------------

   --skip-artify
     Skip the display of ascii art on the beginning of the run
     This can be used when the script is include on another one
     Default to false

END


exit 1;
}

for i in "$@"
do
  case $i in
      -h|--help)
      usage
      ;;

      -e=*|--env=*|env=*)
      env_target="${i#*=}"
      shift
      ;;

      --skip-artify)
      skip_artify=true
      shift
      ;;

  esac
done

# ***** Initialize variables and parameters with defaults values
env_target=${env_target:="default"}
skip_artify=${skip_artify:="false"}


##### FUNCTION #####

# ***** Main function
function main(){

  if ! [ "${skip_artify}" = "true" ]; then
    artify
  fi

  cd ${PROJECT_DIR}
  build_browser
}

# ***** Execute the build for a browser
function build_browser() {

  displayTitle "Build application for Browser"
  check_environment

  echo "Environment : ${env_target}"

  if ! [ "${env_target}" = "default" ]; then
    npm run ng build --configuration=${env_target}
  else
    npm run ng build
  fi

  displayEnd
}

# ***** Check the environment in parameter
function check_environment(){
if ! [ "${env_target}" = "default" -o  "${env_target}" = "int" -o  "${env_target}" = "production" ]; then
  echo "No valid environment !"
  displayEnd
  exit
fi
}

# ****** run main function at the end
main
