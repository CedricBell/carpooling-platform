# NPM EXTRACT DEPENDENCIES

This program allow to export (file) npm dependencies of a project

## How to use

- Run `npm install`
- Run `cd bin/`
- Run `chmod +x *.sh`
- Run `./start.sh --help`
- Read the options
- Run `./start.sh --target="[*]"`
