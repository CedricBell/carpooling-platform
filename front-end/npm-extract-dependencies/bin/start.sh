#!/bin/bash

function usage(){
cat <<-END

Usage:
======

from  root directory './bin/start.sh'

    -h | --help
     Display this help.

    -prod=* | --production=*
     Allow production dependencies
     Default value is true

    -dev=* | --development=*
     Allow development dependencies
     Default value is true  
    
    -json=* | --json=*
     Write output json file
     Default value is true

    -exl=* | --excel=*
     Write output Excel file
     Default value is true    

    -file=* | --fileName=*
     Output file name
     Default value is 'EXPORT_DEPENDENCIES'

    -outPath=* | --outputPath=*
     Output file path
     Default value is the export directory
   
    -tgt=* | --target=*
     Target project path
     Default value is the current directory

    -depth=* | --depth=*
     Depth of dependencies
     Value can be O or 1
     Default value is 0

    -debug | --debug
     Debug mode
     Default value is false


END

exit 1;

}

# ***** Catch script parameters
for i in "$@"
do
  case $i in
      -h|--help)
      usage
      ;;

      -prod=*|--production=*)
      prodMode="${i#*=}"
      shift
      ;;

      -dev=*|--development=*)
      devMode="${i#*=}"
      shift
      ;;

      -json=*|--json=*)
      json="${i#*=}"
      shift
      ;;
      -exl=*|--excel=*)
      excel="${i#*=}"
      shift
      ;;

      -file=*|--fileName=*)
      fileName="${i#*=}"
      shift
      ;;

      -outPath=*|--outputPath=*)
      outputPath="${i#*=}"
      shift
      ;;

      -tgt=*|--target=*)
      target="${i#*=}"
      shift
      ;;

      -depth=*|--depth=*)
      depth="${i#*=}"
      shift
      ;;

      -debug|--debug)
      debug=true
      shift
      ;;

  esac
done

# ***** Initialize variables and parameters with defaults values
export CURRENT_PATH=`dirname "$0"`
export CURRENT_DIR=`( cd "$CURRENT_PATH" && pwd )`
export APP_DIR=${CURRENT_DIR}/..
export EXPORT_PATH=${CURRENT_DIR}/../export

prodMode=${prodMode:="true"}
devMode=${devMode:="true"}
fileName=${fileName:="EXPORT_DEPENDENCIES"}
json=${json:="true"}
excel=${excel:="true"}
outputPath=${outputPath:=${EXPORT_PATH}}
depth=${depth:="0"}
debug=${debug:="false"}
target=${target:=${APP_DIR}}

##### FUNCTION #####

# ***** Main function
function main(){
  node ${APP_DIR}/index.js --target ${target} --prodMode ${prodMode} --devMode ${devMode} --json ${json} --excel ${excel} --fileName ${fileName} --outputPath ${outputPath} --depth ${depth} --debug ${debug}
}

# ****** run main function at the end
main