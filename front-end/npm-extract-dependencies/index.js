const shellExec = require('shell-exec');
const _ = require('lodash');
const stats = require('download-stats');
const fs = require('fs');
const XLSX = require('xlsx');
const argv = require('minimist')(process.argv.slice(2));
const stronglyTyped = require('strongly-typed');

const NOT_A_BOOLEAN = 'is not a boolean';

var Configuration = stronglyTyped({
  devMode: 'boolean',
  prodMode: 'boolean',
  depth: 'number',
  exportName: 'string',
  exportPath: 'string',
  projectPath: 'string',
  exportJson: 'boolean',
  exportExcel: 'boolean',
  debug: 'boolean'
});

var conf = {};

/****************************************************************************************************************************************
 *
 *                                                        MAIN
 *
 ***************************************************************************************************************************************/

main();
function init() {
  conf = Configuration({
    devMode: convertToBoolean(argv.devMode, 'devMode'),
    prodMode: convertToBoolean(argv.prodMode, 'prodMode'),
    depth: argv.depth,
    exportName: argv.fileName,
    exportPath: argv.outputPath,
    projectPath: argv.target,
    exportJson: convertToBoolean(argv.json, 'json'),
    exportExcel: convertToBoolean(argv.excel, 'excel'),
    debug: convertToBoolean(argv.debug, 'debug')
  });

  //check type
  Configuration.created(conf) === true;

  console.log(
    ` ----------------------- conf -----------------------
    Project Path : ${conf.projectPath}
    Export name : ${conf.exportName}
    Export path : ${conf.exportPath}
    Development mode for dependencies : ${conf.devMode}
    Production mode for dependencies : ${conf.prodMode}
    Depth dedendencies : ${conf.depth}
    Export to Json file : ${conf.exportJson}
    Export to Excel file : ${conf.exportExcel}
    Debug mode : ${conf.debug}
  -----------------------------------------------------------
  `
  );
}

async function main() {
  init();
  console.log('=> Start NPM List');
  var dependencies = await getNpmList();
  console.log('=> Finish NPM List');
  var commands = await getCommandList(dependencies);
  console.log('=> Start NPM View');
  var result = await getNpmViews(commands);
  console.log('=> Finish NPM View');

  if (conf.exportJson) {
    // WRITE IN JSON FILE
    writeJson(result);
  }

  if (conf.exportExcel) {
    // WRITE IN EXCEL FILE
    writeExcel(result);
  }
}

/****************************************************************************************************************************************
 *
 *                                                        NPM LIST
 *
 ***************************************************************************************************************************************/

function getNpmList() {
  return new Promise(resolve => {
    // MANAGE PRODUCTION / DEVELOPMENT dependencies
    shellExec(`cd ${conf.projectPath} && npm ls --depth ${conf.depth} ${conf.devMode ? '--dev' : ''} ${conf.prodMode ? '--prod' : ''} --json`)
      .then(res => {
        checkShellExecCode(res);
        // Parsing
        var out = JSON.parse(res.stdout);
        // Convert in array
        resolve(Object.values(out.dependencies));
      })
      .catch(console.log);
  });
}

/****************************************************************************************************************************************
 *
 *                                                        NPM VIEW
 *
 ***************************************************************************************************************************************/

async function getNpmView(command) {
  return new Promise(resolve => {
    var data = {};
    shellExec(command.exec)
      .then(res => {
        checkShellExecCode(res);

        var parseResult = JSON.parse(res.stdout);

        if (!_.isNil(parseResult)) {
          prepareData(data, parseResult, command);
          resolve(data);
        }
      })
      .catch(console.log);
  });
}

async function getNpmViews(commands) {
  var datas = [];
  let index = 1;
  const size = commands.length;
  for (const command of commands) {
    console.log(`   => (${index}/${size}) ${JSON.stringify(command.dependencieRef)}`);
    var result = await getNpmView(command);
    // Add lastMonthDownload
    result.lastMonthDownload = await getLastMonthDownload(result.name);
    // Add weeklyDownload
    result.weeklyDownload = await getLastWeekDownload(result.name);

    datas.push(result);
    index++;
  }
  return datas;
}

/****************************************************************************************************************************************
 *
 *                                                        STATS LIBRARIE
 *
 ***************************************************************************************************************************************/

async function getLastMonthDownload(name) {
  return new Promise(resolve => {
    stats.get.lastMonth(name, (err, results) => {
      if (err) console.error(err);
      resolve(results.downloads);
    });
  });
}

async function getLastWeekDownload(name) {
  return new Promise(resolve => {
    stats.get.lastWeek(name, (err, results) => {
      if (err) console.error(err);
      resolve(results.downloads);
    });
  });
}

/****************************************************************************************************************************************
 *
 *                                                        UTILS
 *
 ***************************************************************************************************************************************/

function addIfNotNull(object, propertyName, propertyValue) {
  if (!_.isNil(propertyValue)) {
    object[propertyName] = propertyValue;
  }
}

function writeJson(values) {
  const jsonFileName = `${conf.exportPath}/${conf.exportName}.json`;
  fs.writeFile(jsonFileName, JSON.stringify(values), err => {
    if (err) {
      console.log(err);
    }
    console.log(`=> Finish write Json : ${jsonFileName}`);
  });
}

function writeExcel(dataToExcel) {
  const xlsFilename = `${conf.exportPath}/${conf.exportName}.xlsx`;
  const ws = XLSX.utils.json_to_sheet(dataToExcel);
  const wb = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  XLSX.writeFile(wb, xlsFilename, { Props: { Author: 'InTech' } });
  console.log(`=> Finish write Excel : ${xlsFilename}`);
}

function checkShellExecCode(shellExecResponse) {
  if (shellExecResponse.code !== 0) {
    console.log('----------------------- Shell exec error -----------------------');
    console.log(shellExecResponse.stderr);
    process.exit(1);
  }
}

function prepareData(data, parseResult, command) {
  var lastVersion = _.get(parseResult, 'dist-tags.latest', null);

  addIfNotNull(data, 'name', parseResult.name);
  addIfNotNull(data, 'currentVersion', command.version);
  addIfNotNull(data, 'lastVersion', lastVersion);
  addIfNotNull(data, 'lastPublishDate', _.get(parseResult.time, lastVersion));

  if (!_.isNil(parseResult.versions)) {
    addIfNotNull(data, 'nbVersions', parseResult.versions.length);
  }

  if (!_.isNil(parseResult.maintainers)) {
    addIfNotNull(data, 'contributors', parseResult.maintainers.length);
  }

  addIfNotNull(data, 'repository', _.get(parseResult, 'repository.url', null));
}

function getCleanDependencyName(denpendencyObject) {
  ifDebug(JSON.stringify(denpendencyObject.from));

  // Clean from, example : tslint@^5.12.1
  denpendencyObject.from = _.replace(denpendencyObject.from, /[\^|~]/, '');
  denpendencyObject.from = _.replace(denpendencyObject.from, '@latest', '');

  // Example @angular-devkit/core@0.6.8
  const regex1 = /([@][0-9]+.[0-9]+.[0-9]+)+$/;
  // Example 4.0.0-alpha.16@4.0.0-alpha.16
  const regex2 = /([@][0-9]+.[0-9]+.[0-9]+[-]?[a-z]*.[0-9]+)+$/;

  return denpendencyObject.from.match(regex1) || denpendencyObject.from.match(regex2)
    ? denpendencyObject.from
    : `${denpendencyObject.from}@${denpendencyObject.version}`;
}

function generateNpmViewCommand(dependency) {
  var cleanDependencyName = getCleanDependencyName(dependency);

  return {
    version: dependency.version,
    dependencieRef: cleanDependencyName,
    exec: `cd ${conf.projectPath} && npm view ${cleanDependencyName} name repository.url dist-tags.latest time versions maintainers  -json`
  };
}

function ifDebug(message) {
  if (conf.debug) {
    console.log(message);
  }
}

function fatalError(message) {
  console.log(message);
  process.exit(1);
}

function isBoolean(stringValue) {
  return stringValue === 'true' || stringValue === 'false';
}

function convertToBoolean(stringValue, propertyName) {
  if (isBoolean(stringValue)) {
    return stringValue === 'true' ? true : false;
  } else {
    return fatalError(`${propertyName} ${NOT_A_BOOLEAN}`);
  }
}

async function getCommandList(dependencies) {
  return new Promise(resolve => {
    var commands = [];

    dependencies.forEach(dep => {
      // Depth > 0 => child dependencies
      if (!_.isNil(dep.dependencies)) {
        // Convert in array
        var childDependencies = Object.values(dep.dependencies);
        childDependencies.forEach(childDep => {
          commands.push(generateNpmViewCommand(childDep));
        });
      }

      commands.push(generateNpmViewCommand(dep));
    });
    resolve(commands);
  });
}
