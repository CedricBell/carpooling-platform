package com.intech.covoit.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.intech.covoit.api.AddressDto;
import com.intech.covoit.services.impl.AddressServiceImpl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/address")
public class AddressController {


	private Logger logger = LogManager.getLogger(AddressController.class);

	@Autowired
	AddressServiceImpl addressService;

	@ApiOperation(value = "Create an address")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful create of address", response = AddressDto.class),
			@ApiResponse(code = 500, message = "Internal server error") })
	@PostMapping
	public @ResponseBody AddressDto createAnAddress(
			@ApiParam(required = true, name = "address", value = "New address") @RequestBody @Valid AddressDto address) {

		logger.info("POST /address/");
		return this.addressService.createAddress(address);
	}

	@ApiOperation(value = "Update a specific address")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update of a given id address"),
			@ApiResponse(code = 404, message = "Address with given id does not exist, can't update it"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@PutMapping(value = "/{id}")
	public void updateAddress(
			@ApiParam(required = true, name = "id", value = "ID of the address you want to update") @PathVariable String id,
			@ApiParam(required = true, name = "id", value = "Address with the new properties") @RequestBody @Valid AddressDto addressDto) {
		logger.info("PUT /address/" + id);
		addressService.updateAddress(id,  addressDto);
	}
	
	@ApiOperation(value = "Retrieve the full address list", response = AddressDto.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of address list"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping
	public @ResponseBody List<AddressDto> getAllAddress() {

		return this.addressService.findAllAddress();

	}

	@ApiOperation(value = "Retrieve information about a specific address")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of address", response = AddressDto.class),
			@ApiResponse(code = 404, message = "Address with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "/{id}")
	public @ResponseBody AddressDto getAddressId(
			@ApiParam(required = true, name = "id", value = "ID of the address you want to get") @PathVariable String id) {

		logger.info("GET /address/" + id);
		return this.addressService.findAddressById(id);
	}

	@ApiOperation(value = "Delete a specific address")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete of address"),
			@ApiResponse(code = 404, message = "Address with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@DeleteMapping(value = "/{id}")
	public void deleteAAddress(
			@ApiParam(required = true, name = "id", value = "ID of the address you want to delete") @PathVariable String id) {

		logger.info("DELETE /address/" + id);
		this.addressService.deleteAddressById(id);
	}
}
