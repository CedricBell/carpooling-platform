package com.intech.covoit.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.intech.covoit.api.RideRequestHistoryDto;
import com.intech.covoit.services.impl.RideRequestHistoryServiceImpl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rideRequestHistories")
public class RideRequestHistoryController {

	private Logger logger = LogManager.getLogger(RideRequestHistoryController.class);

	@Autowired
	RideRequestHistoryServiceImpl rideRequestHistoryService;

	@ApiOperation(value = "Retrieve the full ride request history list", response = RideRequestHistoryDto.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of ride request list"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping
	public @ResponseBody List<RideRequestHistoryDto> getAllRideRequestHistories() {
		logger.info("GET /rideRequestHistory/");
		return this.rideRequestHistoryService.findAllRideRequestHistory();

	}

	@ApiOperation(value = "Retrieve information about a specific history")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of ride request history", response = RideRequestHistoryDto.class),
			@ApiResponse(code = 404, message = "Ride request history with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "/{id}")
	public @ResponseBody RideRequestHistoryDto getRideRequestHistoryById(
			@ApiParam(required = true, name = "id", value = "ID of the ride request history you want to get") @PathVariable String id) {

		logger.info("GET /rideRequestHistory/" + id);
		return this.rideRequestHistoryService.findRideRequestHistoryById(id);
	}

	@ApiOperation(value = "Retrieve all the ride request history associated to one ride", response = RideRequestHistoryDto.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of ride request list"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "byRide/{rideId}")
	public @ResponseBody RideRequestHistoryDto getRideRequestHistoryByRideId(
			@ApiParam(required = true, name = "rideId", value = "ID of the ride you want to get") @PathVariable String rideId) {
		logger.info("GET /rideRequestHistory/" + rideId);
		return this.rideRequestHistoryService.findRideRequestHistoryByRideId(rideId);

	}

	@ApiOperation(value = "Create a ride request history")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful create of ride request history", response = RideRequestHistoryDto.class),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping
	public @ResponseBody RideRequestHistoryDto createARideRequestHistory(
			@ApiParam(required = true, name = "rideRequestHistory", value = "New ride request history") @RequestBody @Valid RideRequestHistoryDto rideRequestHistory)
			throws Exception {

		logger.info("POST /rideRequestHistory/");
		return this.rideRequestHistoryService.createRideRequestHistory(rideRequestHistory);
	}

	@ApiOperation(value = "Update information of a specific history")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update of pride request history"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@PutMapping(value = "/{id}")
	public void updateARideRequestHistory(
			@ApiParam(required = true, name = "id", value = "ID of the ride request history you want to update") @PathVariable String id,
			@ApiParam(required = true, name = "rideRequestHistory", value = "Updated ride request history") @RequestBody @Valid RideRequestHistoryDto rideRequestHistory) {

		logger.info("PUT /rideRequestHistory/" + id);
		this.rideRequestHistoryService.updateRideRequestHistory(id, rideRequestHistory);
	}

	@ApiOperation(value = "Delete a specific history")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete of ride request history"),
			@ApiResponse(code = 404, message = "Ride request history with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@DeleteMapping(value = "/{id}")
	public void deleteARideRequestHistory(
			@ApiParam(required = true, name = "id", value = "ID of the ride request history you want to delete") @PathVariable String id) {

		logger.info("DELETE /rideRequestHistory/" + id);
		this.rideRequestHistoryService.deleteRideRequestHistoryById(id);
	}

}