package com.intech.covoit.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.intech.covoit.api.RideDto;
import com.intech.covoit.services.impl.RideServiceImpl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rides")
public class RideController {

	@Autowired
	private RideServiceImpl rideServiceImpl;
	public static final String SECRET = "SecretKeyToGenJWTs";

	private Logger logger = LogManager.getLogger(RideController.class);

	@ApiOperation(value = "Create a new ride", response = RideDto.class, responseContainer = "Ride")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful creation of a new ride"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@ResponseBody
	@PostMapping
	public RideDto createRide(
			@ApiParam(required = true, name = "ride", value = "New ride") @RequestBody @Valid RideDto ridedto) {
		logger.info("POST /rides/");
		return rideServiceImpl.createRide(ridedto);
	}

	@ApiOperation(value = "Retrieve the full rides list", response = RideDto.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfull retrieval of rides list"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping
	public @ResponseBody List<RideDto> showAllRides() {
		logger.info("GET /rides");
		return rideServiceImpl.findAllRides();
	}

	@ApiOperation(value = "Retrieve a specific ride", response = RideDto.class, responseContainer = "Ride")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of specific ride"),
			@ApiResponse(code = 404, message = "Ride with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "/{id}")
	public @ResponseBody RideDto getARide(
			@ApiParam(required = true, name = "id", value = "ID of the ride you want to get") @PathVariable String id) {
		logger.info("GET /rides/" + id);
		return rideServiceImpl.findRideById(id);
	}

	@ApiOperation(value = "Delete a specific ride")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete of a given id ride"),
			@ApiResponse(code = 404, message = "Ride with given id does not exist, can't delete it"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@DeleteMapping(value = "/{id}")
	public void deleteARide(
			@ApiParam(required = true, name = "id", value = "ID of the ride you want to delete") @PathVariable String id) {
		logger.info("DELETE /rides/" + id);
		this.rideServiceImpl.deleteRideById(id);
	}

	@ApiOperation(value = "Update a specific ride")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update of a given id ride"),
			@ApiResponse(code = 404, message = "Ride with given id does not exist, can't update it"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@PutMapping(value = "/{id}")
	public void updateRide(
			@ApiParam(required = true, name = "id", value = "ID of the ride you want to update") @PathVariable String id,
			@ApiParam(required = true, name = "id", value = "Ride with the new properties") @RequestBody @Valid RideDto ridedto) {
		logger.info("PUT /rides/" + id);
		rideServiceImpl.updateRide(id, ridedto);
	}

	@ApiOperation(value = "Retrieve all the rides associated to one user", response = RideDto.class, responseContainer = "Ride")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of alls rides related to one user"),
			@ApiResponse(code = 404, message = "Rides associated to user with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "/forUser")
	public @ResponseBody List<RideDto> getRidesByUsername(@RequestHeader(name = "Authorization") String auth) {
		logger.info("GET /rides/ByUsername/" + auth);
		return rideServiceImpl.getRidesByUsername(auth);

	}

}
