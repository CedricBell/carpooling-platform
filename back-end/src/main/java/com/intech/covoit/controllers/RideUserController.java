package com.intech.covoit.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.intech.covoit.api.RideUserDto;
import com.intech.covoit.services.impl.RideUserServiceImpl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rides/{rideId}/users")
public class RideUserController {

	private Logger logger = LogManager.getLogger(RideUserController.class);

	@Autowired
	RideUserServiceImpl rideUserService;

	@ApiOperation(value = "Retrieve the full ride user list", response = RideUserDto.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of ride user list"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping
	public @ResponseBody List<RideUserDto> getAllRideUsers() {

		return this.rideUserService.findAllRideUser();

	}

	@ApiOperation(value = "Retrieve information about a specific ride user")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of ride user", response = RideUserDto.class),
			@ApiResponse(code = 404, message = "PRide user with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "/{id}")
	public @ResponseBody RideUserDto getRideUserId(
			@ApiParam(required = true, name = "id", value = "ID of the ride user you want to get") @PathVariable String id) {

		logger.info("GET /rideUser/" + id);
		return this.rideUserService.findRideUserById(id);
	}

	@ApiOperation(value = "Update a specific ride user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update of a given id ride user"),
			@ApiResponse(code = 404, message = "Ride user with given id does not exist, can't update it"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@PutMapping(value = "/{id}")
	public void updateRide(
			@ApiParam(required = true, name = "id", value = "ID of the ride user you want to update") @PathVariable String id,
			@PathVariable String rideId,
			@ApiParam(required = true, name = "id", value = "Ride with the new properties") @RequestBody @Valid RideUserDto rideUserDto) {
		logger.info("PUT /rides/" + id);
		rideUserService.updateRideUser(id, rideId, rideUserDto);
	}

	@ApiOperation(value = "Create a place")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful create of ride user", response = RideUserDto.class),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping
	public @ResponseBody RideUserDto createARideUser(@PathVariable String rideId,
			@ApiParam(required = true, name = "rideUser", value = "New ride user") @RequestBody @Valid RideUserDto rideUser) {

		logger.info("POST /rideUser/");
		return this.rideUserService.createRideUser(rideUser, rideId);
	}

	@ApiOperation(value = "Delete a specific ride user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete of ride user"),
			@ApiResponse(code = 404, message = "Ride user with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@DeleteMapping(value = "/{id}")
	public void deleteARideUser(
			@ApiParam(required = true, name = "id", value = "ID of the ride user you want to delete") @PathVariable String id) {

		logger.info("DELETE /rideUser/" + id);
		this.rideUserService.deleteRideUserById(id);
	}

}