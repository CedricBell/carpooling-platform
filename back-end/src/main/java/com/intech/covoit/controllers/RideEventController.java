package com.intech.covoit.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.intech.covoit.api.RideEventDto;
import com.intech.covoit.services.impl.RideEventServiceImpl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rides/{rideId}/events")
public class RideEventController {

	private Logger logger = LogManager.getLogger(RideEventController.class);

	@Autowired
	RideEventServiceImpl rideEventService;

	@ApiOperation(value = "Retrieve the full ride event list", response = RideEventDto.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of ride event list"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping
	public @ResponseBody List<RideEventDto> getAllRideEvents() {

		return this.rideEventService.findAllRideEvent();

	}

	@ApiOperation(value = "Retrieve information about a specific ride event")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of ride event", response = RideEventDto.class),
			@ApiResponse(code = 404, message = "PRide event with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "/{id}")
	public @ResponseBody RideEventDto getRideEventId(
			@ApiParam(required = true, name = "id", value = "ID of the ride event you want to get") @PathVariable String id) {

		logger.info("GET /rideEvent/" + id);
		return this.rideEventService.findRideEventById(id);
	}

	@ApiOperation(value = "Create a place")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful create of ride event", response = RideEventDto.class),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping
	public @ResponseBody RideEventDto createARideEvent(
			@ApiParam(required = true, name = "rideEvent", value = "New ride event") @RequestBody @Valid RideEventDto rideEvent) {

		logger.info("POST /rideEvent/");
		return this.rideEventService.createRideEvent(rideEvent);
	}

	@ApiOperation(value = "Delete a specific ride event")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete of ride event"),
			@ApiResponse(code = 404, message = "Ride event with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@DeleteMapping(value = "/{id}")
	public void deleteARideEvent(
			@ApiParam(required = true, name = "id", value = "ID of the ride event you want to delete") @PathVariable String id) {

		logger.info("DELETE /rideEvent/" + id);
		this.rideEventService.deleteRideEventById(id);
	}

}