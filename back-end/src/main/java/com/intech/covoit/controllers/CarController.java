package com.intech.covoit.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.intech.covoit.api.CarDto;
import com.intech.covoit.services.impl.CarServiceImpl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/cars")
public class CarController {

	private Logger logger = LogManager.getLogger(CarController.class);

	@Autowired
	CarServiceImpl carService;

	@ApiOperation(value = "Create a car")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful create of car", response = CarDto.class),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping
	public @ResponseBody CarDto createACar(
			@ApiParam(required = true, name = "car", value = "New car") @RequestBody @Valid CarDto car) {

		logger.info("POST /car/");
		return this.carService.createCar(car);
	}

	@ApiOperation(value = "Update a specific car")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update of a given id car"),
			@ApiResponse(code = 404, message = "Car with given id does not exist, can't update it"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@PutMapping(value = "/{id}")
	public void updateCar(
			@ApiParam(required = true, name = "id", value = "ID of the car you want to update") @PathVariable String id,
			@ApiParam(required = true, name = "id", value = "Car with the new properties") @RequestBody @Valid CarDto carDto) {
		logger.info("PUT /car/" + id);
		carService.updateCar(id, carDto);
	}

	@ApiOperation(value = "Retrieve the full car list", response = CarDto.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of car list"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping
	public @ResponseBody List<CarDto> getAllCars() {

		return this.carService.findAllCar();

	}

	@ApiOperation(value = "Retrieve information about a specific car")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of car", response = CarDto.class),
			@ApiResponse(code = 404, message = "Car with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "/{id}")
	public @ResponseBody CarDto getCarId(
			@ApiParam(required = true, name = "id", value = "ID of the car you want to get") @PathVariable String id) {

		logger.info("GET /car/" + id);
		return this.carService.findCarById(id);
	}

	@ApiOperation(value = "Delete a specific car")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete of car"),
			@ApiResponse(code = 404, message = "Car with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@DeleteMapping(value = "/{id}")
	public void deleteACar(
			@ApiParam(required = true, name = "id", value = "ID of the car you want to delete") @PathVariable String id) {

		logger.info("DELETE /car/" + id);
		this.carService.deleteCarById(id);
	}
}
