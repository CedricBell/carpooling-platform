package com.intech.covoit.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.intech.covoit.api.UserDto;
import com.intech.covoit.services.impl.UserServiceImpl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserServiceImpl userService;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserController(UserServiceImpl userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.userService = userService;
	}

	private Logger logger = LogManager.getLogger(UserController.class);

	@ApiOperation(value = "Create a new user", response = UserDto.class, responseContainer = "User")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful creation of a new user"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@ResponseBody
	@PostMapping
	public UserDto createUser(
			@ApiParam(required = true, name = "user", value = "New user") @RequestBody @Valid UserDto userdto) {
		logger.info("POST /users/");
		return userService.createUser(userdto);
	}

	@ApiOperation(value = "Retrieve the full users list", response = UserDto.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfull retrieval of users list"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping
	public @ResponseBody List<UserDto> showAllUsers() {
		logger.info("GET /users");
		return userService.findAllUsers();
	}

	@ApiOperation(value = "Retrieve a specific user", response = UserDto.class, responseContainer = "User")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of specific user"),
			@ApiResponse(code = 404, message = "User with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "/{id}")
	public @ResponseBody UserDto getAUser(
			@ApiParam(required = true, name = "id", value = "ID of the user you want to get") @PathVariable String id) {
		logger.info("GET /users/" + id);
		return userService.findUserById(id);
	}

	@ApiOperation(value = "Delete all users")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete of all users"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@DeleteMapping
	public @ResponseBody void deleteAllUsers() {
		logger.info("DELETE /users");
		userService.deleteAllUsers();
	}

	@ApiOperation(value = "Delete a specific user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete of a given id user"),
			@ApiResponse(code = 404, message = "user with given id does not exist, can't delete it"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@DeleteMapping(value = "/{id}")
	public void deleteAUser(
			@ApiParam(required = true, name = "id", value = "ID of the user you want to delete") @PathVariable String id) {
		logger.info("DELETE /users/" + id);
		userService.deleteUserById(id);
	}

	@ApiOperation(value = "Update a specific user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update of a given id user"),
			@ApiResponse(code = 404, message = "User with given id does not exist, can't update it"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@PutMapping(value = "/{id}")
	public void updateUser(
			@ApiParam(required = true, name = "id", value = "ID of the user you want to update") @PathVariable String id,
			@ApiParam(required = true, name = "id", value = "User with the new properties") @RequestBody @Valid UserDto userDto) {
		logger.info("PUT /users/" + id);
		userService.updateUser(id, userDto);
	}

	@ApiOperation(value = "Sign up the user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful sign un of the user"),
			@ApiResponse(code = 404, message = "No user connect"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@PostMapping("/sign-up")
	public void signUp(@ApiParam(required = true, name = "user", value = "user to sign up") @RequestBody UserDto user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userService.createUser(user);
	}

	@ApiOperation(value = "Get the current connect user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful get of the current user"),
			@ApiResponse(code = 404, message = "No current user"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "/me")
	public @ResponseBody UserDto getCurrentUser(
			@ApiParam(required = true, name = "auth", value = "user authentification") @RequestHeader(name = "Authorization") String auth) {

		logger.info("GET /me" + auth);
		auth = auth.replaceAll("Bearer ", "");
		String jwtdecode = "";

		try {
			DecodedJWT jwt = JWT.decode(auth);
			jwtdecode = jwt.getSubject();
			logger.info("totot {}", jwt);
		} catch (JWTDecodeException exception) {
			// Invalid token
		}
		return this.userService.findUserByUsername(jwtdecode);
	}

}
