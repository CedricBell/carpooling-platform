package com.intech.covoit.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.intech.covoit.api.ParkingHistoryDto;
import com.intech.covoit.api.ParkingPlaceDto;
import com.intech.covoit.services.impl.ParkingPlaceServiceImpl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/parkingPlaces")
public class ParkingPlaceController {

	private Logger logger = LogManager.getLogger(ParkingPlaceController.class);

	@Autowired
	ParkingPlaceServiceImpl parkingPlaceServiceImpl;

	@ApiOperation(value = "Retrieve the full parking list", response = ParkingPlaceDto.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of parking list"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping
	public @ResponseBody List<ParkingPlaceDto> getAllParkingPlaces() {

		return this.parkingPlaceServiceImpl.findAllParkingPlace();

	}

	@ApiOperation(value = "Retrieve information about a specific place")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of parking place", response = ParkingPlaceDto.class),
			@ApiResponse(code = 404, message = "Parking place with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "/{id}")
	public @ResponseBody ParkingPlaceDto getParkingPlaceyId(
			@ApiParam(required = true, name = "id", value = "ID of the parking place you want to get") @PathVariable String id) {

		logger.info("GET /parking/" + id);
		return this.parkingPlaceServiceImpl.findParkingPlaceById(id);
	}

	@ApiOperation(value = "Create a place")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful create of parking place", response = ParkingPlaceDto.class),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping
	public @ResponseBody ParkingPlaceDto createAParkingPlace(
			@ApiParam(required = true, name = "parkingPlace", value = "New parking place") @RequestBody @Valid ParkingPlaceDto parkingPlace) {

		logger.info("POST /parking/");
		return this.parkingPlaceServiceImpl.createParkingPlace(parkingPlace);
	}

	@ApiOperation(value = "Update information of a specific place")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update of parking place"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@PutMapping(value = "/{id}")
	public void updateAParkingPlace(
			@ApiParam(required = true, name = "id", value = "ID of the parking place you want to update") @PathVariable String id,
			@ApiParam(required = true, name = "parkinghistory", value = "parking history created") @RequestBody @Valid ParkingHistoryDto parkinghistory) {

		logger.info("PUT/parking/" + id);
		this.parkingPlaceServiceImpl.updateParkingPlace(id, parkinghistory);
	}

	@ApiOperation(value = "Delete a specific place")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete of parking place"),
			@ApiResponse(code = 404, message = "Parking place with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@DeleteMapping(value = "/{id}")
	public void deleteAParkingPlace(
			@ApiParam(required = true, name = "id", value = "ID of the parking place you want to delete") @PathVariable String id) {

		logger.info("DELETE /parking/" + id);
		this.parkingPlaceServiceImpl.deleteParkingPlaceById(id);
	}

}
