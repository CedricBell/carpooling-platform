package com.intech.covoit.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.intech.covoit.api.ParkingHistoryDto;
import com.intech.covoit.services.ParkingHistoryService;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/parkingHistories")
@ApiModel(value = "parkingHistory", description = "Represent a park history")
public class ParkingHistoryController {

	private Logger logger = LogManager.getLogger(ParkingHistoryController.class);

	@Autowired
	ParkingHistoryService parkingHistoryServiceImpl;

	@ApiOperation(value = "Retrieve the full parking history list", response = ParkingHistoryDto.class, responseContainer = "List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of parking history list"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping
	public @ResponseBody List<ParkingHistoryDto> getAllParkingHistories() {
		logger.info("GET /parking/");
		return parkingHistoryServiceImpl.findAllParkingHistory();

	}

	@ApiOperation(value = "Retrieve information about a specific history")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of parking history", response = ParkingHistoryDto.class),
			@ApiResponse(code = 404, message = "Parking History with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@GetMapping(value = "/{id}")
	public @ResponseBody ParkingHistoryDto getParkingHistoryById(
			@ApiParam(required = true, name = "id", value = "ID of the parking history you want to get") @PathVariable String id) {

		logger.info("GET /parking/" + id);
		return parkingHistoryServiceImpl.findParkingHistoryById(id);
	}

	@ApiOperation(value = "Create a parking history")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful create of parking history", response = ParkingHistoryDto.class),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 400, message = "Bad Request") })
	@PostMapping
	public @ResponseBody ParkingHistoryDto createAParkingHistory(
			@ApiParam(required = true, name = "parkinghistory", value = "New parking history") @RequestBody @Valid ParkingHistoryDto parkinghistory)
			throws Exception {

		logger.info("POST /parking/");

		return parkingHistoryServiceImpl.createParkingHistory(parkinghistory);
	}

	@ApiOperation(value = "Update information of a specific history")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update of parking history"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@PutMapping(value = "/{id}")
	public void updateAParkingHistory(
			@ApiParam(required = true, name = "id", value = "ID of the parking history you want to update") @PathVariable String id,
			@ApiParam(required = true, name = "parkinghistory", value = "Updated parking history") @RequestBody @Valid ParkingHistoryDto parkinghistory) {

		logger.info("PUT /parking/" + id);
		parkingHistoryServiceImpl.updateParkingHistory(id, parkinghistory);
	}

	@ApiOperation(value = "Delete a specific history")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful delete of parking history"),
			@ApiResponse(code = 404, message = "Parking History with given id does not exist"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@DeleteMapping(value = "/{id}")
	public void deleteAParkingHistory(
			@ApiParam(required = true, name = "id", value = "ID of the parking history you want to delete") @PathVariable String id) {

		logger.info("DELETE /parking/" + id);
		parkingHistoryServiceImpl.deleteParkingHistoryById(id);
	}

}
