package com.intech.covoit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.intech.covoit.model.RideEvent;

public interface RideEventRepository extends MongoRepository<RideEvent, String> {

}
