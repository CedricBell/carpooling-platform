package com.intech.covoit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.intech.covoit.model.ParkingPlace;

public interface ParkingPlaceRepository extends MongoRepository<ParkingPlace, String> {

}
