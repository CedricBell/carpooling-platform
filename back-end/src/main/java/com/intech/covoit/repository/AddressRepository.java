package com.intech.covoit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.intech.covoit.model.Address;

public interface AddressRepository extends MongoRepository<Address, String> {

}
