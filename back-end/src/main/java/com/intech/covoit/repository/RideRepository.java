package com.intech.covoit.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.intech.covoit.model.Ride;

public interface RideRepository extends MongoRepository<Ride, String>{

	Optional<Ride> findById(String id);
}
