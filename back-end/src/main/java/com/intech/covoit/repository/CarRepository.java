package com.intech.covoit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.intech.covoit.model.Car;

public interface CarRepository extends MongoRepository<Car, String> {

}
