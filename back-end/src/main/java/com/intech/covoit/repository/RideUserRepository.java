package com.intech.covoit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.intech.covoit.model.RideUser;

public interface RideUserRepository extends MongoRepository<RideUser, String> {

}
