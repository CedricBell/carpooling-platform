package com.intech.covoit.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.intech.covoit.model.RideRequestHistory;

public interface RideRequestHistoryRepository extends MongoRepository<RideRequestHistory, String> {
	
	Optional<RideRequestHistory> findByRideId(String id);

}
