package com.intech.covoit.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.intech.covoit.api.UserDto;
import com.intech.covoit.model.User;

// This is an Interface.
// No need Annotation here
public interface UserRepository extends MongoRepository<User, Long> { // Long: Type of user ID.

	User findByUserNo(String userNo);

	User findByFirstName(String firstName);

	Optional<User> findById(String id);

	UserDto findByUsername(String username);

}
