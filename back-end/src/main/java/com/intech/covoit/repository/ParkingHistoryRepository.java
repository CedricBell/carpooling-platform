package com.intech.covoit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.intech.covoit.model.ParkingHistory;

public interface ParkingHistoryRepository extends MongoRepository<ParkingHistory, String> {

}
