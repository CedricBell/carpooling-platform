package com.intech.covoit.database;

import java.time.LocalDateTime;
import java.time.Month;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.intech.covoit.configurations.YAMLConfig;
import com.intech.covoit.model.Address;
import com.intech.covoit.model.Car;
import com.intech.covoit.model.ParkingHistory;
import com.intech.covoit.model.ParkingPlace;
import com.intech.covoit.model.User;
import com.intech.covoit.repository.AddressRepository;
import com.intech.covoit.repository.CarRepository;
import com.intech.covoit.repository.ParkingHistoryRepository;
import com.intech.covoit.repository.ParkingPlaceRepository;
import com.intech.covoit.repository.RideEventRepository;
import com.intech.covoit.repository.RideRepository;
import com.intech.covoit.repository.RideRequestHistoryRepository;
import com.intech.covoit.repository.RideUserRepository;
import com.intech.covoit.repository.UserRepository;

@Configuration
public class CreateDatabase {

	private Logger logger = LogManager.getLogger(CreateDatabase.class);

	@Bean
	CommandLineRunner initDatabase(UserRepository urepo, RideRepository rrepo, ParkingPlaceRepository ppr,
			ParkingHistoryRepository phr, CarRepository crp, YAMLConfig myConfig, RideEventRepository rep,
			RideRequestHistoryRepository rrhr, RideUserRepository rup, AddressRepository arp) {
		logger.info("using environment: " + myConfig.getEnvironment());
		logger.info("name: " + myConfig.getName());

		return args -> {

			urepo.deleteAll();
			rrepo.deleteAll();
			rrhr.deleteAll();
			ppr.deleteAll();
			phr.deleteAll();
			crp.deleteAll();
			rep.deleteAll();
			rup.deleteAll();
			arp.deleteAll();

			logger.info("Preloading database");
			logger.info("Preloading users");
			logger.info("Preloading parking place");
			
			Address address1 = new Address("Metz", "6.1727", "49.119");
			Address address2 = new Address("Kayl", "6.03932", "49.4866");
			arp.save(address1);
			arp.save(address2);

			Car car = new Car("AFGREF12", "Clio 3");
			Car car2 = new Car("JORANE<3", "TESLA XXXXXXX");

			crp.save(car);
			crp.save(car2);


			User user1 = new User("User1", "alban", "schwaller", "alban.schwaller@intech.lu", "alban", "password",
					car, "ASL",address1,"","");
			User user2 = new User("User2", "jorane", "schuster", "jorane.schuster@intech.lu", "jorane", "password",
					car2, "JOR",address2,"","");
			User user3 = new User("User3", "remy", "benoist", "remy.benoist@intech.lu", "remy", "password",
					car, "RBT",address2,"","");
			User user4 = new User("User4", "thibaut", "humbert", "thibaut.humbert@intech.lu", "thibaut", "password",
					car, "THT",address2,"","");
			User user5 = new User("User5", "marine", "pallotta", "marine.pallotta@intech.lu", "marine", "password",
					car, "MPA",address2,"","");
			urepo.save(user1);
			urepo.save(user2);
			urepo.save(user3);
			urepo.save(user4);
			urepo.save(user5);

			
			ParkingPlace p1 = new ParkingPlace(88, "Libre");
			ParkingPlace p2 = new ParkingPlace(89, "Libre");
			ppr.save(p1);
			ppr.save(p2);

			logger.info(ppr.findAll());

			ParkingHistory ph2 = new ParkingHistory(p2, "JOR", "FFDDK13TFVDZ",
					LocalDateTime.of(2019, Month.MAY, 15, 8, 0), LocalDateTime.now());

			phr.save(ph2);

		};
	}

}
