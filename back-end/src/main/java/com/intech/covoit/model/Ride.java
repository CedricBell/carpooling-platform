package com.intech.covoit.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(collection = "ride")
@Builder(toBuilder = true)
public class Ride {

	@Id
	private String id;

	@Type(type = "User")
	@NotNull
	@DBRef
	private User owner;

	@DBRef
	@NotNull(message = "Veuillez indiquer un lieu de départ")
	private Address start;

	@DBRef
	@NotNull
	private Address end;

	@NotNull
	private LocalDateTime date;

	@NotNull
	private Integer capacity;

	@DBRef
	private List<RideUser> users;
	@DBRef
	private List<Address> steps;

}
