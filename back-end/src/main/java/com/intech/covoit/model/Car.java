package com.intech.covoit.model;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "car")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Car {

	@Id
	private String id;

	@NotNull(message = "immatriculation cannot be null")
	private String licensePlate;

	@NotNull(message = "marque cannot be null")
	private String brand;

	public Car(String immatriculation, String marque) {

		this.licensePlate = immatriculation;
		this.brand = marque;
	}

}
