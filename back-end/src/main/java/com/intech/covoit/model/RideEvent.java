package com.intech.covoit.model;

import java.time.LocalDateTime;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "rideEvent")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class RideEvent {

	@Id
	private String id;

	@DBRef
	@NotNull(message = "userId cannot be null")
	private User user;

	@NotNull(message = "status cannot be null")
	private RideRequestStatusEnum status;

	@NotNull(message = "date cannot be null")
	private LocalDateTime date;

	private String comments;

	public RideEvent(User user, RideRequestStatusEnum status, LocalDateTime date, String comments) {
		this.user = user;
		this.status = status;
		this.date = date;
		this.comments = comments;
	}

}