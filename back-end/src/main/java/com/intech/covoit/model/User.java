package com.intech.covoit.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document(collection = "User")
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class User {
	private String password;
	private String username;

	@Id
	private String id;

	private String userNo;

	private String lastName;

	private String firstName;

	private String email;

	private String trigram;

	@DBRef
	private Car car;
	
	@DBRef
	private Address address;
	
	private String picture;
	
	private String fcmToken;

	public User(String userno, String firstname, String lastname, String email, String username, String password,
			Car car, String trigramme, Address address, String picture, String fcmToken) {
		this.userNo = userno;
		this.firstName = firstname;
		this.lastName = lastname;
		this.email = email;
		this.username = username;
		this.car = car;
		this.password = password;
		this.trigram = trigramme;
		this.address = address;
		this.picture = picture;
		this.fcmToken= fcmToken;
	}

}
