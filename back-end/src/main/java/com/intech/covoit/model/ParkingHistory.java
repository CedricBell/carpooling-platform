package com.intech.covoit.model;

import java.time.LocalDateTime;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "parkingHistory")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class ParkingHistory {

	@Id
	private String id;

	@DBRef
	@NotNull(message = "Parking Spot cannot be null")
	private ParkingPlace parkingSpot;

	@NotNull(message = "Trigramm cannot be null")
	@Size(min = 3, max = 3)
	private String trigramOccupant;

	@NotNull
	private LocalDateTime date;

	@NotNull(message = "Immatriculation cannot be null")
	private String licensePlate;

	private LocalDateTime freePlaceDate;

	public ParkingHistory(ParkingPlace parkingSpot, String trigramme, String immatriculation, LocalDateTime date,
			LocalDateTime freedate) {

		this.parkingSpot = parkingSpot;
		this.trigramOccupant = trigramme;
		this.licensePlate = immatriculation;
		this.date = date;
		this.freePlaceDate = freedate;

	}

}
