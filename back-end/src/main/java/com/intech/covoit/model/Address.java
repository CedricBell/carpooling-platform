package com.intech.covoit.model;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "address")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Address {

	@Id
	private String id;

	@NotNull(message = "name cannot be null")
	private String name;

	@NotNull(message = "longitude cannot be null")
	private String longitude;

	@NotNull(message = "latitude cannot be null")
	private String latitude;
	
	public Address(String name, String longitude, String latitude) {

		this.name = name;
		this.longitude = longitude;
		this.latitude = latitude;
	}

}
