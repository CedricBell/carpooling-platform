package com.intech.covoit.model;

import java.util.ArrayList;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "rideRequestHistory")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class RideRequestHistory {
	
	@Id
	private String id;
	
	@DBRef
	@NotNull(message = "ride id cannot be null")
	private Ride ride;

	@NotNull(message ="ride event cannot be null")
	private ArrayList<RideEvent> rideEvents;
	
	public RideRequestHistory(Ride ride, ArrayList<RideEvent> rideEvents ) {

		this.ride = ride;
		this.rideEvents = rideEvents;
	}
}
