package com.intech.covoit.model;

public enum RideRequestStatusEnum {
	RIDE_REQUESTED, RIDE_ACCEPTED, RIDE_REJECTED, RIDE_ABANDONNED;
}
