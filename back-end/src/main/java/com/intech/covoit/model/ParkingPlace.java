package com.intech.covoit.model;

import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "parkingSpot")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class ParkingPlace {

	@org.springframework.data.annotation.Id
	private String id;

	@Field(value = "number")
	@NotNull
	private int number;

	@Field(value = "status")
	@NotNull
	private String status;

	public ParkingPlace(int number, String status) {

		this.number = number;
		this.status = status;
	}

}
