package com.intech.covoit.errors.exceptions;

import lombok.Data;

@Data
public class InformationInvalidException extends RuntimeException {

	private String relatedId;

	public InformationInvalidException(String id) {

		this.relatedId = id;
	}

}
