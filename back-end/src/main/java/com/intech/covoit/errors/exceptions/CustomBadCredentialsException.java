package com.intech.covoit.errors.exceptions;


import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;



public class CustomBadCredentialsException extends AuthenticationException {

	public CustomBadCredentialsException(String msg) {
		super(msg);
	}


}
