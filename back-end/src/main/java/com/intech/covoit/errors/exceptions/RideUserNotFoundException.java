package com.intech.covoit.errors.exceptions;

import java.util.NoSuchElementException;

import lombok.Data;

@Data
public class RideUserNotFoundException extends NoSuchElementException {

	private String relatedId;

	public RideUserNotFoundException(String id) {
		this.relatedId = id;
	}
	
}
