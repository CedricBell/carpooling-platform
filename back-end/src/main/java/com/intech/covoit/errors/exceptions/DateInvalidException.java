package com.intech.covoit.errors.exceptions;

import lombok.Data;

@Data
public class DateInvalidException extends RuntimeException {

	private String relatedId;

	public DateInvalidException(String id) {

		this.relatedId = id;
	}

}
