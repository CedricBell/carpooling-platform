package com.intech.covoit.errors.exceptions;

import lombok.Data;

@Data
public class CapacityInvalidException extends RuntimeException {

	private String relatedId;

	public CapacityInvalidException(String id) {

		this.relatedId = id;
	}

}
