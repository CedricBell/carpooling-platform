package com.intech.covoit.errors.exceptions;

import lombok.Data;

@Data
public class UserAlreadyRejectedInRideException extends RuntimeException {

	private String relatedId;

	public UserAlreadyRejectedInRideException(String id) {

		this.relatedId = id;
	}

}
