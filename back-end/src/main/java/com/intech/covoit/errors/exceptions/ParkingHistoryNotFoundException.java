package com.intech.covoit.errors.exceptions;

import java.util.NoSuchElementException;

import lombok.Data;

@Data
public class ParkingHistoryNotFoundException extends NoSuchElementException {

	private final String relatedId;

	public ParkingHistoryNotFoundException(String id) {
		this.relatedId = id;
	}

}
