package com.intech.covoit.errors.exceptions;

import java.util.NoSuchElementException;

import lombok.Data;

@Data
public class RideNotFoundException extends NoSuchElementException {

	private static final long serialVersionUID = 1L;
	private final String relatedId;

	public RideNotFoundException(String id) {
		this.relatedId = id;
	}

}
