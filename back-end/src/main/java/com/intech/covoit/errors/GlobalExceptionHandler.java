package com.intech.covoit.errors;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.intech.covoit.errors.api.ErrorResponse;
import com.intech.covoit.errors.code.ErrorCodes;
import com.intech.covoit.errors.exceptions.AddressNotFoundException;
import com.intech.covoit.errors.exceptions.CapacityInvalidException;
import com.intech.covoit.errors.exceptions.CarNotFoundException;
import com.intech.covoit.errors.exceptions.CustomBadCredentialsException;
import com.intech.covoit.errors.exceptions.DateInvalidException;
import com.intech.covoit.errors.exceptions.EmailInvalidException;
import com.intech.covoit.errors.exceptions.ForbiddenException;
import com.intech.covoit.errors.exceptions.InformationInvalidException;
import com.intech.covoit.errors.exceptions.InternalServerError;
import com.intech.covoit.errors.exceptions.ParkingHistoryNotFoundException;
import com.intech.covoit.errors.exceptions.ParkingPlaceAlreadyTakenException;
import com.intech.covoit.errors.exceptions.ParkingPlaceNotFoundException;
import com.intech.covoit.errors.exceptions.PictureTooBigException;
import com.intech.covoit.errors.exceptions.RideEventNotFoundException;
import com.intech.covoit.errors.exceptions.RideFullException;
import com.intech.covoit.errors.exceptions.RideNotFoundException;
import com.intech.covoit.errors.exceptions.RideRequestHistoryNotFoundException;
import com.intech.covoit.errors.exceptions.UnAuthorizedException;
import com.intech.covoit.errors.exceptions.UserAlreadyAcceptedInRideException;
import com.intech.covoit.errors.exceptions.UserAlreadyRejectedInRideException;
import com.intech.covoit.errors.exceptions.UserAlreadyRequestedInRideException;
import com.intech.covoit.errors.exceptions.UserNotFoundException;
import com.intech.covoit.errors.exceptions.UserNotInRideException;

@ControllerAdvice
public class GlobalExceptionHandler {

	private Logger logger = LogManager.getLogger(GlobalExceptionHandler.class);

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(AddressNotFoundException.class)
	public @ResponseBody ErrorResponse handleAddressNotFound(AddressNotFoundException e) {
		logger.error(String.format(ErrorCodes.ADDRESS_NOT_FOUND.getMessage(), e.getRelatedId()));

		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.ADDRESS_NOT_FOUND.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<String>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(CarNotFoundException.class)
	public @ResponseBody ErrorResponse handleCarNotFound(CarNotFoundException e) {
		logger.error(String.format(ErrorCodes.CAR_NOT_FOUND.getMessage(), e.getRelatedId()));

		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.CAR_NOT_FOUND.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<String>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(RideEventNotFoundException.class)
	public @ResponseBody ErrorResponse handleRideEventNotFound(RideEventNotFoundException e) {
		logger.error(String.format(ErrorCodes.RIDE_EVENT_NOT_FOUND.getMessage(), e.getRelatedId()));

		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.RIDE_EVENT_NOT_FOUND.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<String>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(RideRequestHistoryNotFoundException.class)
	public @ResponseBody ErrorResponse handleRideRequestHistoryNotFound(RideRequestHistoryNotFoundException e) {
		logger.error(String.format(ErrorCodes.RIDE_REQUEST_HISTORY_NOT_FOUND.getMessage(), e.getRelatedId()));

		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.RIDE_REQUEST_HISTORY_NOT_FOUND.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<String>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(ParkingPlaceNotFoundException.class)
	public @ResponseBody ErrorResponse handleParkingPlaceNotFound(ParkingPlaceNotFoundException e) {
		logger.error(String.format(ErrorCodes.PARKING_PLACE_NOT_FOUND.getMessage(), e.getRelatedId()));

		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.PARKING_PLACE_NOT_FOUND.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(ParkingHistoryNotFoundException.class)
	public @ResponseBody ErrorResponse handleParkingHistoryNotFound(ParkingHistoryNotFoundException e) {
		logger.error(String.format(ErrorCodes.PARKING_HISTORY_NOT_FOUND.getMessage(), e.getRelatedId()));

		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.PARKING_HISTORY_NOT_FOUND.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public @ResponseBody ErrorResponse handleValidationFailed(MethodArgumentNotValidException e) {
		logger.error(HttpStatus.BAD_REQUEST.getReasonPhrase());
		ErrorResponse response = new ErrorResponse();
		response.setCode(ErrorCodes.WRONG_FORMAT.getMessage());
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;

	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(UserNotFoundException.class)
	public @ResponseBody ErrorResponse handleUserNotFound(UserNotFoundException e) {
		logger.error(String.format(ErrorCodes.USER_NOT_FOUND.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.USER_NOT_FOUND.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(RideNotFoundException.class)
	public @ResponseBody ErrorResponse handleRideNotFound(RideNotFoundException e) {
		logger.error(String.format(ErrorCodes.RIDE_NOT_FOUND.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.RIDE_NOT_FOUND.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(InternalServerError.class)
	public @ResponseBody ErrorResponse handleServerError(RuntimeException e) {
		logger.error(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		ErrorResponse response = new ErrorResponse();
		response.setCode(ErrorCodes.INTERNAL_SERVER_ERROR.getMessage());
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler(CustomBadCredentialsException.class)
	public @ResponseBody ErrorResponse handleCustomBadCredentials(CustomBadCredentialsException e) {
		logger.error("ici");
		ErrorResponse response = new ErrorResponse();
		response.setCode(ErrorCodes.FORBIDDEN.getMessage());
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler(ForbiddenException.class)
	public @ResponseBody ErrorResponse handleForbiddenError(AuthenticationException e) {
		logger.error("ici");
		logger.error(HttpStatus.FORBIDDEN.getReasonPhrase());
		ErrorResponse response = new ErrorResponse();
		response.setCode(ErrorCodes.FORBIDDEN.getMessage());
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ExceptionHandler(UnAuthorizedException.class)
	public @ResponseBody ErrorResponse handleUnAuthorized(AuthenticationException e) {
		logger.error(HttpStatus.UNAUTHORIZED.getReasonPhrase());
		ErrorResponse response = new ErrorResponse();
		response.setCode(ErrorCodes.UNAUTHORIZED.getMessage());
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(RideFullException.class)
	public @ResponseBody ErrorResponse handleRideFull(RideFullException e) {
		logger.error(String.format(ErrorCodes.RIDE_FULL.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.RIDE_FULL.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(UserAlreadyAcceptedInRideException.class)
	public @ResponseBody ErrorResponse handleUserAlreadyAcceptedInRide(UserAlreadyAcceptedInRideException e) {
		logger.error(String.format(ErrorCodes.USER_ALREADY_ACCEPTED_IN_RIDE.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.USER_ALREADY_ACCEPTED_IN_RIDE.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(UserAlreadyRequestedInRideException.class)
	public @ResponseBody ErrorResponse handleUserAlreadyRequestedInRide(UserAlreadyRequestedInRideException e) {
		logger.error(String.format(ErrorCodes.USER_ALREADY_REQUESTED_IN_RIDE.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.USER_ALREADY_REQUESTED_IN_RIDE.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(UserNotInRideException.class)
	public @ResponseBody ErrorResponse handleUserNotInRide(UserNotInRideException e) {
		logger.error(String.format(ErrorCodes.USER_NOT_IN_RIDE.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.USER_NOT_IN_RIDE.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(EmailInvalidException.class)
	public @ResponseBody ErrorResponse handleEmailInvalid(EmailInvalidException e) {
		logger.error(String.format(ErrorCodes.EMAIL_INVALID.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.EMAIL_INVALID.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(InformationInvalidException.class)
	public @ResponseBody ErrorResponse handleInformationInvalid(InformationInvalidException e) {
		logger.error(String.format(ErrorCodes.INFORMATION_INVALID.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.INFORMATION_INVALID.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(PictureTooBigException.class)
	public @ResponseBody ErrorResponse handlePictureTooBig(PictureTooBigException e) {
		logger.error(String.format(ErrorCodes.PICTURE_TOO_BIG.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.PICTURE_TOO_BIG.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(CapacityInvalidException.class)
	public @ResponseBody ErrorResponse handleCapacityInvalid(CapacityInvalidException e) {
		logger.error(String.format(ErrorCodes.CAPACITY_INVALID.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.CAPACITY_INVALID.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(UserAlreadyRejectedInRideException.class)
	public @ResponseBody ErrorResponse handleUserAlreadyRejectedInRide(UserAlreadyRejectedInRideException e) {
		logger.error(String.format(ErrorCodes.USER_ALREADY_REJECTED_IN_RIDE.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.USER_ALREADY_REJECTED_IN_RIDE.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(ParkingPlaceAlreadyTakenException.class)
	public @ResponseBody ErrorResponse handleParkingPlaceAlreadyTaken(ParkingPlaceAlreadyTakenException e) {
		logger.error(String.format(ErrorCodes.PARKING_PLACE_ALREADY_TAKEN.getMessage(), e.getRelatedId()));
		ErrorResponse response = new ErrorResponse();
		response.setCode(String.format(ErrorCodes.PARKING_PLACE_ALREADY_TAKEN.getMessage(), e.getRelatedId()));
		List<String> location = new ArrayList<>();
		location.add(e.getStackTrace()[0].getClassName());
		location.add(e.getStackTrace()[0].getMethodName());
		response.setLocation(location);
		response.setLineError(e.getStackTrace()[0].getLineNumber());
		return response;
	}

}
