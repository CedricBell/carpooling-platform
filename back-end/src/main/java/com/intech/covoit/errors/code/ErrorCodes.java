package com.intech.covoit.errors.code;

public enum ErrorCodes {

	RIDE_EVENT_NOT_FOUND("error.rideeventnotfound"), PARKING_PLACE_NOT_FOUND("error.parkingplacenotfound"),
	WRONG_FORMAT("error.wrongformat"), RIDE_REQUEST_HISTORY_NOT_FOUND("error.riderequesthistorynotfound"),
	PARKING_HISTORY_NOT_FOUND("error.parkinghistorynotfound"), USER_NOT_FOUND("error.usernotfound"),
	RIDE_NOT_FOUND("error.ridenotfound"), INTERNAL_SERVER_ERROR("error.internalserver"), FORBIDDEN("error.forbidden"),
	UNAUTHORIZED("error.unauthorized"), JOIN_RIDE_TWICE("error.joinridetwice"),
	PARKING_PLACE_ALREADY_TAKEN("error.parkingplacealreadytaken"),
	USER_ALREADY_ACCEPTED_IN_RIDE("error.useralreadyacceptedinride"),
	USER_ALREADY_REJECTED_IN_RIDE("error.useralreadyarejectedinride"), RIDE_FULL("error.ridefull"),
	DATE_INVALID("error.dateinvalid"), CAPACITY_INVALID("error.capacityinvalid"), CAR_NOT_FOUND("error.carnotfound"),
	USER_NOT_IN_RIDE("error.usernotinride"), ADDRESS_NOT_FOUND("Address with id %s not found"),
	USER_ALREADY_REQUESTED_IN_RIDE("error.useralreadyrequestedinride"),
	PICTURE_TOO_BIG("error.picturetoobig"), INFORMATION_INVALID("error.informationinvalid"), 
	EMAIL_INVALID("error.emailinvalid");

	private final String code;

	ErrorCodes(String code) {
		this.code = code;
	}

	public String getMessage() {
		return code;
	}

}
