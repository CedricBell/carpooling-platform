package com.intech.covoit.errors.exceptions;

import java.util.NoSuchElementException;

import lombok.Data;

@Data
public class RideRequestHistoryNotFoundException extends NoSuchElementException {

	private String relatedId;

	public RideRequestHistoryNotFoundException(String id) {
		this.relatedId = id;
	}

}
