package com.intech.covoit.errors.exceptions;

import lombok.Data;

@Data
public class UserAlreadyRequestedInRideException extends RuntimeException {

	private String relatedId;

	public UserAlreadyRequestedInRideException(String id) {

		this.relatedId = id;
	}
}
