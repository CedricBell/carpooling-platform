package com.intech.covoit.errors.exceptions;

import lombok.Data;

@Data
public class PictureTooBigException extends RuntimeException {

	private String relatedId;

	public PictureTooBigException(String id) {

		this.relatedId = id;
	}

}
