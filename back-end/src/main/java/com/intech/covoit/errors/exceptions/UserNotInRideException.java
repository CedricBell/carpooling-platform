package com.intech.covoit.errors.exceptions;

import lombok.Data;

@Data
public class UserNotInRideException extends RuntimeException {

	private String relatedId;

	public UserNotInRideException(String id) {

		this.relatedId = id;
	}

}
