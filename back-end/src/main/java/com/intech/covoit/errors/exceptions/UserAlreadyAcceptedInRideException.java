package com.intech.covoit.errors.exceptions;

import lombok.Data;

@Data
public class UserAlreadyAcceptedInRideException extends RuntimeException {

	private String relatedId;

	public UserAlreadyAcceptedInRideException(String id) {

		this.relatedId = id;
	}

}
