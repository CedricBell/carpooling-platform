package com.intech.covoit.errors.exceptions;

import java.util.NoSuchElementException;

import lombok.Data;

@Data
public class RideEventNotFoundException extends NoSuchElementException {

	private String relatedId;

	public RideEventNotFoundException(String id) {
		this.relatedId = id;
	}
	
}
