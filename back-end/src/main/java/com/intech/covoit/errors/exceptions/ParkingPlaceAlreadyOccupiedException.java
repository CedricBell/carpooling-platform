package com.intech.covoit.errors.exceptions;

import java.io.IOException;

import lombok.Data;

@Data
public class ParkingPlaceAlreadyOccupiedException extends IOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String relatedId;

	public ParkingPlaceAlreadyOccupiedException(String id) {
		this.relatedId = id;
	}

}
