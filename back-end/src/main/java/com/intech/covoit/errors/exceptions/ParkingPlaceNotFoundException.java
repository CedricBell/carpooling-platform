package com.intech.covoit.errors.exceptions;

import java.util.NoSuchElementException;

import lombok.Data;

@Data
public class ParkingPlaceNotFoundException extends NoSuchElementException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String relatedId;

	public ParkingPlaceNotFoundException(String id) {
		this.relatedId = id;
	}

}
