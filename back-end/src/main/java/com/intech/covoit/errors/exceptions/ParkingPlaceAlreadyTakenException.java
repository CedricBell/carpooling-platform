package com.intech.covoit.errors.exceptions;

import lombok.Data;

@Data
public class ParkingPlaceAlreadyTakenException extends RuntimeException {

	private String relatedId;

	public ParkingPlaceAlreadyTakenException(String id) {

		this.relatedId = id;
	}

}
