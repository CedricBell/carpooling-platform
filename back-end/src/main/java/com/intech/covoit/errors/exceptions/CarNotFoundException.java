package com.intech.covoit.errors.exceptions;

import java.util.NoSuchElementException;

import lombok.Data;

@Data
public class CarNotFoundException extends NoSuchElementException {

	private static final long serialVersionUID = 1L;
	private final String relatedId;

	public CarNotFoundException(String id) {
		this.relatedId = id;
	}

}
