package com.intech.covoit.errors.exceptions;

import lombok.Data;

@Data
public class EmailInvalidException extends RuntimeException {

	private String relatedId;

	public EmailInvalidException(String id) {

		this.relatedId = id;
	}

}
