package com.intech.covoit.errors.exceptions;

import java.util.NoSuchElementException;

import lombok.Data;

@Data
public class UserNotFoundException extends NoSuchElementException {

	private final String relatedId;

	public UserNotFoundException(String id) {
		this.relatedId = id;
	}

}
