package com.intech.covoit.errors.api;

import java.util.List;

import lombok.Data;

@Data
public class ErrorResponse {

	private String code;

	private List<String> location;

	private int lineError;

}
