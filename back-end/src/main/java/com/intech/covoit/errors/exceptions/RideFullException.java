package com.intech.covoit.errors.exceptions;

import lombok.Data;

@Data
public class RideFullException extends RuntimeException {

	private String relatedId;

	public RideFullException(String id) {

		this.relatedId = id;
	}

}
