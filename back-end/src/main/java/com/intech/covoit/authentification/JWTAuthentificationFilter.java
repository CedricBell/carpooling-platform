package com.intech.covoit.authentification;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.server.ResponseStatusException;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.intech.covoit.api.TokenDto;
import com.intech.covoit.api.UserDto;
import com.intech.covoit.constants.AppConstants;
import com.intech.covoit.errors.GlobalExceptionHandler;
import com.intech.covoit.errors.exceptions.CustomBadCredentialsException;
import com.intech.covoit.errors.exceptions.ForbiddenException;
import com.intech.covoit.errors.exceptions.RideNotFoundException;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Remove this class when Active Directory authentification will be available
 */
public class JWTAuthentificationFilter extends UsernamePasswordAuthenticationFilter {

	private String secret = AppConstants.SECRET;

	private int expirationTime = AppConstants.EXPIRATION_TIME;

	public static final String HEADER_STRING = "Authorization";
	public static final String SIGN_UP_URL = "/users/sign-up";
	private AuthenticationManager authenticationManager;

	public JWTAuthentificationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
		try {
			UserDto creds = new ObjectMapper().readValue(request.getInputStream(), UserDto.class);
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(creds.getUsername(),
					creds.getPassword(), new ArrayList<>());
			return authenticationManager.authenticate(token);
		} catch (AuthenticationException | IOException e) {
			/* TODO Auto-generated catch block
				if(e instanceof InternalAuthenticationServiceException ) {
					throw new CustomBadCredentialsException("mauvais login");
				}else if (e instanceof BadCredentialsException) {
					throw new CustomBadCredentialsException("mauvais login");
				}else {
				    logger.error("autre exception");
				}*/
			return null;
		}
					
								
	}
	

	
	

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication auth) throws IOException, ServletException {

		String token = JWT.create().withSubject(((User) auth.getPrincipal()).getUsername())
				.withExpiresAt(new Date(System.currentTimeMillis() + expirationTime)).sign(HMAC512(secret));
		response.addHeader(HEADER_STRING, token);
		TokenDto token2 = new TokenDto();
		token2.setToken(token);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		ObjectMapper mapper = new ObjectMapper();
		out.print(mapper.writeValueAsString(token2));
		out.flush();
	}
	
	@Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException failed) {
        SecurityContextHolder.clearContext();
        logger.error(failed);
        throw new CustomBadCredentialsException("mauvais login");
    }

	/**
	 * Generate JWT with RSA PrivateKey
	 * 
	 * @param login
	 * @return
	 */
	public String generateRS(String login) {
		String jwt = null;
		try {
			KeyStore keystore = KeyStore.getInstance("PKCS12");
			keystore.load(this.getClass().getClassLoader().getResourceAsStream(AppConstants.KEYSTOREPATH),
					AppConstants.KEYSTOREPASSWORD.toCharArray());
			PrivateKey key = (PrivateKey) keystore.getKey(AppConstants.KEYSTOREALIAS,
					AppConstants.KEYSTOREPASSWORD.toCharArray());

			jwt = Jwts.builder().setSubject(login)
					.setExpiration(new Date(System.currentTimeMillis() + AppConstants.EXPIRATION_TIME * 1000))
					.signWith(SignatureAlgorithm.RS256, key).compact();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jwt;
	}

	/**
	 * Parse JWT for RSA certificat
	 * 
	 * @param token
	 */
	public String verifyRS(String token) {
		String user = null;
		try {
			KeyStore keystore = KeyStore.getInstance("PKCS12");
			keystore.load(this.getClass().getClassLoader().getResourceAsStream(AppConstants.KEYSTOREPATH),
					AppConstants.KEYSTOREPASSWORD.toCharArray());
			X509Certificate certificate = (X509Certificate) keystore.getCertificate(AppConstants.KEYSTOREALIAS);
			PublicKey pubKey = certificate.getPublicKey();

			user = Jwts.parser().setSigningKey(pubKey).parseClaimsJws(token).getBody().getSubject();
		} catch (Exception e) {
		}
		return user;
	}

}
