package com.intech.covoit.notification;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.TopicManagementResponse;
import com.intech.covoit.api.UserDto;
import com.intech.covoit.services.impl.UserServiceImpl;

@Service
public class PushNotificationService {

	@Value("#{${app.notifications.defaults}}")
	private Map<String, String> defaults;

	private Logger logger = LogManager.getLogger(PushNotificationService.class);
	private FCMService fcmService;
	private UserServiceImpl userService;

	public PushNotificationService(FCMService fcmService, UserServiceImpl userService) {
		this.fcmService = fcmService;
		this.userService = userService;
	}

	// @Scheduled(initialDelay = 60000, fixedDelay = 60000)
	public void sendSamplePushNotification() {
		try {
			fcmService.sendMessageWithoutData(getSamplePushNotificationRequest());
		} catch (InterruptedException | ExecutionException e) {
			logger.error(e.getMessage());
		}
	}

	// Unused for the moment
	public void sendPushNotification(PushNotificationRequest request) {
		try {
			fcmService.sendMessage(getSamplePayloadData(), request);
		} catch (InterruptedException | ExecutionException e) {
			logger.error(e.getMessage());
		}
	}

	// Unused for the moment
	public void sendMessage(Message request) {
		try {
			System.out.println(fcmService.sendAndGetResponse(request));
		} catch (InterruptedException | ExecutionException e) {
			logger.error(e.getMessage());
		}
	}

	// Unused for the moment
	public void sendPushNotificationWithoutData(PushNotificationRequest request) {
		try {
			fcmService.sendMessageWithoutData(request);
		} catch (InterruptedException | ExecutionException e) {
			logger.error(e.getMessage());
		}
	}

	public void sendPushNotificationToToken(PushNotificationRequest request) {
		try {
			fcmService.sendMessageToToken(request);
		} catch (InterruptedException | ExecutionException e) {
			logger.error(e.getMessage());
		}
	}

	private Map<String, String> getSamplePayloadData() {
		Map<String, String> pushData = new HashMap<>();
		pushData.put("messageId", defaults.get("payloadMessageId"));
		pushData.put("text", defaults.get("payloadData") + " " + LocalDateTime.now());
		return pushData;
	}

	private PushNotificationRequest getSamplePushNotificationRequest() {
		PushNotificationRequest request = new PushNotificationRequest(defaults.get("title"), defaults.get("message"),
				defaults.get("topic"));
		return request;
	}

	public void customSubscribe(String id, String topic, String clientToken)
			throws InterruptedException, ExecutionException {
		UserDto userdto = userService.findUserById(id);
		userdto.setFcmToken(clientToken);
		userService.updateUser(id, userdto);
		System.out.println("User " + userdto.getUsername() + " registered for notifications with token " + clientToken);
	}

	// Unused for the moment
	public void subscribe(String topic, String clientToken) {
		try {
			TopicManagementResponse response = FirebaseMessaging.getInstance()
					.subscribeToTopicAsync(Collections.singletonList(clientToken), topic).get();
			System.out.println(response.getSuccessCount() + " tokens were subscribed successfully");
		} catch (InterruptedException | ExecutionException e) {
			System.out.println(e);
		}
	}
}