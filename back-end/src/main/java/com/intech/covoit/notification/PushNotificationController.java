package com.intech.covoit.notification;

import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;

@RestController
public class PushNotificationController {

	private PushNotificationService pushNotificationService;

	private Logger logger = LogManager.getLogger(PushNotificationController.class);

	public PushNotificationController(PushNotificationService pushNotificationService) {
		this.pushNotificationService = pushNotificationService;
	}

	@PostMapping("/notifications/topic")
	public ResponseEntity sendNotification(@RequestBody PushNotificationRequest request) {
		logger.info("Posting a notification to notification/topic");
		pushNotificationService.sendPushNotificationWithoutData(request);
		return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."),
				HttpStatus.OK);
	}

	@PostMapping("/notifications/token")
	public ResponseEntity sendTokenNotification(@RequestBody PushNotificationRequest request) {
		logger.info("Posting a notification to token");
		pushNotificationService.sendPushNotificationToToken(request);
		return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."),
				HttpStatus.OK);
	}

	@PostMapping("/notifications/data")
	public ResponseEntity sendDataNotification(@RequestBody PushNotificationRequest request) {
		logger.info("Posting a notification to data");
		pushNotificationService.sendPushNotification(request);
		return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."),
				HttpStatus.OK);
	}

	@GetMapping("/notifications")
	public ResponseEntity sendSampleNotification() {
		logger.info("Posting a notification ");
		pushNotificationService.sendSamplePushNotification();
		return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."),
				HttpStatus.OK);
	}

	@PostMapping("notifications/register")
	public ResponseEntity register(@RequestBody PushNotificationRequest request)
			throws InterruptedException, ExecutionException {
		logger.info("Subscribing someone" + request);
		pushNotificationService.customSubscribe(request.getTitle(), request.getTopic(), request.getToken());
		return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(),
				String.format("subscription to %s succesfully added", request.getTopic())), HttpStatus.OK);
	}

	@PostMapping("/message/token")
	public ResponseEntity sendTokenNotification(@RequestBody Message request) throws FirebaseMessagingException {
		logger.info("Posting a notification to token");

		System.out.println(request);
		pushNotificationService.sendMessage(request);

		String response = FirebaseMessaging.getInstance().send(request);
		// Response is a message ID string.
		System.out.println("Successfully sent message: " + response);
		return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."),
				HttpStatus.OK);
	}

}
