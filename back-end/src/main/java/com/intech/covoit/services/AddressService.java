package com.intech.covoit.services;

import java.util.List;

import javax.validation.Valid;

import com.intech.covoit.api.AddressDto;

public interface AddressService {
	
	public AddressDto createAddress (@Valid AddressDto addressDto);
	
	public void updateAddress(String id,@Valid AddressDto addressDto);
	
	public List<AddressDto> findAllAddress();
	
	public AddressDto findAddressById(String id);
	
	public void deleteAddressById(String id);
	
}
