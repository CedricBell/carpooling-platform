package com.intech.covoit.services.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.intech.covoit.api.RideDto;
import com.intech.covoit.api.RideUserDto;
import com.intech.covoit.constants.AppConstants;
import com.intech.covoit.converters.RideConverter;
import com.intech.covoit.errors.exceptions.CapacityInvalidException;
import com.intech.covoit.errors.exceptions.DateInvalidException;
import com.intech.covoit.errors.exceptions.RideNotFoundException;
import com.intech.covoit.model.Ride;
import com.intech.covoit.model.RideRequestStatusEnum;
import com.intech.covoit.repository.RideRepository;
import com.intech.covoit.services.RideService;

@Service
public class RideServiceImpl implements RideService {

	@Autowired
	private RideRepository rideRepository;

	@Autowired
	private RideConverter rideConverter;

	public RideServiceImpl(RideRepository rideRepository, RideConverter rideConverter) {
		this.rideRepository = rideRepository;
		this.rideConverter = rideConverter;
	}

	public RideDto createRide(@Valid RideDto ridedto) {
		Ride newRide = new Ride();
		newRide = rideConverter.mergeEntity(newRide, ridedto);

		if (!isDateValid(ridedto))
			throw new DateInvalidException(ridedto.getId());
		if (!isCapacityValid(ridedto))
			throw new CapacityInvalidException(ridedto.getId());

		Ride rideCreated = rideRepository.save(newRide);
		return rideConverter.convert(rideCreated);
	}

	public void updateRide(String id, RideDto ridedto) {
		Ride existingRide = rideRepository.findById(id).orElseThrow(() -> new RideNotFoundException(id));

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT);
		existingRide.setDate(LocalDateTime.parse(ridedto.getDate(), formatter));

		BeanUtils.copyProperties(ridedto, existingRide);
		existingRide = rideConverter.mergeEntity(existingRide, ridedto);

		if (!isDateValid(ridedto))
			throw new DateInvalidException(ridedto.getId());
		if (!isCapacityValid(ridedto))
			throw new CapacityInvalidException(ridedto.getId());

		this.rideRepository.save(existingRide);
	}

	public List<RideDto> findAllRides() {
		return StreamSupport.stream(rideRepository.findAll().spliterator(), false).map(rideConverter::convert)
				.collect(Collectors.toList());
	}

	public RideDto findRideById(String id) {
		return rideRepository.findById(id).map(rideConverter::convert).orElseThrow(() -> new RideNotFoundException(id));
	}

	public void deleteRideById(String id) {
		Ride ride = rideRepository.findById(id).orElseThrow(() -> new RideNotFoundException(id));
		rideRepository.delete(ride);
	}

	public boolean isDateValid(RideDto rideDto) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT);
		LocalDateTime rideDate = LocalDateTime.parse(rideDto.getDate(), formatter);
		if (rideDate.isBefore(LocalDateTime.now()))
			return false;

		return true;
	}

	public boolean isCapacityValid(RideDto rideDto) {

		int nbParticipants = 0;

		for (RideUserDto rideUser : rideDto.getUsers()) {

			if (rideUser.getStatus().equals(RideRequestStatusEnum.RIDE_ACCEPTED))
				nbParticipants++;
		}

		if (rideDto.getCapacity() <= 0 || rideDto.getCapacity() < nbParticipants)
			return false;

		return true;
	}

	public List<RideDto> getRidesByUsername(String auth) {

		auth = auth.replaceAll("Bearer ", "");
		List<RideDto> ridesDto = null;

		try {
			DecodedJWT jwt = JWT.decode(auth);
			final String jwtdecode = jwt.getSubject();
			ridesDto = findAllRides().stream().filter(ride -> {

				DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT);
				LocalDateTime rideDate = LocalDateTime.parse(ride.getDate(), formatter);
				return ride.getOwner().getUsername().equals(jwtdecode) && rideDate.isAfter(LocalDateTime.now());
			}).collect(Collectors.toList());
		} catch (JWTDecodeException exception) {
			// Invalid token
		}

		return ridesDto;
	}
}
