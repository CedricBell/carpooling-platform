package com.intech.covoit.services;

import java.util.List;

import com.intech.covoit.api.ParkingHistoryDto;
import com.intech.covoit.api.ParkingPlaceDto;

public interface ParkingPlaceService {

	public ParkingPlaceDto createParkingPlace(ParkingPlaceDto parkingplace);

	public void updateParkingPlace(String id, ParkingHistoryDto parkinghistory);

	public List<ParkingPlaceDto> findAllParkingPlace();

	public ParkingPlaceDto findParkingPlaceById(String id);

	public void deleteParkingPlaceById(String id);

	public void updateStatus();

}
