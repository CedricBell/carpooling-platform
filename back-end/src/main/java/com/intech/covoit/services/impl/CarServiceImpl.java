package com.intech.covoit.services.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intech.covoit.api.CarDto;
import com.intech.covoit.converters.CarConverter;
import com.intech.covoit.errors.exceptions.CarNotFoundException;
import com.intech.covoit.model.Car;
import com.intech.covoit.repository.CarRepository;
import com.intech.covoit.services.CarService;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	private CarRepository carRepository;
	@Autowired
	private CarConverter carConverter;

	public CarServiceImpl(CarRepository carRepository, CarConverter carConverter) {
		this.carRepository = carRepository;
		this.carConverter = carConverter;
	}

	public CarDto createCar(CarDto carDto) {
		Car newCar = new Car();
		newCar = carConverter.mergeEntity(newCar, carDto);
		Car carSaved = this.carRepository.save(newCar);
		return carConverter.convert(carSaved);
	}

	public void updateCar(String id, CarDto carDto) {
		Car existingCar = carRepository.findById(id).orElseThrow(() -> new CarNotFoundException(id));
		BeanUtils.copyProperties(carDto, existingCar);
		carRepository.save(existingCar);
	}

	public List<CarDto> findAllCar() {
		return StreamSupport.stream(carRepository.findAll().spliterator(), false).map(carConverter::convert)
				.collect(Collectors.toList());
	}

	public CarDto findCarById(String id) {
		return carRepository.findById(id).map(carConverter::convert).orElseThrow(() -> new CarNotFoundException(id));

	}

	public void deleteCarById(String id) {
		Car car = carRepository.findById(id).orElseThrow(() -> new CarNotFoundException(id));
		carRepository.delete(car);
	}
}
