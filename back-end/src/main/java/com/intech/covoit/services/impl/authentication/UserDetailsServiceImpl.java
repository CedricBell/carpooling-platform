package com.intech.covoit.services.impl.authentication;

import static java.util.Collections.emptyList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.intech.covoit.api.UserDto;
import com.intech.covoit.services.impl.UserServiceImpl;

@Service
public class UserDetailsServiceImpl implements UserDetailsService { 
	private UserServiceImpl userService;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserDetailsServiceImpl(UserServiceImpl userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userService = userService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public UserDetails loadUserByUsername(String userNo)  {
		UserDto applicationUser = userService.findUserByUsername(userNo);
		applicationUser.setPassword(bCryptPasswordEncoder.encode(applicationUser.getPassword()));
		return new User(applicationUser.getUsername(), applicationUser.getPassword(), emptyList());
	}
}