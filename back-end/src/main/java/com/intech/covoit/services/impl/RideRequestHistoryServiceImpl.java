package com.intech.covoit.services.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intech.covoit.api.RideRequestHistoryDto;
import com.intech.covoit.converters.RideRequestHistoryConverter;
import com.intech.covoit.errors.exceptions.RideRequestHistoryNotFoundException;
import com.intech.covoit.model.RideRequestHistory;
import com.intech.covoit.repository.RideRequestHistoryRepository;

@Service
public class RideRequestHistoryServiceImpl {

	@Autowired
	private RideRequestHistoryRepository rideRequestHistoryRepository;
	@Autowired
	private RideRequestHistoryConverter rideRequestHistoryConverter;

	public RideRequestHistoryServiceImpl(RideRequestHistoryRepository rideRequestHistoryRepository,
			RideRequestHistoryConverter rideRequestHistoryConverter) {

		this.rideRequestHistoryRepository = rideRequestHistoryRepository;
		this.rideRequestHistoryConverter = rideRequestHistoryConverter;
	}

	public void updateRideRequestHistory(String id, RideRequestHistoryDto entryRideRequestHistory) {
		RideRequestHistory existingRideRequestHistory = rideRequestHistoryRepository.findById(id)
				.orElseThrow(() -> new RideRequestHistoryNotFoundException(id));
		existingRideRequestHistory = rideRequestHistoryConverter.mergeEntity(existingRideRequestHistory,
				entryRideRequestHistory);
		rideRequestHistoryRepository.save(existingRideRequestHistory);
	}

	public RideRequestHistoryDto findRideRequestHistoryById(String id) {
		return rideRequestHistoryRepository.findById(id).map(rideRequestHistoryConverter::convert)
				.orElseThrow(() -> new RideRequestHistoryNotFoundException(id));
	}

	public List<RideRequestHistoryDto> findAllRideRequestHistory() {
		return StreamSupport.stream(rideRequestHistoryRepository.findAll().spliterator(), false)
				.map(rideRequestHistoryConverter::convert).collect(Collectors.toList());

	}

	public RideRequestHistoryDto findRideRequestHistoryByRideId(String id) {
		return rideRequestHistoryRepository.findByRideId(id).map(rideRequestHistoryConverter::convert)
				.orElseThrow(() -> new RideRequestHistoryNotFoundException(id));
	}

	public RideRequestHistoryDto createRideRequestHistory(RideRequestHistoryDto rideRequestHistory) {

		RideRequestHistory newRideRequestHistory = new RideRequestHistory();
		newRideRequestHistory = rideRequestHistoryConverter.mergeEntity(newRideRequestHistory, rideRequestHistory);
		RideRequestHistory rideRequestHistoryCreated = rideRequestHistoryRepository.save(newRideRequestHistory);
		return rideRequestHistoryConverter.convert(rideRequestHistoryCreated);

	}

	public void deleteRideRequestHistoryById(String id) {
		rideRequestHistoryRepository.deleteById(id);
	}

}