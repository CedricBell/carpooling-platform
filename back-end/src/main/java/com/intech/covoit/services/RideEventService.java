package com.intech.covoit.services;

import java.util.List;

import com.intech.covoit.api.RideEventDto;

public interface RideEventService {

	public RideEventDto createRideEvent(RideEventDto rideEvent);

	public List<RideEventDto> findAllRideEvent();

	public RideEventDto findRideEventById(String id);

	public void deleteRideEventById(String id);

}
