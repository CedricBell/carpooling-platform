package com.intech.covoit.services.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intech.covoit.api.ParkingHistoryDto;
import com.intech.covoit.api.ParkingPlaceDto;
import com.intech.covoit.api.ParkingPlaceUpdateDto;
import com.intech.covoit.controllers.ParkingPlaceController;
import com.intech.covoit.converters.ParkingPlaceConverter;
import com.intech.covoit.errors.exceptions.ParkingPlaceNotFoundException;
import com.intech.covoit.model.ParkingPlace;
import com.intech.covoit.repository.ParkingPlaceRepository;
import com.intech.covoit.services.ParkingPlaceService;

@Service
public class ParkingPlaceServiceImpl implements ParkingPlaceService {

	private Logger logger = LogManager.getLogger(ParkingPlaceController.class);

	@Autowired
	private ParkingPlaceRepository parkingPlaceRepository;

	@Autowired
	private ParkingPlaceConverter parkingplaceConverter;

	@Autowired
	private ParkingHistoryServiceImpl parkingHistoryServiceImpl;

	public ParkingPlaceServiceImpl(ParkingPlaceRepository parkingPlaceRepository,
			ParkingPlaceConverter parkingplaceConverter, ParkingHistoryServiceImpl parkingHistoryServiceImpl) {

		this.parkingPlaceRepository = parkingPlaceRepository;
		this.parkingplaceConverter = parkingplaceConverter;
		this.parkingHistoryServiceImpl = parkingHistoryServiceImpl;
	}

	public @ResponseBody ParkingPlaceDto createParkingPlace(ParkingPlaceDto parkingplace) {

		ParkingPlace newParkingPlace = new ParkingPlace();
		newParkingPlace = parkingplaceConverter.mergeEntity(newParkingPlace, parkingplace);
		ParkingPlace parkingPlaceCreated = parkingPlaceRepository.save(newParkingPlace);

		return parkingplaceConverter.convert(parkingPlaceCreated);
	}

	public void updateParkingPlace(String id, ParkingHistoryDto parkinghistory) {

		ParkingPlace existingParkingplace = parkingPlaceRepository.findById(id)
				.orElseThrow(() -> new ParkingPlaceNotFoundException(id));

		BeanUtils.copyProperties(parkinghistory.getParkingSpot(), existingParkingplace);

		parkingPlaceRepository.save(existingParkingplace);

		try {

			parkingHistoryServiceImpl.createParkingHistory(parkinghistory);
		} catch (Exception e) {

			ParkingPlaceUpdateDto updatePlace = new ParkingPlaceUpdateDto(parkinghistory.getParkingSpot().getNumber(),
					"Libre");

			BeanUtils.copyProperties(updatePlace, existingParkingplace);
			parkingPlaceRepository.save(existingParkingplace);
		}

	}

	public List<ParkingPlaceDto> findAllParkingPlace() {

		return StreamSupport.stream(parkingPlaceRepository.findAll().spliterator(), false)
				.map(parkingplaceConverter::convert).collect(Collectors.toList());

	}

	public ParkingPlaceDto findParkingPlaceById(String id) {

		return parkingPlaceRepository.findById(id).map(parkingplaceConverter::convert)
				.orElseThrow(() -> new ParkingPlaceNotFoundException(id));

	}

	public void deleteParkingPlaceById(String id) {

		ParkingPlace parkingPlace = parkingPlaceRepository.findById(id)
				.orElseThrow(() -> new ParkingPlaceNotFoundException(id));

		parkingPlaceRepository.delete(parkingPlace);
	}

	@Scheduled(cron = "0 0 4 * * ?")
	public void updateStatus() {

		logger.info("Update all parking places to free");

		parkingPlaceRepository.findAll().forEach(parkingPlace -> {

			parkingPlace.setStatus("Libre");
			parkingPlaceRepository.save(parkingPlace);

		});

	}

}
