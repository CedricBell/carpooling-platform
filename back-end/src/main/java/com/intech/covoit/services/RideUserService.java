package com.intech.covoit.services;

import java.util.List;

import com.intech.covoit.api.RideUserDto;

public interface RideUserService {

	public RideUserDto createRideUser(RideUserDto rideUser);

	public List<RideUserDto> findAllRideUser();

	public RideUserDto findRideUserById(String id);

	public void deleteRideUserById(String id);

}
