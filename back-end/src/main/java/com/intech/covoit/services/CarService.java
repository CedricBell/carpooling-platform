package com.intech.covoit.services;

import java.util.List;

import com.intech.covoit.api.CarDto;

public interface CarService {

	public CarDto createCar(CarDto carDto);

	public void updateCar(String id, CarDto carDto);

	public List<CarDto> findAllCar();

	public CarDto findCarById(String id);

	public void deleteCarById(String id);

}
