package com.intech.covoit.services;

import java.util.List;

import com.intech.covoit.api.RideDto;

public interface RideService {

	public RideDto createRide(RideDto ridedto);

	public void updateRide(String id, RideDto ridedto);

	public List<RideDto> findAllRides();

	public RideDto findRideById(String id);

	public void deleteRideById(String id);

}
