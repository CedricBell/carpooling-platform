package com.intech.covoit.services.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intech.covoit.api.RideEventDto;
import com.intech.covoit.controllers.RideEventController;
import com.intech.covoit.converters.RideEventConverter;
import com.intech.covoit.errors.exceptions.RideEventNotFoundException;
import com.intech.covoit.model.RideEvent;
import com.intech.covoit.repository.RideEventRepository;

@Service
public class RideEventServiceImpl {

	@Autowired
	private RideEventRepository rideEventRepository;
	@Autowired
	private RideEventConverter rideEventConverter;

	private Logger logger = LogManager.getLogger(RideEventController.class);

	public RideEventServiceImpl(RideEventRepository rideEventRepository, RideEventConverter rideEventConverter) {

		this.rideEventRepository = rideEventRepository;
		this.rideEventConverter = rideEventConverter;
	}

	public RideEventDto createRideEvent(RideEventDto rideEvent) {

		RideEvent newRideEvent = new RideEvent();
		newRideEvent = rideEventConverter.mergeEntity(newRideEvent, rideEvent);
		RideEvent rideEventCreated = rideEventRepository.save(newRideEvent);

		return rideEventConverter.convert(rideEventCreated);
	}

	public List<RideEventDto> findAllRideEvent() {

		return StreamSupport.stream(rideEventRepository.findAll().spliterator(), false).map(rideEventConverter::convert)
				.collect(Collectors.toList());
	}

	public RideEventDto findRideEventById(String id) {

		return rideEventRepository.findById(id).map(rideEventConverter::convert)
				.orElseThrow(() -> new RideEventNotFoundException(id));

	}

	public void deleteRideEventById(String id) {

		RideEvent rideEvent = rideEventRepository.findById(id).orElseThrow(() -> new RideEventNotFoundException(id));

		rideEventRepository.delete(rideEvent);
	}

}