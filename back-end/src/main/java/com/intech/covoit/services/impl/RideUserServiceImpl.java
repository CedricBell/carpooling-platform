package com.intech.covoit.services.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intech.covoit.api.RideDto;
import com.intech.covoit.api.RideUserDto;
import com.intech.covoit.constants.AppConstants;
import com.intech.covoit.converters.RideConverter;
import com.intech.covoit.converters.RideUserConverter;
import com.intech.covoit.errors.exceptions.RideFullException;
import com.intech.covoit.errors.exceptions.RideUserNotFoundException;
import com.intech.covoit.errors.exceptions.UserAlreadyAcceptedInRideException;
import com.intech.covoit.errors.exceptions.UserAlreadyRejectedInRideException;
import com.intech.covoit.errors.exceptions.UserAlreadyRequestedInRideException;
import com.intech.covoit.model.Ride;
import com.intech.covoit.model.RideRequestStatusEnum;
import com.intech.covoit.model.RideUser;
import com.intech.covoit.repository.RideUserRepository;

@Service
public class RideUserServiceImpl {

	@Autowired
	private RideUserRepository rideUserRepository;
	@Autowired
	private RideServiceImpl rideService;
	@Autowired
	private RideUserConverter rideUserConverter;
	@Autowired
	private RideConverter rideConverter;

	public RideUserServiceImpl(RideUserRepository rideUserRepository, RideUserConverter rideUserConverter) {

		this.rideUserRepository = rideUserRepository;
		this.rideUserConverter = rideUserConverter;
	}

	public RideUserDto createRideUser(RideUserDto rideUser, String rideId) {

		RideDto rideDto = rideService.findRideById(rideId);
		Ride newRide = new Ride();
		BeanUtils.copyProperties(rideDto, newRide);
		newRide = rideConverter.mergeEntity(newRide, rideDto);

		RideUser newRideUser = new RideUser();
		newRideUser = rideUserConverter.mergeEntity(newRideUser, rideUser);
		RideUser rideUserCreated = rideUserRepository.save(newRideUser);

		if (isUserAcceptedInRide(newRide, rideUser))
			throw new UserAlreadyAcceptedInRideException(rideUser.getId());
		if (isUserRequestedInRide(newRide, rideUser))
			throw new UserAlreadyRequestedInRideException(rideUser.getId());
		if (isUserRejectedInRide(newRide, rideUser))
			throw new UserAlreadyAcceptedInRideException(rideUser.getId());
		if (isRideFull(newRide, rideUser))
			throw new RideFullException(rideUser.getId());

		return rideUserConverter.convert(rideUserCreated);
	}

	public List<RideUserDto> findAllRideUser() {

		return StreamSupport.stream(rideUserRepository.findAll().spliterator(), false).map(rideUserConverter::convert)
				.collect(Collectors.toList());
	}

	public RideUserDto findRideUserById(String id) {

		return rideUserRepository.findById(id).map(rideUserConverter::convert)
				.orElseThrow(() -> new RideUserNotFoundException(id));

	}

	public void deleteRideUserById(String id) {

		RideUser rideUser = rideUserRepository.findById(id).orElseThrow(() -> new RideUserNotFoundException(id));

		rideUserRepository.delete(rideUser);
	}

	public void updateRideUser(String id, String rideId, @Valid RideUserDto rideUserDto) {

		RideDto rideDto = rideService.findRideById(rideId);
		Ride newRide = new Ride();
		BeanUtils.copyProperties(rideDto, newRide);
		newRide = rideConverter.mergeEntity(newRide, rideDto);

		RideUser existingRideUser = rideUserRepository.findById(id)
				.orElseThrow(() -> new RideUserNotFoundException(id));

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT);
		existingRideUser.setDate(LocalDateTime.parse(rideUserDto.getDate(), formatter));

		BeanUtils.copyProperties(rideUserDto, existingRideUser);
		existingRideUser = rideUserConverter.mergeEntity(existingRideUser, rideUserDto);

		if (rideUserDto.getStatus().equals(RideRequestStatusEnum.RIDE_ABANDONNED)) {
			if (isUserRejectedInRide(newRide, rideUserDto))
				throw new UserAlreadyRejectedInRideException(rideUserDto.getId());

		} else {
			if (isUserAcceptedInRide(newRide, rideUserDto))
				throw new UserAlreadyAcceptedInRideException(rideUserDto.getId());
			if (isUserRejectedInRide(newRide, rideUserDto))
				throw new UserAlreadyRejectedInRideException(rideUserDto.getId());
			if (isRideFull(newRide, rideUserDto))
				throw new RideFullException(rideUserDto.getId());
		}

		this.rideUserRepository.save(existingRideUser);
	}

	public boolean isUserAcceptedInRide(Ride rideBefore, RideUserDto rideUser) {

		List<RideUser> rideUsersBefore = rideBefore.getUsers().stream().filter(rideUserTested -> {
			if (rideUserTested.getStatus().equals(RideRequestStatusEnum.RIDE_ACCEPTED)) {
				if (rideUser.equals(rideUserTested))
					return true;
			}
			return false;
		}).collect(Collectors.toList());

		return rideUsersBefore.size() != 0;

	}

	public boolean isUserRequestedInRide(Ride rideBefore, RideUserDto rideUser) {

		List<RideUser> rideUsersBefore = rideBefore.getUsers().stream().filter(rideUserTested -> {
			if (rideUserTested.getStatus().equals(RideRequestStatusEnum.RIDE_REQUESTED)) {
				if (rideUser.equals(rideUserTested))
					return true;
			}
			return false;
		}).collect(Collectors.toList());

		return rideUsersBefore.size() != 0;

	}

	public boolean isUserRejectedInRide(Ride rideBefore, RideUserDto rideUser) {

		List<RideUser> rideUsersBefore = rideBefore.getUsers().stream().filter(rideUserTested -> {
			if (rideUserTested.getStatus().equals(RideRequestStatusEnum.RIDE_REJECTED)) {
				if (rideUser.equals(rideUserTested))
					return true;
			}
			return false;
		}).collect(Collectors.toList());

		return rideUsersBefore.size() != 0;

	}

	public boolean isRideFull(Ride rideBefore, RideUserDto rideUserDto) {

		int nbParticiants = 0;
		for (RideUser rideUser : rideBefore.getUsers()) {
			if (rideUser.getStatus() == RideRequestStatusEnum.RIDE_ACCEPTED)
				nbParticiants++;
		}

		if (rideBefore.getCapacity() - nbParticiants == 0)
			return true;
		return false;
	}

}