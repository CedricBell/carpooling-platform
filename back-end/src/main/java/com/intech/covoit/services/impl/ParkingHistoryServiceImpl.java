package com.intech.covoit.services.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intech.covoit.api.ParkingHistoryDto;
import com.intech.covoit.constants.AppConstants;
import com.intech.covoit.converters.ParkingHistoryConverter;
import com.intech.covoit.errors.exceptions.ParkingHistoryNotFoundException;
import com.intech.covoit.errors.exceptions.ParkingPlaceAlreadyTakenException;
import com.intech.covoit.model.ParkingHistory;
import com.intech.covoit.repository.ParkingHistoryRepository;
import com.intech.covoit.services.ParkingHistoryService;

@Service
public class ParkingHistoryServiceImpl implements ParkingHistoryService {

	@Autowired
	private ParkingHistoryRepository parkingHistoryRepository;
	@Autowired
	private ParkingHistoryConverter parkinghistoryConverter;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateParkingHistory(String id, ParkingHistoryDto entryParkinghistory) {

		ParkingHistory existingParkinghistory = parkingHistoryRepository.findById(id)
				.orElseThrow(() -> new ParkingHistoryNotFoundException(id));

		BeanUtils.copyProperties(entryParkinghistory, existingParkinghistory);

		parkingHistoryRepository.save(existingParkinghistory);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ParkingHistoryDto findParkingHistoryById(String id) {

		return parkingHistoryRepository.findById(id).map(parkinghistoryConverter::convert)
				.orElseThrow(() -> new ParkingHistoryNotFoundException(id));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ParkingHistoryDto> findAllParkingHistory() {

		return StreamSupport.stream(parkingHistoryRepository.findAll().spliterator(), false)
				.map(parkinghistoryConverter::convert).collect(Collectors.toList());

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ParkingHistoryDto createParkingHistory(ParkingHistoryDto parkingHistory) {

		List<ParkingHistoryDto> historyFiltered = findAllParkingHistory().stream().filter(
				existingParkingHistory -> aPlaceIsAlreadyOccupatedByUser(parkingHistory, existingParkingHistory))
				.collect(Collectors.toList());

		if (historyFiltered.size() != 0)
			throw new ParkingPlaceAlreadyTakenException("Une place a deja été réservée à ce nom aujourd'hui !");

		ParkingHistory newParkingHistory = new ParkingHistory();
		newParkingHistory = parkinghistoryConverter.mergeEntity(newParkingHistory, parkingHistory);
		ParkingHistory parkingHistorySaved = parkingHistoryRepository.save(newParkingHistory);
		return parkinghistoryConverter.convert(parkingHistorySaved);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean aPlaceIsAlreadyOccupatedByUser(ParkingHistoryDto parkinghistory,
			ParkingHistoryDto existingParkingHistory) {

		/* Verification qu'une occupation avec le même trigramme est deja enregistré */

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT);
		LocalDate historyDate = LocalDateTime.parse(existingParkingHistory.getDate(), formatter).toLocalDate();

		boolean dateHistoryEqualsAndPlaceNotFree = historyDate.equals(LocalDate.now())
				&& existingParkingHistory.getFreePlaceDate() == null
				&& existingParkingHistory.getTrigramOccupant().equals(parkinghistory.getTrigramOccupant());

		return dateHistoryEqualsAndPlaceNotFree;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteParkingHistoryById(String id) {

		parkingHistoryRepository.deleteById(id);

	}

}
