package com.intech.covoit.services.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intech.covoit.api.UserDto;
import com.intech.covoit.converters.UserConverter;
import com.intech.covoit.errors.exceptions.EmailInvalidException;
import com.intech.covoit.errors.exceptions.InformationInvalidException;
import com.intech.covoit.errors.exceptions.PictureTooBigException;
import com.intech.covoit.errors.exceptions.UserNotFoundException;
import com.intech.covoit.model.User;
import com.intech.covoit.repository.UserRepository;
import com.intech.covoit.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	private UserConverter userConverter;

	public UserServiceImpl(UserRepository userRepository, UserConverter userConverter) {
		this.userRepository = userRepository;
		this.userConverter = userConverter;
	}

	public @ResponseBody UserDto createUser(UserDto entryuser) {
		User user = new User();
		user = userConverter.mergeEntity(user, entryuser);
		User userCreated = userRepository.save(user);
		return userConverter.convert(userCreated);
	}

	public void updateUser(String id, UserDto userDto) {
		User existingUser = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
		if (userDto.getPicture().length()>=16793600) {
			throw new PictureTooBigException(userDto.getId());
		}
		if (!isValid(userDto.getEmail())) {
			throw new EmailInvalidException(userDto.getId());
		}
		if (userDto.getFirstName().length()>=30 || userDto.getLastName().length()>=30 ||
				userDto.getCar().getBrand().length()>=30 ||userDto.getCar().getLicensePlate().length()>=10) {
			throw new InformationInvalidException(userDto.getId());
		}
		BeanUtils.copyProperties(userDto, existingUser);
		userRepository.save(existingUser);
	}

	public List<UserDto> findAllUsers() {
		return StreamSupport.stream(userRepository.findAll().spliterator(), false).map(userConverter::convert)
				.collect(Collectors.toList());
	}

	public UserDto findUserById(String id) {
		return userRepository.findById(id).map(userConverter::convert).orElseThrow(() -> new UserNotFoundException(id));
	}

	public UserDto findUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public void deleteAllUsers() {
		this.userRepository.deleteAll();
	}

	public void deleteUserById(String id) {
		User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
		userRepository.delete(user);
	}
	
	public boolean isValid(String email) {
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		return email.matches(regex);
	}
}
