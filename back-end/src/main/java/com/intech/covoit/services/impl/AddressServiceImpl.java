package com.intech.covoit.services.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.intech.covoit.services.AddressService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intech.covoit.api.AddressDto;
import com.intech.covoit.converters.AddressConverter;
import com.intech.covoit.errors.exceptions.AddressNotFoundException;
import com.intech.covoit.model.Address;
import com.intech.covoit.repository.AddressRepository;

@Service
public class AddressServiceImpl implements AddressService {
	
	@Autowired
	private AddressRepository addressRepository;
	@Autowired
	private AddressConverter addressConverter;
	
	public AddressServiceImpl(AddressRepository addressRepository, AddressConverter addressConverter) {
		this.addressRepository = addressRepository;
		this.addressConverter = addressConverter;
	}
	
	public AddressDto createAddress(AddressDto addressDto) {
		Address newAddress = new Address();
		newAddress = addressConverter.mergeEntity(newAddress, addressDto);
		return addressConverter.convert(this.addressRepository.save(newAddress));
	}
	
	public void updateAddress(String id, AddressDto addressDto) {
		Address existingAddress = addressRepository.findById(id)
				.orElseThrow(() -> new AddressNotFoundException(id));
		BeanUtils.copyProperties(addressDto, existingAddress);
		addressRepository.save(existingAddress);
	}
	
	public List<AddressDto> findAllAddress(){
		return StreamSupport.stream(addressRepository.findAll().spliterator(), false).map(addressConverter::convert)
				.collect(Collectors.toList());
	}
	
	public AddressDto findAddressById(String id) {
		return addressRepository.findById(id).map(addressConverter::convert)
				.orElseThrow(() -> new AddressNotFoundException(id));

	}
	
	public void deleteAddressById(String id) {
		Address existingAddress = addressRepository.findById(id).orElseThrow(() -> new AddressNotFoundException(id));
		addressRepository.delete(existingAddress);
	}
}
