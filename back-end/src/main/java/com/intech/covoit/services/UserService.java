package com.intech.covoit.services;

import java.util.List;

import com.intech.covoit.api.UserDto;

public interface UserService {

	public UserDto createUser(UserDto entryuser);

	public void updateUser(String id, UserDto userDto);

	public List<UserDto> findAllUsers();

	public UserDto findUserById(String id);

	public UserDto findUserByUsername(String username);

	public void deleteAllUsers();

	public void deleteUserById(String id);
}
