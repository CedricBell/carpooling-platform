package com.intech.covoit.services;

import java.util.List;

import com.intech.covoit.api.ParkingHistoryDto;

public interface ParkingHistoryService {

	/**
	 * @param id
	 * @param entryParkinghistory
	 */
	public void updateParkingHistory(String id, ParkingHistoryDto entryParkinghistory);

	public ParkingHistoryDto findParkingHistoryById(String id);

	public List<ParkingHistoryDto> findAllParkingHistory();

	public ParkingHistoryDto createParkingHistory(ParkingHistoryDto parkinghistory);

	public void deleteParkingHistoryById(String id);

	boolean aPlaceIsAlreadyOccupatedByUser(ParkingHistoryDto parkinghistory, ParkingHistoryDto existingParkingHistory);
}
