package com.intech.covoit.constants;

public class AppConstants {

	public static final String DATEFORMAT = "yyyy-MM-d H:m";

	public static final String SECRET = "SecretKeyToGenJWTs";

	public static final int EXPIRATION_TIME = 3600000;

	public static final String KEYSTOREPATH = "movIntech.intech.lu.p12";

	public static final String KEYSTOREPASSWORD = "movIntechpassword";

	public static final String KEYSTOREALIAS = "movIntech.intech.lu";

	public static final String SIGN_UP_URL = "/users/sign-up";

	private AppConstants() {

	}

}
