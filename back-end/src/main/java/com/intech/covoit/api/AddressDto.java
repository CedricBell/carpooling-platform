package com.intech.covoit.api;

import java.io.Serializable;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AddressDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@NotNull(message = "Name cannot be null")
	private String name;

	@NotNull(message = "Longitude cannot be null")
	private String longitude;

	@NotNull(message = "Latitude date cannot be null")
	private String latitude;

}
