package com.intech.covoit.api;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ParkingHistoryDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;

	@NotNull(message = "Parking Spot cannot be null")
	private ParkingPlaceDto parkingSpot;

	@NotNull(message = "Trigramm cannot be null")
	@Size(min = 3, max = 3, message = "A trigram has only 3 characters")
	private String trigramOccupant;

	@NotNull(message = "history date cannot be null")
	private String date;

	private String freePlaceDate;

	@NotNull(message = "Immatriculation cannot be null")
	private String licensePlate;

}
