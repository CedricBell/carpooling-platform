package com.intech.covoit.api;

import javax.validation.constraints.NotNull;

import com.intech.covoit.model.RideRequestStatusEnum;
import com.intech.covoit.model.RideUser;

import lombok.Data;

@Data
public class RideUserDto {

	private String id;

	@NotNull(message = "userId cannot be null")
	private UserDto user;

	@NotNull(message = "status cannot be null")
	private RideRequestStatusEnum status;

	@NotNull(message = "date cannot be null")
	private String date;

	private String comments;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RideUserDto other = (RideUserDto) obj;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	public boolean equals(RideUser rideUser) {

		if (rideUser == null)
			return false;
		if (user == null) {
			if (rideUser.getUser() == null) {
				return true;
			}
		} else if (!user.equals(rideUser.getUser()))
			return false;

		return true;

	}

}
