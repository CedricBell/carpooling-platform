package com.intech.covoit.api;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParkingPlaceUpdateDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private int number;

	private String status;

}
