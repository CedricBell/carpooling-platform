package com.intech.covoit.api;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CarDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	@NotNull(message = "immatriculation cannot be null")
	private String licensePlate;

	@NotNull(message = "marque cannot be null")
	private String brand;

}
