package com.intech.covoit.api;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class RideRequestHistoryDto {
	
	private String id;

	@NotNull(message = "rideId cannot be null")
	private RideDto ride;

	@NotNull(message = "rideEvent cannot be null")
	private ArrayList<RideEventDto> rideEvents;

}
