package com.intech.covoit.api;

import java.io.Serializable;

import com.intech.covoit.model.User;

import lombok.Data;

@Data
public class UserDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private String password;

	private String username;

	private String userNo;

	private String lastName;

	private String firstName;

	private String email;

	private CarDto car;

	private String trigram;

	private AddressDto address;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDto other = (UserDto) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (trigram == null) {
			if (other.trigram != null)
				return false;
		} else if (!trigram.equals(other.trigram))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	public boolean equals(User user) {

		if (user == null)
			return false;
		if (email == null) {
			if (user.getEmail() != null)
				return false;
		} else if (!email.equals(user.getEmail()))
			return false;
		if (firstName == null) {
			if (user.getFirstName() != null)
				return false;
		} else if (!firstName.equals(user.getFirstName()))
			return false;
		if (lastName == null) {
			if (user.getLastName() != null)
				return false;
		} else if (!lastName.equals(user.getLastName()))
			return false;
		if (trigram == null) {
			if (user.getTrigram() != null)
				return false;
		} else if (!trigram.equals(user.getTrigram()))
			return false;
		if (username == null) {
			if (user.getUsername() != null)
				return false;
		} else if (!username.equals(user.getUsername()))
			return false;
		return true;

	}

	private String picture;

	private String fcmToken;
}
