package com.intech.covoit.api;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ParkingPlaceDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;

	@NotNull(message = "place Number cannot be null")
	private int number;

	@NotNull(message = "place status cannot be null")
	private String status;

}
