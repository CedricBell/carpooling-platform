package com.intech.covoit.api;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import lombok.Data;

@Data
public class RideDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;

	@Type(type = "User")
	@NotNull
	private UserDto owner;

	@NotNull(message = "Veuillez indiquer un lieu de départ")
	private AddressDto start;

	@NotNull
	private AddressDto end;

	@NotNull
	private String date;

	@NotNull
	private Integer capacity;

	@Type(type = "List<RideUser>")
	private List<RideUserDto> users;

	@Type(type = "List<Address>")
	private List<AddressDto> steps;

}
