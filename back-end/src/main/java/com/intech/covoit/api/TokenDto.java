package com.intech.covoit.api;

import lombok.Data;

import java.io.Serializable;

@Data
public class TokenDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String token;
	
	
	public TokenDto(String token) {
		this.token = token;
	}

	public TokenDto() {
	}

	
	

}
