package com.intech.covoit.api;

import javax.validation.constraints.NotNull;

import com.intech.covoit.model.RideRequestStatusEnum;

import lombok.Data;

@Data
public class RideEventDto {

	private String id;

	@NotNull(message = "userId cannot be null")
	private UserDto user;

	@NotNull(message = "status cannot be null")
	private RideRequestStatusEnum status;

	@NotNull(message = "date cannot be null")
	private String date;

	private String comments;

}
