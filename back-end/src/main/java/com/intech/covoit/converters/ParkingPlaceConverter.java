package com.intech.covoit.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.intech.covoit.api.ParkingPlaceDto;
import com.intech.covoit.model.ParkingPlace;

@Component
public class ParkingPlaceConverter implements Converter<ParkingPlace, ParkingPlaceDto> {

	@Override
	public ParkingPlaceDto convert(ParkingPlace parkingplace) {

		ParkingPlaceDto dto = new ParkingPlaceDto();
		BeanUtils.copyProperties(parkingplace, dto);

		return dto;
	}

	public ParkingPlace mergeEntity(ParkingPlace existingParkingPlace, ParkingPlaceDto parkingplaceDto) {

		return existingParkingPlace.toBuilder().number(parkingplaceDto.getNumber()).status(parkingplaceDto.getStatus())
				.build();

	}

}
