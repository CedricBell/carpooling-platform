package com.intech.covoit.converters;

import java.util.ArrayList;

import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.intech.covoit.api.RideEventDto;
import com.intech.covoit.api.RideRequestHistoryDto;
import com.intech.covoit.model.Ride;
import com.intech.covoit.model.RideEvent;
import com.intech.covoit.model.RideRequestHistory;

@Component
public class RideRequestHistoryConverter implements Converter<RideRequestHistory, RideRequestHistoryDto> {

	private RideEventConverter rideEventConverter;
	private RideConverter rideConverter;

	RideRequestHistoryConverter(RideEventConverter rideEventConverter, RideConverter rideConverter) {
		this.rideConverter = rideConverter;
		this.rideEventConverter = rideEventConverter;
	}

	@Override
	public RideRequestHistoryDto convert(RideRequestHistory rideRequestHistory) {

		RideRequestHistoryDto dto = new RideRequestHistoryDto();
		BeanUtils.copyProperties(rideRequestHistory, dto);
		
		dto.setRide(rideConverter.convert(rideRequestHistory.getRide()));
		
		ArrayList<RideEventDto> rideEventsDto = new ArrayList<>();
		rideRequestHistory.getRideEvents().forEach(event -> rideEventsDto.add(rideEventConverter.convert(event)) ); 
		dto.setRideEvents(rideEventsDto);
		return dto;
	}

	public RideRequestHistory mergeEntity(RideRequestHistory existingRideRequestHistory, RideRequestHistoryDto rideRequestHistoryDto) {

		Ride newRide = new Ride();
		newRide = rideConverter.mergeEntity(newRide, rideRequestHistoryDto.getRide());
		newRide.setId(rideRequestHistoryDto.getRide().getId()); // Id of the ride event for the dbRef

		ArrayList<RideEvent> newRideEvents = new ArrayList<>();
		RideEvent varRideEvent = new RideEvent();
		ArrayList<RideEventDto> rideEventsDto = rideRequestHistoryDto.getRideEvents();
		for (RideEventDto rideEventDto : rideEventsDto) {
			varRideEvent = rideEventConverter.mergeEntity(varRideEvent, rideEventDto);
			varRideEvent.setId(rideEventDto.getId());
			newRideEvents.add(varRideEvent);
		}
		
		return existingRideRequestHistory.toBuilder()
				.ride(newRide)
				.rideEvents(newRideEvents)
				.build();
	}

}