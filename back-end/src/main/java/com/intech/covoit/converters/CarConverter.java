package com.intech.covoit.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.intech.covoit.api.CarDto;
import com.intech.covoit.model.Car;

@Component
public class CarConverter implements Converter<Car, CarDto> {

	@Override
	public CarDto convert(Car car) {

		CarDto dto = new CarDto();
		BeanUtils.copyProperties(car, dto);

		return dto;
	}

	public Car mergeEntity(Car existingCar, CarDto carDto) {

		return existingCar.toBuilder().brand(carDto.getBrand()).licensePlate(carDto.getLicensePlate()).build();

	}

}
