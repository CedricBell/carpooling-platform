package com.intech.covoit.converters;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.intech.covoit.api.ParkingHistoryDto;
import com.intech.covoit.constants.AppConstants;
import com.intech.covoit.model.ParkingHistory;
import com.intech.covoit.model.ParkingPlace;

@Component
public class ParkingHistoryConverter implements Converter<ParkingHistory, ParkingHistoryDto> {

	private ParkingPlaceConverter parkingplaceConverter;

	ParkingHistoryConverter(ParkingPlaceConverter parkingplaceConverter) {

		this.parkingplaceConverter = parkingplaceConverter;
	}

	@Override
	public ParkingHistoryDto convert(ParkingHistory parkinghistory) {

		ParkingHistoryDto dto = new ParkingHistoryDto();
		BeanUtils.copyProperties(parkinghistory, dto);

		dto.setParkingSpot(parkingplaceConverter.convert(parkinghistory.getParkingSpot()));

		if (parkinghistory.getDate() != null) {

			// Transform LocalDateTime to String Date
			dto.setDate(parkinghistory.getDate().format(DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT)));
		}
		if (parkinghistory.getFreePlaceDate() != null) {

			dto.setFreePlaceDate(
					parkinghistory.getFreePlaceDate().format(DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT)));
		}

		return dto;
	}

	public ParkingHistory mergeEntity(ParkingHistory existingParkingHistory, ParkingHistoryDto parkinghistoryDto) {

		ParkingPlace newparkingPlace = new ParkingPlace();
		newparkingPlace = parkingplaceConverter.mergeEntity(newparkingPlace, parkinghistoryDto.getParkingSpot());

		newparkingPlace.setId(parkinghistoryDto.getParkingSpot().getId()); // Id of the parking place for the dbRef

		// Transform String Date to LocalDatetime
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT);

		ParkingHistory parkinghistory = existingParkingHistory.toBuilder()
				.trigramOccupant(parkinghistoryDto.getTrigramOccupant()).parkingSpot(newparkingPlace)
				.date(LocalDateTime.parse(parkinghistoryDto.getDate(), formatter))
				.licensePlate(parkinghistoryDto.getLicensePlate()).build();

		if (parkinghistoryDto.getFreePlaceDate() != null) {

			parkinghistory.setFreePlaceDate(LocalDateTime.parse(parkinghistoryDto.getFreePlaceDate(), formatter));
		}

		return parkinghistory;
	}

}
