package com.intech.covoit.converters;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.intech.covoit.api.RideEventDto;
import com.intech.covoit.constants.AppConstants;
import com.intech.covoit.model.RideEvent;
import com.intech.covoit.model.RideRequestStatusEnum;
import com.intech.covoit.model.User;

@Component
public class RideEventConverter implements Converter<RideEvent, RideEventDto> {

	private UserConverter userConverter;

	RideEventConverter(UserConverter userConverter) {
		this.userConverter = userConverter;
	}

	@Override
	public RideEventDto convert(RideEvent rideEvent) {

		RideEventDto dto = new RideEventDto();
		BeanUtils.copyProperties(rideEvent, dto);

		dto.setUser(userConverter.convert(rideEvent.getUser()));

		dto.setStatus(RideRequestStatusEnum.valueOf(rideEvent.getStatus().toString()));
		
		if (rideEvent.getDate() != null) {			
			//Transform LocalDateTime to String Date
			dto.setDate(
					rideEvent.getDate().format(DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT)));
		}
		dto.setComments(rideEvent.getComments());
		return dto;
	}

	public RideEvent mergeEntity(RideEvent existingRideEvent, RideEventDto rideEventDto) {

		User newUser = new User();
		newUser = userConverter.mergeEntity(newUser, rideEventDto.getUser());
		newUser.setId(rideEventDto.getUser().getId());

		// Transform String Date to LocalDatetime
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT);

		return existingRideEvent.toBuilder().user(newUser).status(rideEventDto.getStatus())
				.date(LocalDateTime.parse(rideEventDto.getDate(), formatter)).comments(rideEventDto.getComments())
				.build();
	}

}
