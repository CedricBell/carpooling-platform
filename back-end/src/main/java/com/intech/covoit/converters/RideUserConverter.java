package com.intech.covoit.converters;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.intech.covoit.api.RideUserDto;
import com.intech.covoit.constants.AppConstants;
import com.intech.covoit.model.RideRequestStatusEnum;
import com.intech.covoit.model.RideUser;
import com.intech.covoit.model.User;

@Component
public class RideUserConverter implements Converter<RideUser, RideUserDto> {

	private UserConverter userConverter;

	RideUserConverter(UserConverter userConverter) {
		this.userConverter = userConverter;
	}

	@Override
	public RideUserDto convert(RideUser rideUser) {

		RideUserDto dto = new RideUserDto();
		BeanUtils.copyProperties(rideUser, dto);

		dto.setUser(userConverter.convert(rideUser.getUser()));

		dto.setStatus(RideRequestStatusEnum.valueOf(rideUser.getStatus().toString()));

		if (rideUser.getDate() != null) {
			// Transform LocalDateTime to String Date
			dto.setDate(rideUser.getDate().format(DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT)));
		}
		dto.setComments(rideUser.getComments());
		return dto;
	}

	public RideUser mergeEntity(RideUser existingRideUser, RideUserDto rideUserDto) {

		User newUser = new User();
		newUser = userConverter.mergeEntity(newUser, rideUserDto.getUser());
		newUser.setId(rideUserDto.getUser().getId());

		// Transform String Date to LocalDatetime
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT);

		return existingRideUser.toBuilder().id(rideUserDto.getId()).user(newUser).status(rideUserDto.getStatus())
				.date(LocalDateTime.parse(rideUserDto.getDate(), formatter)).comments(rideUserDto.getComments())
				.build();
	}

}
