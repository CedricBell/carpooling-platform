package com.intech.covoit.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.intech.covoit.api.UserDto;
import com.intech.covoit.model.Address;
import com.intech.covoit.model.Car;
import com.intech.covoit.model.User;

@Component
public class UserConverter implements Converter<User, UserDto> {

	private CarConverter carConverter;
	private AddressConverter addressConverter;

	public UserConverter(CarConverter carConverter, AddressConverter addressConverter) {

		this.carConverter = carConverter;
		this.addressConverter = addressConverter;
	}

	@Override
	public UserDto convert(User user) {

		UserDto dto = new UserDto();
		BeanUtils.copyProperties(user, dto);

		dto.setCar(carConverter.convert(user.getCar()));
		dto.setAddress(addressConverter.convert(user.getAddress()));
		return dto;
	}

	public User mergeEntity(User existingUser, UserDto userDto) {

		Car newcar = new Car();
		newcar = carConverter.mergeEntity(newcar, userDto.getCar());
		newcar.setId(userDto.getCar().getId()); // Id of the car for the dbRef

		Address newAddress = new Address();
		newAddress = addressConverter.mergeEntity(newAddress, userDto.getAddress());
		newAddress.setId(userDto.getAddress().getId()); // Id of the address for the dbRef

		return existingUser.toBuilder().email(userDto.getEmail()).firstName(userDto.getFirstName())
				.lastName(userDto.getLastName()).userNo(userDto.getUserNo()).car(newcar).username(userDto.getUsername())
				.trigram(userDto.getTrigram()).address(newAddress).picture(userDto.getPicture())
				.fcmToken(userDto.getFcmToken()).build();
	}

}
