package com.intech.covoit.converters;

import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.intech.covoit.api.AddressDto;
import com.intech.covoit.model.Address;

@Component
public class AddressConverter implements Converter<Address, AddressDto> {

	@Override
	public AddressDto convert(Address address) {

		AddressDto dto = new AddressDto();
		BeanUtils.copyProperties(address, dto);

		return dto;
	}

	public Address mergeEntity(Address existingAddress, AddressDto addressDto) {

		return existingAddress.toBuilder().id(addressDto.getId()).name(addressDto.getName())
				.longitude(addressDto.getLongitude()).latitude(addressDto.getLatitude()).build();

	}

}
