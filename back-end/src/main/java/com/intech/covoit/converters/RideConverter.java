package com.intech.covoit.converters;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.intech.covoit.api.AddressDto;
import com.intech.covoit.api.RideDto;
import com.intech.covoit.api.RideUserDto;
import com.intech.covoit.constants.AppConstants;
import com.intech.covoit.model.Address;
import com.intech.covoit.model.Ride;
import com.intech.covoit.model.RideUser;
import com.intech.covoit.model.User;

@Component
public class RideConverter implements Converter<Ride, RideDto> {

	private UserConverter userConverter;
	private RideUserConverter rideUserConverter;
	private AddressConverter addressConverter;

	RideConverter(UserConverter userConverter, RideUserConverter rideUserConverter, AddressConverter addressConverter) {

		this.userConverter = userConverter;
		this.rideUserConverter = rideUserConverter;
		this.addressConverter = addressConverter;
	}

	@Override
	public RideDto convert(Ride ride) {

		RideDto dto = new RideDto();
		BeanUtils.copyProperties(ride, dto);
		dto.setOwner(userConverter.convert(ride.getOwner()));

		dto.setCapacity(ride.getCapacity());
		dto.setEnd(addressConverter.convert(ride.getEnd()));
		dto.setStart(addressConverter.convert(ride.getStart()));
		ArrayList<RideUserDto> participants = new ArrayList<>();
		for (RideUser rideUser : ride.getUsers()) {
			participants.add(rideUserConverter.convert(rideUser));
		}
		dto.setUsers(participants);

		ArrayList<AddressDto> stepAddresses = new ArrayList<>();
		for (Address address : ride.getSteps()) {
			stepAddresses.add(addressConverter.convert(address));
		}
		dto.setSteps(stepAddresses);

		if (ride.getDate() != null) {
			dto.setDate(ride.getDate().format(DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT)));
		}
		return dto;
	}

	public Ride mergeEntity(Ride existingRide, RideDto rideDto) {
		System.out.println("ride merge " + rideDto);
		User newUser = new User();
		newUser = userConverter.mergeEntity(newUser, rideDto.getOwner());
		newUser.setId(rideDto.getOwner().getId());

		ArrayList<RideUser> participants = new ArrayList<>();
		for (RideUserDto rideUserdto : rideDto.getUsers()) {
			RideUser rideUser = new RideUser();
			participants.add(rideUserConverter.mergeEntity(rideUser, rideUserdto));
		}

		ArrayList<Address> newStepAddresses = new ArrayList<>();
		for (AddressDto addressdto : rideDto.getSteps()) {
			Address address = new Address();
			newStepAddresses.add(addressConverter.mergeEntity(address, addressdto));
			System.out.println("step adresses " + newStepAddresses);
		}

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(AppConstants.DATEFORMAT);

		Address newEnd = new Address();
		newEnd = addressConverter.mergeEntity(newEnd, rideDto.getEnd());
		newEnd.setId(rideDto.getEnd().getId());

		Address newStart = new Address();
		newStart = addressConverter.mergeEntity(newStart, rideDto.getStart());
		newStart.setId(rideDto.getStart().getId());

		return existingRide.toBuilder().owner(newUser).start(newStart).end(newEnd).capacity(rideDto.getCapacity())
				.date(LocalDateTime.parse(rideDto.getDate(), formatter)).users(participants).steps(newStepAddresses)
				.build();
	}

}
