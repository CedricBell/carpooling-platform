package com.intech.covoit;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intech.covoit.api.ParkingHistoryDto;
import com.intech.covoit.api.ParkingPlaceDto;
import com.intech.covoit.controllers.ParkingHistoryController;
import com.intech.covoit.services.impl.ParkingHistoryServiceImpl;
import com.intech.covoit.services.impl.authentication.UserDetailsServiceImpl;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WebMvcTest(ParkingHistoryController.class)
public class ParkingHistoryTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ParkingHistoryServiceImpl parkinghistorySerivce;
	@MockBean
	private UserDetailsServiceImpl userDetailServiceImpl;

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void creation_success() throws Exception {

		ParkingPlaceDto parkingplace = new ParkingPlaceDto();
		parkingplace.setId("1");
		parkingplace.setNumber(88);
		parkingplace.setStatus("Libre");

		ParkingHistoryDto sh = new ParkingHistoryDto();
		sh.setTrigramOccupant("JOR");
		sh.setId("2");
		sh.setDate("2019-05-29 14:04:10");
		sh.setParkingSpot(parkingplace);
		sh.setFreePlaceDate("2019-05-29 14:06:10");
		sh.setLicensePlate("GRGZEF2");

		given(parkinghistorySerivce.createParkingHistory(sh)).willReturn(sh);

		this.mockMvc
				.perform(MockMvcRequestBuilders.post("/parkingHistory").content(asJsonString(sh))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.trigrammeOccupant", is(sh.getTrigramOccupant())));

	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void findAll() throws Exception {

		ParkingPlaceDto parkingplace = new ParkingPlaceDto();
		parkingplace.setNumber(88);
		parkingplace.setStatus("Occupée");

		ParkingHistoryDto parkinghistory = new ParkingHistoryDto();
		parkinghistory.setDate("2019-05-29 14:04:10");
		parkinghistory.setTrigramOccupant("JOR");
		parkinghistory.setFreePlaceDate("2019-05-29 14:06:10");
		parkinghistory.setLicensePlate("GRGZEF2");
		parkinghistory.setParkingSpot(parkingplace);

		List<ParkingHistoryDto> history = Arrays.asList(parkinghistory);
		given(parkinghistorySerivce.findAllParkingHistory()).willReturn(history);

		this.mockMvc.perform(get("/parkingHistory")).andExpect(status().isOk())
				.andExpect((jsonPath("$[0].trigrammeOccupant", is(parkinghistory.getTrigramOccupant()))));

	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void findById() throws Exception {

		ParkingPlaceDto parkingplace = new ParkingPlaceDto();
		parkingplace.setNumber(88);
		parkingplace.setStatus("Occupée");

		ParkingHistoryDto parkinghistory = new ParkingHistoryDto();
		parkinghistory.setId("2");
		parkinghistory.setDate("2019-05-29 14:04:10");
		parkinghistory.setTrigramOccupant("JOR");
		parkinghistory.setFreePlaceDate("2019-05-29 14:06:10");
		parkinghistory.setLicensePlate("GRGZEF2");
		parkinghistory.setParkingSpot(parkingplace);

		given(parkinghistorySerivce.findParkingHistoryById("2")).willReturn(parkinghistory);

		this.mockMvc.perform(get("/parkingHistory/{id}", "2")).andExpect(status().isOk())
				.andExpect((jsonPath("$.trigrammeOccupant", is(parkinghistory.getTrigramOccupant()))));

	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void update() throws Exception {

		ParkingPlaceDto parkingplace = new ParkingPlaceDto();
		parkingplace.setNumber(88);
		parkingplace.setStatus("Occupée");

		ParkingHistoryDto parkinghistory = new ParkingHistoryDto();
		parkinghistory.setId("3");
		parkinghistory.setDate("2019-05-29 14:04:10");
		parkinghistory.setTrigramOccupant("FFF");
		parkinghistory.setFreePlaceDate("2019-05-29 14:06:10");
		parkinghistory.setLicensePlate("GRGZEF2");
		parkinghistory.setParkingSpot(parkingplace);

		this.mockMvc
				.perform(MockMvcRequestBuilders.put("/parkingHistory/{id}", "2").content(asJsonString(parkinghistory))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void delete() throws Exception {

		this.mockMvc.perform(MockMvcRequestBuilders.delete("/parkingHistory/{id}", "2")).andExpect(status().isOk());
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
