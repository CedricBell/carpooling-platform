package com.intech.covoit;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intech.covoit.api.ParkingPlaceDto;
import com.intech.covoit.controllers.ParkingPlaceController;
import com.intech.covoit.services.impl.ParkingPlaceServiceImpl;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WebMvcTest(ParkingPlaceController.class)
public class ParkingPlaceTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ParkingPlaceServiceImpl parkingplaceSerivce;

	@Test
	public void create() throws Exception {

		ParkingPlaceDto parkingplace = new ParkingPlaceDto();
		parkingplace.setId("1");
		parkingplace.setNumber(88);
		parkingplace.setStatus("Libre");

		given(parkingplaceSerivce.createParkingPlace(parkingplace)).willReturn(parkingplace);

		this.mockMvc
				.perform(MockMvcRequestBuilders.post("/parking").content(asJsonString(parkingplace))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.placeNumber", is(parkingplace.getNumber())));

	}

	@Test
	public void findAll() throws Exception {

		ParkingPlaceDto parkingplace = new ParkingPlaceDto();
		parkingplace.setNumber(88);
		parkingplace.setStatus("Occupée");

		List<ParkingPlaceDto> places = Arrays.asList(parkingplace);
		given(parkingplaceSerivce.findAllParkingPlace()).willReturn(places);

		this.mockMvc.perform(get("/parking")).andExpect(status().isOk())
				.andExpect((jsonPath("$[0].placeNumber", is(parkingplace.getNumber()))));

	}

	@Test
	public void findById() throws Exception {

		ParkingPlaceDto parkingplace = new ParkingPlaceDto();
		parkingplace.setId("1");
		parkingplace.setNumber(88);
		parkingplace.setStatus("Occupée");

		given(parkingplaceSerivce.findParkingPlaceById("1")).willReturn(parkingplace);

		this.mockMvc.perform(get("/parking/{id}", "1")).andExpect(status().isOk())
				.andExpect((jsonPath("$.placeNumber", is(parkingplace.getNumber()))));

	}

	@Test
	public void update() throws Exception {

		ParkingPlaceDto parkingplace = new ParkingPlaceDto();
		parkingplace.setId("1");
		parkingplace.setNumber(88);
		parkingplace.setStatus("Occupée");

		this.mockMvc
				.perform(MockMvcRequestBuilders.put("/parking/{id}", "2").content(asJsonString(parkingplace))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void delete() throws Exception {

		this.mockMvc.perform(MockMvcRequestBuilders.delete("/parking/{id}", "2")).andExpect(status().isOk());
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
