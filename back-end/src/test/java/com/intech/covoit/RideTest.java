package com.intech.covoit;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intech.covoit.api.AddressDto;
import com.intech.covoit.api.CarDto;
import com.intech.covoit.api.RideDto;
import com.intech.covoit.api.UserDto;
import com.intech.covoit.controllers.RideController;
import com.intech.covoit.services.impl.RideServiceImpl;
import com.intech.covoit.services.impl.authentication.UserDetailsServiceImpl;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WebMvcTest(RideController.class)
public class RideTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private RideServiceImpl rideServiceImpl;
	@MockBean
	private UserDetailsServiceImpl userDetailServiceImpl;

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void create() throws Exception {

		CarDto carDto = new CarDto();
		carDto.setLicensePlate("FZEFZEF");
		carDto.setId("55");
		carDto.setBrand("Renault");
		carDto.setBrand("Renault");

		AddressDto addressDto1 = new AddressDto();
		addressDto1.setName("Metz");
		addressDto1.setLongitude("6.1727");
		addressDto1.setLatitude("49.119");

		AddressDto addressDto2 = new AddressDto();
		addressDto2.setName("Kayl");
		addressDto2.setLongitude("6.03932");
		addressDto2.setLatitude("49.4866");

		UserDto user = new UserDto();
		user.setEmail("alban.schwaller@intech.lu");
		user.setFirstName("alban");
		user.setLastName("schwaller");
		user.setId("1");
		user.setUserNo("User1");
		user.setCar(carDto);
		user.setAddress(addressDto1);
		user.setPicture("");

		RideDto ride = new RideDto();
		ride.setId("1");
		ride.setCapacity(2);
		ride.setDate("21/03/2019");
		ride.setEnd(addressDto1);
		ride.setOwner(user);
		ride.setStart(addressDto2);

		given(rideServiceImpl.createRide(ride)).willReturn(ride);
		this.mockMvc
				.perform(MockMvcRequestBuilders.post("/rides").content(asJsonString(ride))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.date", is(ride.getDate())));
	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void findAll() throws Exception {

		CarDto carDto = new CarDto();
		carDto.setLicensePlate("FZEFZEF");
		carDto.setId("55");
		carDto.setBrand("Renault");

		AddressDto addressDto1 = new AddressDto();
		addressDto1.setName("Metz");
		addressDto1.setLongitude("6.1727");
		addressDto1.setLatitude("49.119");

		AddressDto addressDto2 = new AddressDto();
		addressDto2.setName("Kayl");
		addressDto2.setLongitude("6.03932");
		addressDto2.setLatitude("49.4866");

		UserDto user = new UserDto();
		user.setEmail("alban.schwaller@intech.lu");
		user.setFirstName("alban");
		user.setLastName("schwaller");
		user.setId("1");
		user.setUserNo("User1");
		user.setCar(carDto);
		user.setAddress(addressDto1);
		user.setPicture("");

		RideDto ride = new RideDto();
		ride.setId("1");
		ride.setCapacity(2);
		ride.setDate("21/03/2019");
		ride.setEnd(addressDto1);
		ride.setOwner(user);
		ride.setStart(addressDto2);

		List<RideDto> rides = Arrays.asList(ride);
		given(rideServiceImpl.findAllRides()).willReturn(rides);

		this.mockMvc.perform(get("/rides")).andExpect(status().isOk())
				.andExpect((jsonPath("$[0].date", is(ride.getDate()))));

	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void findById() throws Exception {

		CarDto carDto = new CarDto();
		carDto.setLicensePlate("FZEFZEF");
		carDto.setId("55");
		carDto.setBrand("Renault");

		AddressDto addressDto1 = new AddressDto();
		addressDto1.setName("Metz");
		addressDto1.setLongitude("6.1727");
		addressDto1.setLatitude("49.119");

		AddressDto addressDto2 = new AddressDto();
		addressDto2.setName("Kayl");
		addressDto2.setLongitude("6.03932");
		addressDto2.setLatitude("49.4866");

		UserDto user = new UserDto();
		user.setEmail("alban.schwaller@intech.lu");
		user.setFirstName("alban");
		user.setLastName("schwaller");
		user.setId("1");
		user.setUserNo("User1");
		user.setCar(carDto);
		user.setAddress(addressDto1);
		user.setPicture("");

		RideDto ride = new RideDto();
		ride.setId("1");
		ride.setCapacity(2);
		ride.setDate("21/03/2019");
		ride.setEnd(addressDto1);
		ride.setOwner(user);
		ride.setStart(addressDto2);

		given(rideServiceImpl.findRideById("1")).willReturn(ride);

		this.mockMvc.perform(get("/rides/{id}", "1")).andExpect(status().isOk())
				.andExpect((jsonPath("$.start", is(ride.getStart()))));

	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void update() throws Exception {

		CarDto carDto = new CarDto();
		carDto.setLicensePlate("FZEFZEF");
		carDto.setId("55");
		carDto.setBrand("Renault");

		AddressDto addressDto1 = new AddressDto();
		addressDto1.setName("Metz");
		addressDto1.setLongitude("6.1727");
		addressDto1.setLatitude("49.119");

		AddressDto addressDto2 = new AddressDto();
		addressDto2.setName("Kayl");
		addressDto2.setLongitude("6.03932");
		addressDto2.setLatitude("49.4866");

		UserDto user = new UserDto();
		user.setEmail("alban.schwaller@intech.lu");
		user.setFirstName("alban");
		user.setLastName("schwaller");
		user.setId("1");
		user.setUserNo("User1");
		user.setCar(carDto);
		user.setAddress(addressDto1);
		user.setPicture("");

		RideDto ride = new RideDto();
		ride.setId("1");
		ride.setCapacity(2);
		ride.setDate("21/03/2019");
		ride.setEnd(addressDto1);
		ride.setOwner(user);
		ride.setStart(addressDto2);

		this.mockMvc
				.perform(MockMvcRequestBuilders.put("/rides/{id}", "1").content(asJsonString(ride))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void delete() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.delete("/rides/{id}", "1")).andExpect(status().isOk());
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
