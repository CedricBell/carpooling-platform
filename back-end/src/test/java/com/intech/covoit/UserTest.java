package com.intech.covoit;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intech.covoit.api.UserDto;
import com.intech.covoit.controllers.UserController;
import com.intech.covoit.services.impl.UserServiceImpl;
import com.intech.covoit.services.impl.authentication.UserDetailsServiceImpl;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WebMvcTest(UserController.class)
public class UserTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserServiceImpl UserService;

	@MockBean
	private UserDetailsServiceImpl userDetailServiceImpl;

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void create() throws Exception {
		UserDto user = new UserDto();
		user.setEmail("alban.schwaller@intech.lu");
		user.setFirstName("alban");
		user.setLastName("schwaller");
		user.setId("1");
		user.setUserNo("User1");

		given(UserService.createUser(user)).willReturn(user);

		this.mockMvc
				.perform(MockMvcRequestBuilders.post("/users").content(asJsonString(user))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.email", is(user.getEmail())));
	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void findAll() throws Exception {

		UserDto user = new UserDto();
		user.setEmail("alban.schwaller@intech.lu");
		user.setFirstName("alban");
		user.setLastName("schwaller");
		user.setId("1");
		user.setUserNo("User1");

		List<UserDto> Users = Arrays.asList(user);
		given(UserService.findAllUsers()).willReturn(Users);

		this.mockMvc.perform(get("/users")).andExpect(status().isOk())
				.andExpect((jsonPath("$[0].email", is(user.getEmail()))));

	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void findById() throws Exception {
		UserDto user = new UserDto();
		user.setEmail("alban.schwaller@intech.lu");
		user.setFirstName("alban");
		user.setLastName("schwaller");
		user.setId("1");
		user.setUserNo("User1");

		given(UserService.findUserById("1")).willReturn(user);

		this.mockMvc.perform(get("/users/{id}", "1")).andExpect(status().isOk())
				.andExpect((jsonPath("$.email", is(user.getEmail()))));

	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void update() throws Exception {

		UserDto user = new UserDto();
		user.setEmail("alban.schwaller@intech.lu");
		user.setFirstName("alban");
		user.setLastName("schwaller");
		user.setId("1");
		user.setUserNo("User1");
		this.mockMvc
				.perform(MockMvcRequestBuilders.put("/users/{id}", "1").content(asJsonString(user))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	@WithMockUser(username = "username1", password = "password")
	public void delete() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.delete("/users/{id}", "1")).andExpect(status().isOk());
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
