InTech - Backend de la plateforme de covoiturage interne
===========

Réalisé par Jorane SCHUSTER, Marine PALLOTTA et Cédric BELL.
Encadré par Thibaut HUMBERT et Rémy BENOIST.

## Description générale

La plateforme de covoiturage d'InTech **MovInTech** est une des applications internes d'**InTech**. 


## Technologies utilisées

Il s'agit d'une *API REST* développée grâce au framework *Sprint Boot* (basé sur le langage *Java*).


## Configuration

1. Assurez-vous que [Java](https://www.java.com/fr/) et [Maven](https://maven.apache.org/) sont bien installés et configurés.

2. Les IDE suivants sont conseillés :

- [Spring Tool Suite](https://spring.io/tools).
- [Eclipse](https://www.eclipse.org/) avec l'extension `Spring Tool Suite` installée.


## Tests

Quelques tests d'intégrations validant le comportement de notre *API REST* ont été réalisés dans le répertoire src/test/java/com/intech/covoit



## Lancement

### Via un terminal

Vous disposez de deux configurations pour lancer l'application : 

```ruby
foo@bar:~$ mvn spring-boot:run 
```
si vous souhaitez lancer l'application backend communiquant avec une base de données (MongoDb) tournant localement sur votre PC;

ou 

```ruby
foo@bar:~$ mvn spring-boot:run -Dspring.profiles.active=integration
```
si vous souaitez lancer l'application backend communiquant avec la base de données de l'environnement de test.

### Via un des IDE conseillés

- Dans l'onglet `Run`, sélectionner `Run configurations`.
- Créer une nouvelle configuration pour le projet `plateforme-covoiturage`.
- Pour `Main Type`, remplisser `com.intech.covoit.CovoitApplication`.
- Pour `Profile`, remplisser `local` ou `integration`.
- Sauvegarder cette configuration.

Vous pouvez désormais utiliser cette configuration pour lancer le projet depuis votre IDE.

Une fois lancée, l'API est accessible à l'adresse suivante : [http://localhost:8080/](http://localhost:8080/).

## Autres commandes utiles

### Build

Pour builder l'application, lancer la commande :  
```
mvn clean package
```

### Documentation
Pour obtenir la documentation du back-end de l'application, lancer l'application et ouvrer un navigateur à l'adresse 
: [swagger-ui](http://localhost:8080/swagger-ui.html) pour accéder à la documentation de l'*API REST*.

### Sonar analyse
Pour analyser la qualité du code de l'application du front-end de l'application : 

```
mvn sonar:sonar -Dsonar.host.url=http://192.168.11.27:9000  -Dsonar.login=acfeee07700c2bfed555bca864b1e20e24a9a0dd -Dsonar.projectKey=PLATEFORME_COVOITURAGE_BACK
```

Ouvrer un navigateur à l'adresse : [sonar-covoiturage-backend](http://192.168.11.27:9000/dashboard?id=PLATEFORME_COVOITURAGE_BACK)
pour accéder à la dernière analyse lancée.



## Déploiement

Pour déployer, veuillez vous réferrez à ce 
[document de référence](https://gitlab.intech.lu/plateforme-covoiturage/deployment/blob/master/Notice-D%C3%A9ploiement.pdf).

